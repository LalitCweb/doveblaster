import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import {
  PayPal,
  PayPalPayment,
  PayPalConfiguration
} from "@ionic-native/paypal";

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-checkout",
  templateUrl: "checkout.html"
})
export class CheckoutPage {
  cartdata: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    private payPal: PayPal
  ) {
    this.cartdata = this.navParams.get("cartdata");
  }

  ionViewDidLoad() {}
  ionViewWillEnter() {
    this.auth.clearcookie();
  }
  ionViewWillLeave() {
    this.auth.clearcookie();
  }

  goto(page) {
    this.navCtrl.push(page);
  }
  carttotal: any;
  currency_symbol: any;
  subtotal: any;
  booking: any;
  getcart() {
    this.auth.startloader();
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(data => {
        this.auth.stoploader();
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.auth.clearcookie();
            if (data.json().more_data) {
              this.cartdata.cart_data = data.json().more_data;
              this.cartdata.subtotal =
                data.json().content_total -
                this.auth.convertToint(
                  data.json().main_fees.booking_fee_4_95_slot.amount
                );
              this.cartdata.carttotal = data.json().total;
              this.cartdata.booking = this.auth.convertToint(
                data.json().main_fees.booking_fee_4_95_slot.amount
              );
              // this.cartdata.booking =
              //   this.auth.convertToint(data.json().total) -
              //   data.json().content_total;
              this.cartdata.currency_symbol = data.json().currency_symbol;
              this.makePayment();
            }
          } else if (data.json() && data.json().status === 0) {
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (e) {
      this.auth.toast(e);
      this.auth.stoploader();
    }
  }

  gotohome(page) {
    this.navCtrl.setRoot(page);
  }
  makePayment() {
    this.auth.startloader();
    try {
      this.auth.makepayment().subscribe(data => {
        this.auth.stoploader();
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.auth.clearcookie();
            this.openpaypal(
              this.auth.convertToint(data.json().total),
              data.json().order_id,
              data.json().notify_url
            );
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (err) {
      this.auth.clearcookie();
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  openpaypal(payment_amount, order_id, notify_url) {
    this.payPal
      .init({
        PayPalEnvironmentProduction:
          "AVtu8I78hTz7ZygVLlglLor2t0_erFosZgtHwr9d6Thgpg68v3xcIQTAAa6fqpcEQzNoOutZV9DtXpqL",
        PayPalEnvironmentSandbox:
          "AVtu8I78hTz7ZygVLlglLor2t0_erFosZgtHwr9d6Thgpg68v3xcIQTAAa6fqpcEQzNoOutZV9DtXpqL"
      })
      .then(
        env => {
          // debugger;
          // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
          this.payPal
            .prepareToRender(
              "PayPalEnvironmentSandbox",
              new PayPalConfiguration({
                // Only needed if you get an "Internal Service Error" after PayPal login!
                //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
              })
            )
            .then(
              pay => {
                // debugger;
                let payment = new PayPalPayment(
                  payment_amount,
                  "USD",
                  "Description",
                  "sale"
                );
                payment.custom = JSON.stringify({
                  order_id: order_id,
                  notify_url: notify_url
                });
                this.payPal.renderSinglePaymentUI(payment).then(
                  response => {
                    // debugger;
                    if (response.response.state == "approved") {
                      this.auth.clearcookie();
                      this.getorder_detail(order_id, response);
                    }
                    // Successfully paid
                    // Example sandbox response
                    //
                    // {
                    //   "client": {
                    //     "environment": "sandbox",
                    //     "product_name": "PayPal iOS SDK",
                    //     "paypal_sdk_version": "2.16.0",
                    //     "platform": "iOS"
                    //   },
                    //   "response_type": "payment",
                    //   "response": {
                    //     "id": "PAY-1AB23456CD789012EF34GHIJ",
                    //     "state": "approved",
                    //     "create_time": "2016-10-03T13:33:33Z",
                    //     "intent": "sale"
                    //   }
                    // }
                  },
                  () => {
                    // Error or render dialog closed without being successful
                  }
                );
              },
              () => {
                // Error in configuration
              }
            );
        },
        () => {
          // Error in initialization, maybe PayPal isn't supported or something else
        }
      );
  }

  getorder_detail(order_id, response) {
    this.auth.startloader();
    try {
      this.auth.orderdetail(order_id).subscribe(data => {
        this.auth.stoploader();
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.auth.clearcookie();
            var ee: any = this;
            this.cartdata.cart_data.forEach((iten, index) => {
              ee.confirmorder(iten, response, order_id);
            });
            this.navCtrl.setRoot("OrdersuccessPage", {
              order_detail: data.json(),
              cart_data: this.cartdata.cart_data
            });
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (err) {
      this.auth.clearcookie();
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  confirmorder(data, response, order_id) {
    this.auth.startloader();
    try {
      var ddt: any = {
        property_id: data.property_id,
        fromdate: data.date_of_product,
        to_date: data.date_of_product,
        linked_slot_tobooking: data.slotid,
        product_price: data.price,
        booking_product_price_after_discount: data.price_after_discount,
        product_id: data.product_id,
        order_id: order_id,
        transaction_id: response.response.id,
        hunter_paypal_email: this.auth.getUsername("email"),
        user_id: this.auth.getuserId()
      };
      this.auth.confirmorder(ddt).subscribe(data => {
        this.auth.stoploader();
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.auth.clearcookie();
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (err) {
      this.auth.clearcookie();
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
}
