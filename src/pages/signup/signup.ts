import { Component, OnInit, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  Content
} from "ionic-angular";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage implements OnInit {
  @ViewChild(Content)
  content: Content;
  // Validation form group definition
  configs: FormGroup;
  user: any = {
    email: "",
    password: "",
    cpassword: "",
    name: "",
    cname: "",
    conditions: ""
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public auth: AuthProvider
  ) {
    // Disable Side Menu
    this.menuCtrl.enable(false, "myMenu");
  }

  ngOnInit() {
    // Validations Definition
    let EMAILPATTERNS = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.configs = new FormGroup({
      email: new FormControl("", [
        Validators.required,
        Validators.pattern(EMAILPATTERNS)
      ]),
      password: new FormControl("", [Validators.required]),
      cpassword: new FormControl("", [Validators.required]),
      name: new FormControl("", [Validators.required]),
      cname: new FormControl("", [
        Validators.required,
        Validators.pattern("^(?=.*[a-zA-Z])[a-zA-Z0-9]+$")
      ]),
      conditions: new FormControl("", [Validators.required])
    });
  }

  ionViewDidLoad() {}

  ionViewWillLeave() {
    this.auth.clearcookie();
  }

  enter() {
    this.navCtrl.setRoot("LoginPage");
  }

  signup() {
    if (this.configs.valid) {
      this.signuphunter();
    } else {
      for (let i in this.configs.controls)
        this.configs.controls[i].markAsTouched();
      this.auth.toast("Form is invalid. Please validate form carefully");
      this.content.scrollToTop();
    }
  }

  signuphunter() {
    this.auth.startloader();
    try {
      this.auth.signup(this.configs.value, 1).subscribe(
        data => {
          this.auth.stoploader();
          if (data.json()) {
            if (data.json().status === 1) {
              if (data.json().message) {
                this.auth.toast(data.json().message);
                this.navCtrl.push("VerifyemailPage", {
                  user_id: data.json().user_id,
                  userpass: {
                    username: this.configs.value.cname,
                    password: this.configs.value.password
                  }
                });
                this.configs.reset;
              } else {
                this.auth.toast("The account was created.You can login now.");
                this.configs.reset;
              }
            } else if (data.json().status === 0) {
              if (data.json().message) {
                this.auth.toast(data.json().message);
              } else {
                this.auth.toast("Cannot create profile with these details");
              }
            } else {
              if (data.json().message) {
                this.auth.toast(data.json().message);
              } else {
                this.auth.toast("Cannot create profile with these details");
              }
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.toast("Currently not able to signup.");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  mismatch: any;
  pwdMatchValidator() {
    if (this.user.password != "" && this.user.cpassword != "") {
      if (
        this.user.password != this.user.cpassword ||
        this.user.cpassword != this.user.password
      ) {
        this.mismatch = true;
      } else {
        this.mismatch = false;
      }
    } else {
      this.mismatch = false;
    }
  }

  getdetail() {
    this.auth.getuserdata(this.configs.value).subscribe(
      data => {
        this.auth.clearcookie();
      },
      errorHandler => {
        this.auth.stoploader();
        this.auth.toast("Not able to get user data");
      }
      // () => {
      //   this.auth.stoploader();
      // }
    );
  }
}
