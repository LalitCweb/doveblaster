import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import * as $ from "jquery";

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var $: any;

@IonicPage()
@Component({
  selector: "page-search",
  templateUrl: "search.html"
})
export class SearchPage {
  rangeObject: any = { lower: 1, upper: 5000 };
  configs: FormGroup;
  dualValue2: any = {};
  data: any = {
    feat: "",
    categ: ""
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public auth: AuthProvider
  ) {
    this.configs = new FormGroup({
      categ: new FormControl(""),
      feat: new FormControl(""),
      feat1: new FormControl(""),
      feat2: new FormControl(""),
      feat3: new FormControl("")
    });
    if (this.navParams.get("checked_data")) {
      if (
        this.navParams.get("checked_data").all_checkers != "" ||
        this.navParams.get("checked_data").category_values != ""
      ) {
        this.configs.controls["feat"].setValue(
          this.navParams.get("checked_data").all_checkers
        );
        this.configs.controls["feat1"].setValue(
          this.navParams.get("checked_data").all_checkers
        );
        this.configs.controls["feat2"].setValue(
          this.navParams.get("checked_data").all_checkers
        );
        this.configs.controls["feat3"].setValue(
          this.navParams.get("checked_data").all_checkers
        );
        this.configs.controls["categ"].setValue(
          this.navParams.get("checked_data").category_values
        );
        this.rangeObject = this.navParams.get("checked_data").price;
      }
    }
  }
  ionViewWillEnter() {
    this.getcategories();
    this.getfeatures();
    // this.pricesselector();
  }
  ionViewDidLoad() {}
  rangeChange() {}
  dismiss(value) {
    if (value == 0) {
      this.viewCtrl.dismiss();
    } else {
      this.data.feat = [];
      var tis = this;
      if (this.data.available_species && this.data.available_species.length) {
        this.data.available_species.forEach(function(species, index) {
          tis.data.feat.push(species);
        });
      }
      if (this.data.property_features && this.data.property_features.length) {
        this.data.property_features.forEach(function(features, index) {
          tis.data.feat.push(features);
        });
      }
      if (
        this.data.permitted_onproperty &&
        this.data.permitted_onproperty.length
      ) {
        this.data.permitted_onproperty.forEach(function(
          permittedonproperty,
          index
        ) {
          tis.data.feat.push(permittedonproperty);
        });
      }
      if (this.data.permitted_weapon && this.data.permitted_weapon.length) {
        this.data.permitted_weapon.forEach(function(permittedweapon, index) {
          tis.data.feat.push(permittedweapon);
        });
      }
      this.configs.value.categ = this.configs.value.categ
        .replace(/\s+/g, " ")
        .trim();
      let data = {
        category_values: this.configs.value.categ,
        all_checkers: this.data.feat,
        price: this.rangeObject
      };
      this.viewCtrl.dismiss(data);
    }
  }

  pricesselector() {
    $(document).ready(function() {
      $("#demo").ionRangeSlider({
        type: "double",
        grid: true,
        min: -1000,
        max: 1000,
        from: -500,
        to: 500,
        step: 250
      });
    });
  }

  categories: any = [];
  getcategories() {
    this.auth.getcategories(this.auth.getuserId()).subscribe(data => {
      if (data) {
        if (data.json().all_categories) {
          this.categories = data.json().all_categories;
        }
      }
    });
  }

  features: any;
  getfeatures() {
    this.auth.getfeatures(this.auth.getuserId()).subscribe(data => {
      if (data) {
        if (data.json().eminities) {
          this.features = data.json().eminities;
        }
      }
    });
  }

  getprice() {}
}
