import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  group,
  NgZone,
  ViewChild,
  ElementRef
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ModalController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  NativeGeocoder,
  NativeGeocoderReverseResult,
  NativeGeocoderForwardResult,
  NativeGeocoderOptions
} from "@ionic-native/native-geocoder";
import { errorHandler } from "@angular/platform-browser/src/browser";
/**
 * Generated class for the EditpropertyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-editproperty",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "editproperty.html"
})
export class EditpropertyPage {
  @ViewChild("map_canvas") mapElements: ElementRef;
  service = new google.maps.places.AutocompleteService();
  configs: FormGroup;
  placesService: any;
  autocompleteItems;
  autocomplete;
  propertyrvw: any = "detail";
  slot1: any = "test";
  slot2: any = "test";
  slot3: any = "test";
  selectedproperty: any = {
    title: "",
    category: "",
    city: "",
    wantlease: "",
    county: "",
    state: "",
    country: "",
    zipcode: "",
    description: "",
    feature: ""
  };
  property: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    private zone: NgZone,
    private nativeGeocoder: NativeGeocoder,
    public platform: Platform,
    public modalCtrl: ModalController
  ) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ""
    };
    this.configs = new FormGroup({
      title: new FormControl("", [Validators.required]),
      category: new FormControl("", [Validators.required]),
      city: new FormControl("", [Validators.required]),
      wantlease: new FormControl("", [Validators.required]),
      county: new FormControl("", [Validators.required]),
      state: new FormControl("", [Validators.required]),
      country: new FormControl("", [Validators.required]),
      zipcode: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      feature: new FormControl(""),
      feature1: new FormControl(""),
      feature2: new FormControl(""),
      feature3: new FormControl("")
    });
    if (this.navParams.get("property_data")) {
      this.property = this.navParams.get("property_data");
    }
    this.setdata();
  }
  setdata() {
    this.getfeatures();
    this.getcategories();
    this.getlease();
    if (this.property) {
      this.configs.controls["title"].setValue(this.property.property_name);
      this.configs.controls["county"].setValue(this.property.property_county);
      this.configs.controls["state"].setValue(this.property.property_state);
      this.configs.controls["country"].setValue(this.property.property_country);
      this.configs.controls["zipcode"].setValue(this.property.property_zip);
      this.configs.controls["feature"].setValue(this.property.prop_featured);
      this.configs.controls["city"].setValue(this.property.city);
      if (this.property.description) {
        this.configs.controls["description"].setValue(
          this.property.description.replace(/<[^>]*>/g, "")
        );
      }
      this.property.title = this.property.property_name;
      this.property.county = this.property.property_county;
      this.property.state = this.property.property_state;
      this.property.country = this.property.property_country;
      this.property.zipcode = this.property.property_zip;
      // this.property.feature = this.property.prop_featured;
      this.property.city = this.property.city;
      this.propertylocation.prop_area_center_lat = this.property.prop_area_center_lat;
      this.propertylocation.prop_area_center_lng = this.property.prop_area_center_lng;
      if (this.property.description) {
        this.property.description = this.property.description.replace(
          /<[^>]*>/g,
          ""
        );
      }
      // this.configs.controls["city"].setValue(this.property.city)
      // if (this.property.all_eminities && this.property.all_eminities.length) {
      //   for (var emn = 0; emn < this.property.all_eminities.length; emn++) {
      //     this.property.all_eminities[emn] =
      //       this.property.all_eminities[emn].charAt(0).toUpperCase() +
      //       this.property.all_eminities[emn].substring(1);
      //     this.property.all_eminities[emn] = this.property.all_eminities[
      //       emn
      //     ].replace(/_/g, " ");
      //   }
      //   this.configs.controls["feature"].setValue(this.property.all_eminities);
      //   this.property.feature = this.property.all_eminities;
      // }
    } else {
    }
  }
  ionViewWillEnter() {
    this.initializemap();
  }
  ionViewDidLoad() {}

  categories: any = [];
  getcategories() {
    this.auth.getcategories(this.auth.getuserId()).subscribe(data => {
      if (data) {
        if (data.json().all_categories) {
          this.categories = data.json().all_categories;
          for (var cat = 0; cat < this.categories.length; cat++) {
            if (
              this.property.property_category &&
              this.categories[cat].name ==
                this.property.property_category.replace(/<[^>]*>/g, "")
            ) {
              this.configs.controls["category"].setValue(
                this.categories[cat].cat_ID
              );
            }
          }
        }
      }
    });
  }
  features: any = [];
  getfeatures() {
    this.auth.getfeatures(this.auth.getuserId()).subscribe(data => {
      if (data) {
        if (data.json().eminities) {
          var es: any = this;
          es.features = data.json().eminities;
          if (es.property.all_eminities.property_features) {
            es.property.features = es.property.all_eminities.property_features;
            es.property.all_eminities.property_features.forEach(
              (item, index) => {
                // debugger;
                es.property.all_eminities.property_features[
                  index
                ] = es.auth.capitalizeb(item);
              }
            );
            es.configs.controls["feature"].setValue(
              es.property.all_eminities.property_features
            );
          }
          if (es.property.all_eminities.available_species) {
            es.property.available_species =
              es.property.all_eminities.available_species;
            es.property.all_eminities.available_species.forEach(
              (item, index) => {
                // debugger;
                es.property.all_eminities.available_species[
                  index
                ] = es.auth.capitalizeb(item);
              }
            );
            es.configs.controls["feature1"].setValue(
              es.property.all_eminities.available_species
            );
          }

          if (es.property.all_eminities.permitted_weapon) {
            es.property.permitted_weapon =
              es.property.all_eminities.permitted_weapon;
            es.property.all_eminities.permitted_weapon.forEach(
              (item, index) => {
                // debugger;
                es.property.all_eminities.permitted_weapon[
                  index
                ] = es.auth.capitalizeb(item);
              }
            );
            es.configs.controls["feature2"].setValue(
              es.property.all_eminities.permitted_weapon
            );
          }

          if (es.property.all_eminities.permitted_onproperty) {
            es.property.permitted_onproperty =
              es.property.all_eminities.permitted_onproperty;
            es.property.all_eminities.permitted_onproperty.forEach(
              (item, index) => {
                // debugger;
                es.property.all_eminities.permitted_onproperty[
                  index
                ] = es.auth.capitalizeb(item);
              }
            );
            es.configs.controls["feature3"].setValue(
              es.property.all_eminities.permitted_onproperty
            );
          }
        }
      }
    });
  }
  lease: any = [];
  getlease() {
    this.auth.getlease(this.auth.getuserId()).subscribe(data => {
      if (data) {
        if (data.json().all_action_categories) {
          this.lease = data.json().all_action_categories;

          for (var lea = 0; lea < this.lease.length; lea++) {
            if (
              this.lease[lea].name ==
              this.property.property_action.replace(/<[^>]*>/g, "")
            ) {
              this.configs.controls["wantlease"].setValue(
                this.lease[lea].cat_ID
              );
              this.property.wantlease = this.lease[lea].cat_ID;
            }
          }
        }
      }
    });
  }
  emnities: any = {};
  addproperty() {
    if (this.configs.valid) {
      this.property.feature = [];
      var tis = this;
      tis.emnities.checkboxes_eminities = {};
      if (
        this.property.available_species &&
        this.property.available_species.length
      ) {
        this.property.available_species.forEach(function(species, index) {
          tis.property.feature.push(species);
        });
      }
      if (this.property.features && this.property.features.length) {
        this.property.features.forEach(function(features, index) {
          tis.property.feature.push(features);
        });
      }
      if (
        this.property.permitted_onproperty &&
        this.property.permitted_onproperty.length
      ) {
        this.property.permitted_onproperty.forEach(function(
          permittedonproperty,
          index
        ) {
          tis.property.feature.push(permittedonproperty);
        });
      }
      if (
        this.property.permitted_weapon &&
        this.property.permitted_weapon.length
      ) {
        this.property.permitted_weapon.forEach(function(
          permittedweapon,
          index
        ) {
          tis.property.feature.push(permittedweapon);
        });
      }
      this.features.available_species.forEach((value, index) => {
        tis.emnities.checkboxes_eminities[
          tis.auth.capitalize(value)
        ] = tis.checkemnities(value, tis.property.available_species);
      });
      this.features.permitted_onproperty.forEach((value, index) => {
        tis.emnities.checkboxes_eminities[
          tis.auth.capitalize(value)
        ] = tis.checkemnities(value, tis.property.permitted_onproperty);
      });
      this.features.permitted_weapon.forEach((value, index) => {
        tis.emnities.checkboxes_eminities[
          tis.auth.capitalize(value)
        ] = tis.checkemnities(value, tis.property.permitted_weapon);
      });
      this.features.property_features.forEach((value, index) => {
        tis.emnities.checkboxes_eminities[
          tis.auth.capitalize(value)
        ] = tis.checkemnities(value, tis.property.features);
      });
      // this.features.available_species = this.features.available_species.map(
      //   user => user.name
      // );
      this.submitproperty();
    }
  }
  checkemnities(value, dataarr) {
    if (dataarr && value) {
      if (dataarr.filter(item => item == value).length == 0) {
        return "0";
      } else {
        return "1";
      }
    } else {
      return "0";
    }
  }

  map_canvas;
  initializemap() {
    this.platform.ready().then(() => {
      let mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(22, 77),
        tilt: 0
      };
      this.map_canvas = new google.maps.Map(
        this.mapElements.nativeElement,
        mapOptions
      );
      this.placesService = new google.maps.places.PlacesService(
        this.map_canvas
      );
    });
  }
  public propertylocation: any = {};
  chooseItem(data) {
    this.filter(data.place_id);
    // if (data.terms && data.terms.length) {
    //   this.property.country = data.terms[data.terms.length - 1].value;
    // }
    // this.property.state = data.structured_formatting.main_text;
    // this.property.city = data.description;
    this.property.property_admin_area =
      data.structured_formatting.secondary_text;
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.nativeGeocoder
      .forwardGeocode(data.description, options)
      .then((coordinates: NativeGeocoderForwardResult[]) => {
        this.propertylocation.prop_area_center_lat = coordinates[0].latitude;
        this.propertylocation.prop_area_center_lng = coordinates[0].longitude;
      })
      .catch((error: any) => {
        this.auth.errtoast(error);
      });

    this.nativeGeocoder
      .reverseGeocode(
        this.propertylocation.prop_area_center_lat,
        this.propertylocation.prop_area_center_lng,
        options
      )
      .then((result: NativeGeocoderReverseResult[]) => {})
      .catch((error: any) => {
        this.auth.errtoast(error);
      });
    this.autocompleteItems = [];
  }
  submitproperty() {
    this.auth.startloader();
    try {
      var senddata = {
        user_id: this.auth.getuserId(),
        wpestate_title: this.property.title,
        prop_category: this.property.category,
        prop_action_category: this.property.wantlease,
        property_country: this.property.country,
        property_city: this.configs.value.city,
        property_admin_area: this.property.property_admin_area,
        prop_area_center_lat: this.propertylocation.prop_area_center_lat,
        prop_area_center_lng: this.propertylocation.prop_area_center_lng,
        property_county: this.property.county,
        property_state: this.property.state,
        property_zip: this.property.zipcode,
        property_description: this.property.description,
        checkboxes_eminities: this.property.feature,
        property_id: ""
      };
      this.auth
        .editproperty(
          this.auth.getuserId(),
          this.property,
          this.configs.value,
          this.propertylocation,
          this.property.property_id,
          this.emnities.checkboxes_eminities
        )
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              if (data.json().status == 1) {
                if (data.json().property_id) {
                  this.auth.toast("Property updated successfully");
                  // senddata.property_id = data.json().property_id;
                  // this.navCtrl.push("AddareaPage", {
                  //   property_data: senddata
                  // });
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
          }
        );
    } catch (err) {
      this.auth.stoploader();
    }
  }
  citysearch(data) {
    if (this.property.city == "") {
      this.autocompleteItems = [];
      return;
    }

    let me = this;
    this.service.getPlacePredictions(
      {
        input: this.property.city
      },
      (predictions, status) => {
        me.autocompleteItems = [];

        me.zone.run(() => {
          if (predictions != null) {
            predictions.forEach(prediction => {
              me.autocompleteItems.push(prediction);
            });
          }
        });
      }
    );
  }

  goback() {
    this.navCtrl.setRoot("PropertieslistviewPage");
  }
  goto(page, index) {
    if (index == 0 || index > 0) {
      // this.navCtrl.push(page, { property_data: data });
      // this.clickEvent(null, null, null);
      this.status = false;
      this.show = false;
      let profileModal = this.modalCtrl.create(page, {
        property_data: {
          index: index,
          property: this.property
        }
      });
      profileModal.present();
      profileModal.onDidDismiss(data => {
        if (data && data.push) {
          this.navCtrl.push(data.currpage, { property_data: this.property });
        } else {
          if (this.property && this.property.property_id) {
            this.getsingledetailpage(this.property.property_id);
          }
        }
      });
    } else {
      this.navCtrl.push(page, { property_data: this.property });
    }
  }

  getsingledetailpage(property_id) {
    this.auth.startloader();
    try {
      this.auth
        .getsinglealldetail(property_id, this.auth.getuserId())
        .subscribe(
          data => {
            if (data) {
              if (data.json().status == 1) {
                this.property = data.json().events;
                this.setdata();
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
            this.auth.stoploader();
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  gotomodel(page) {
    // this.navCtrl.push(page);
    let profileModal = this.modalCtrl.create(page, {
      property_data: this.property
    });
    profileModal.present();
  }

  status: boolean = false;
  show: boolean = false;
  selectproperty: any;
  areaindex: any;
  areaindex1: any;
  clickEvent(property, index, ind) {
    this.status = !this.status;
    this.show = !this.show;
    if (property) {
      this.selectproperty = property;
      this.areaindex = index;
      this.areaindex1 = ind;
    }
  }
  getuserdetail() {
    try {
      this.auth
        .getsinglealldetail(
          this.navParams.get("property_data").property_id,
          this.auth.getuserId()
        )
        .subscribe(
          data => {
            if (data.json()) {
              if (data.json().status == 1) {
                this.property = data.json().events;
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (err) {
      this.auth.errtoast(err);
    }
  }
  deleteselectedslot() {
    if (this.selectedproperty) {
      try {
        this.auth
          .deleteslot(this.auth.getuserId(), this.selectproperty.id)
          .subscribe(
            data => {
              if (data.json()) {
                if (data.json().status == 1) {
                  this.auth.toast(data.json().message);
                  this.clickEvent(null, null, null);
                  this.getuserdetail();
                } else if (data.json().status == 0) {
                  if (data.json().message) {
                    this.auth.toast(data.json().message);
                  } else {
                    this.auth.toast("Cannot delete this slot");
                  }
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                }
              }
            },
            errorHandler => {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.errtoast(errorHandler);
            }
          );
      } catch (err) {
        this.auth.errtoast(err);
      }
    }
  }
  filter(value) {
    var el = this;
    el.placesService.getDetails(
      {
        placeId: value
      },
      (details: any) => {
        this.zone.run(() => {
          for (var srch = 0; srch < details.address_components.length; srch++) {
            if (details.address_components[srch].types[0] == "locality") {
              this.property.city = details.address_components[srch].long_name;
            } else if (details.address_components[srch].types[0] == "country") {
              this.property.country =
                details.address_components[srch].long_name;
            } else if (
              details.address_components[srch].types[0] ==
              "administrative_area_level_2"
            ) {
              this.property.state = details.address_components[srch].long_name;
            } else if (
              details.address_components[srch].types[0] ==
              "administrative_area_level_1"
            ) {
              this.property.state = details.address_components[srch].long_name;
            } else if (
              details.address_components[srch].types[0] == "postal_code"
            ) {
              this.property.zipcode =
                details.address_components[srch].long_name;
            }
          }
        });
      }
    );
  }
}
