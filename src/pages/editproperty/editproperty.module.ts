import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditpropertyPage } from './editproperty';

@NgModule({
  declarations: [
    EditpropertyPage,
  ],
  imports: [
    IonicPageModule.forChild(EditpropertyPage),
  ],
})
export class EditpropertyPageModule {}
