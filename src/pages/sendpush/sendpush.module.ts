import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendpushPage } from './sendpush';

@NgModule({
  declarations: [
    SendpushPage,
  ],
  imports: [
    IonicPageModule.forChild(SendpushPage),
  ],
})
export class SendpushPageModule {}
