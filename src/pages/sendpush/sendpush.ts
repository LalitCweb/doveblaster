import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-sendpush",
  templateUrl: "sendpush.html"
})
export class SendpushPage {
  message: any = "";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    if (this.navParams.get("type") == 1) {
      if (
        this.navParams.get("message") &&
        this.checkurlcontain(this.navParams.get("message"))
      ) {
        this.message = this.replaceURLWithHTMLLinks(
          this.navParams.get("message")
        );
      } else {
        this.message = this.navParams.get("message");
      }
    }
  }

  checkurlcontain(status_text) {
    if (
      new RegExp(
        "([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?"
      ).test(status_text)
    ) {
      return true;
    } else {
      return false;
    }
  }
  replaceURLWithHTMLLinks(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
    return text.replace(exp, "<a href='$1'>$1</a>");
  }
  ionViewDidLoad() {}
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
