import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the ManagewalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-managewallet",
  templateUrl: "managewallet.html"
})
export class ManagewalletPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {}
  ionViewWillEnter() {
    this.getwallet();
  }
  ionViewDidLoad() {}
  data: any = [];
  getwallet() {
    this.auth.getwallet(this.auth.getuserId()).subscribe(
      data => {
        if (data && data.json() && data.json().status == 1) {
          var dataa: any = data.json();
          this.data = dataa.wallet_data;
        }
      },
      errorHandler => {
        this.auth.logout();
        this.navCtrl.setRoot("LoginPage");
        this.auth.errtoast(errorHandler);
      }
    );
  }
}
