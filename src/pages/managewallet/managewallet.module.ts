import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManagewalletPage } from './managewallet';

@NgModule({
  declarations: [
    ManagewalletPage,
  ],
  imports: [
    IonicPageModule.forChild(ManagewalletPage),
  ],
})
export class ManagewalletPageModule {}
