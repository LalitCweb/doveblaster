import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditareainfoPage } from './editareainfo';

@NgModule({
  declarations: [
    EditareainfoPage,
  ],
  imports: [
    IonicPageModule.forChild(EditareainfoPage),
  ],
})
export class EditareainfoPageModule {}
