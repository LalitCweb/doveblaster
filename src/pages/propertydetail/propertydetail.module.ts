import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertydetailPage } from './propertydetail';

@NgModule({
  declarations: [
    PropertydetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PropertydetailPage),
  ],
})
export class PropertydetailPageModule {}
