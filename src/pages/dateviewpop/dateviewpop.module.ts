import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DateviewpopPage } from './dateviewpop';

@NgModule({
  declarations: [
    DateviewpopPage,
  ],
  imports: [
    IonicPageModule.forChild(DateviewpopPage),
  ],
})
export class DateviewpopPageModule {}
