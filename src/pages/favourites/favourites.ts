import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import * as $ from "jquery";

/**
 * Generated class for the FavouritesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-favourites",
  templateUrl: "favourites.html"
})
export class FavouritesPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {}
  ionViewWillEnter() {
    this.auth.startloader();
    this.getallmessages();
  }
  ionViewDidLoad() {}

  favourites: any = [];
  getallmessages() {
    try {
      this.auth.getfavourites(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              var dataa: any = data.json().all_favs_loop;
              $(document).ready(function() {
                $("span")
                  .find("a")
                  .contents()
                  .unwrap();
              });
              this.favourites = dataa;
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (e) {
      this.auth.stoploader();
    }
  }

  favorite(property_id, user_id, index) {
    this.auth.startloader();
    try {
      this.auth.addtofavourite(property_id, user_id).subscribe(data => {
        if (data) {
          this.auth.stoploader();
          var dataa: any = data.json();
          if (dataa.added == false) {
            this.favourites.splice(index, 1);
            this.auth.vibalert();
          } else if (dataa.added == true) {
            this.favourites.is_fav = 1;
            this.auth.vibalert();
          }
        }
      });
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
}
