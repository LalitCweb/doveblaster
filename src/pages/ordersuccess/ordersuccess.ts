import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { TracklocationProvider } from "../../providers/tracklocation/tracklocation";

/**
 * Generated class for the OrdersuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-ordersuccess",
  templateUrl: "ordersuccess.html"
})
export class OrdersuccessPage {
  order_detail: any;
  cart_data: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public tracklocation: TracklocationProvider
  ) {
    if (this.navParams.get("order_detail")) {
      this.order_detail = this.navParams.get("order_detail");
      this.cart_data = this.navParams.get("cart_data");
      if (this.cart_data && this.cart_data.length) {
        for (var crt = 0; crt < this.cart_data.length; crt++) {
          this.removecart(this.cart_data[crt]);
        }
      }
    } else {
    }
  }
  gotohome(page) {
    this.navCtrl.setRoot(page);
    // setTimeout(() => {
    //   window.location.reload;
    // }, 2000);
  }
  removecart(product) {
    try {
      this.auth
        .removeproductcart(this.auth.getuserId(), product.product_id)
        .subscribe(
          data => {
            if (data) {
              if (data.json() && data.json().status === 1) {
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                // this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (e) {
      this.auth.toast(e);
    }
  }
  ionViewDidLoad() {}
}
