import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the VerifyemailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-verifyemail",
  templateUrl: "verifyemail.html"
})
export class VerifyemailPage {
  otp: any;
  signupdata: any;
  usertype: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {
    if (this.navParams.get("value")) {
      this.signupdata = this.navParams.get("value").data;
      this.usertype = this.navParams.get("value").type;
    }
  }

  ionViewDidLoad() {}
  verifyEmail() {
    if (this.otp == "" || this.otp == null || this.otp == undefined) {
      this.auth.toast("Please Enter pin number here");
    } else {
      this.navCtrl.push("EmailverificationPage");
    }
  }
  resendEmail() {
    if (this.otp == "" || this.otp == null || this.otp == undefined) {
      this.auth.toast("Please enter code you received in email");
    } else {
      this.auth.startloader();
      try {
        this.auth
          .signupemailverify(this.navParams.get("user_id"), this.otp)
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data.json()) {
                if (data.json().status == 1) {
                  this.auth.toast(data.json().message);
                  this.navCtrl.push("EmailverificationPage", {
                    user_id: this.navParams.get("user_id"),
                    userpass: this.navParams.get("userpass")
                  });
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                } else if (data.json().status == 0) {
                  if (data.json().message) {
                    this.auth.toast(data.json().message);
                  }
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.toast("Currently not able to signup.");
            }
          );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    }
  }
}
