import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the LandownersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-landowners",
  templateUrl: "landowners.html"
})
export class LandownersPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {}

  ionViewDidLoad() {
    this.getlandowners();
  }

  ////////////////////////
  ///////Land Owners//////
  ////////////////////////
  landownerdesc: any = {};
  getlandowners() {
    this.auth.startloader();
    try {
      this.auth.landowners(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data.json() && data.json().status == 1) {
            this.landownerdesc = data.json();
          }
        },
        errorHandler => {
          this.auth.stoploader();
        }
      );
    } catch (err) {
      this.auth.stoploader();
    }
  }
  goto(page, landowner) {
    this.navCtrl.push(page, { landowner: landowner });
  }
}
