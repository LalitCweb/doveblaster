import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoreviewpopPage } from './moreviewpop';

@NgModule({
  declarations: [
    MoreviewpopPage,
  ],
  imports: [
    IonicPageModule.forChild(MoreviewpopPage),
  ],
})
export class MoreviewpopPageModule {}
