import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ReservationPage } from "./reservation";
import { AgmCoreModules } from "../../agm/core";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [ReservationPage],
  imports: [
    AgmCoreModules,
    IonicPageModule.forChild(ReservationPage),
    PipesModule
  ]
})
export class ReservationPageModule {}
