import {
  Component,
  ViewChild,
  ElementRef,
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController,
  Platform
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { Geolocation } from "@ionic-native/geolocation";
import { TracklocationProvider } from "../../providers/tracklocation/tracklocation";
import { errorHandler } from "@angular/platform-browser/src/browser";

/**
 * Generated class for the ReservationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-reservation",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "reservation.html"
})
export class ReservationPage {
  @ViewChild("map") mapElement: ElementRef;
  @ViewChild("map_canvas") map_canvas_Element: ElementRef;
  drivinglocation: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public modalCtrl: ModalController,
    private geolocation: Geolocation,
    public tracklocation: TracklocationProvider,
    public alertController: AlertController,
    public track: TracklocationProvider,
    public platform: Platform,
    public alertCtrl: AlertController
  ) {
    this.auth.clearcookie();
    this.paged = 0;
    this.reservation = {};
    this.getuserReservation();
  }
  ionViewWillEnter() {
    this.auth.clearcookie();
    this.track.getlocation();
    // this.getlocation();
  }
  ionViewWillLeave() {
    this.auth.clearcookie();
    this.track.stoplocation();
  }
  ionViewDidLoad() {}
  options: any = {
    zoom: 16,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    strokeWeight: 3,
    strokePos: 0
  };
  slotarea: any;
  lat: Number;
  lng: Number;
  map: any;
  display: string = "hidemap";
  selected: any;
  index: any;
  status: any;
  show: any;
  clickEvent(value, index) {
    if (value) {
      this.selected = value;
      this.index = index;
    } else {
      this.selected = {};
      this.index = null;
    }
    this.status = !this.status;
    this.show = !this.show;
  }
  async cancelconfirm() {
    const alert = await this.alertController.create({
      message:
        "Are you Sure to Cancel your this slot booking? It will refund (" +
        this.selected.show_policy.refund_percentage +
        "% and deduct $" +
        this.selected.show_policy.fee_amnt +
        " cancellation charges) according to cancellation policy",
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {}
        },
        {
          text: "Yes",
          handler: () => {
            this.cancelreservation();
          }
        }
      ]
    });

    await alert.present();
  }
  async disputeconfirm() {
    let alert = await this.alertController.create({
      title: "Reason",
      inputs: [
        {
          name: "reason",
          placeholder: "Reason"
        }
      ],
      buttons: [
        {
          text: "Close",
          role: "cancel",
          handler: data => {}
        },
        {
          text: "Ok",
          handler: data => {
            if (data.reason) {
              this.selected.reason = data.reason;
              this.disputereservation();
            } else {
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }
  cancelreservation() {
    this.auth.startloader();
    try {
      this.auth
        .cancelreservation(this.auth.getuserId(), this.selected)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data.json().status == 1) {
              this.auth.toast(data.json().message);
              this.reservation[this.index].can_cancel = null;
              this.reservation[this.index].already_cancelled = true;
              this.clickEvent(null, null);
            }
          },
          errorHandler => {
            this.auth.stoploader();
          }
        );
    } catch (err) {
      this.auth.stoploader();
    }
  }
  disputereservation() {
    this.auth.startloader();
    try {
      this.auth
        .disputereservation(this.auth.getuserId(), this.selected)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data.json().status == 1) {
              this.auth.toast(data.json().message);
              this.reservation[this.index].can_dispute = null;
              this.reservation[this.index].already_disputed = true;
              this.clickEvent(null, null);
            } else if (data.json().status == 0) {
              this.auth.toast(data.json().message);
            }
          },
          errorHandler => {
            this.auth.stoploader();
          }
        );
    } catch (err) {
      this.auth.stoploader();
    }
  }
  goto1(linked_slot_tothis_booking, array_slots_data, lat, lng) {
    this.navCtrl.push("LocationonmapPage", {
      lat: lat,
      lng: lng,
      linked_slot_tothis_booking: linked_slot_tothis_booking,
      array_slots_data: array_slots_data
    });
  }
  goto(linked_slot_tothis_booking, array_slots_data, lat, lng) {
    this.display = "showmap";
    // this.navCtrl.push(page);
    this.lat = lat;
    this.lng = lng;

    for (var x = 0; x < array_slots_data.length; x++) {
      if (array_slots_data[x].slot_id == linked_slot_tothis_booking) {
        this.slotarea = array_slots_data[x];
      }
    }
    var latlng = new google.maps.LatLng(this.lat, this.lng);
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 16,
      center: latlng,
      mapTypeId: "hybrid",
      disableDefaultUI: true,
      tilt: 0,
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_CENTER
      },
      mapTypeControl: true,
      fullscreenControl: false,
      streetViewControl: true,
      streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      }
    });
    if (this.slotarea.shape_type == "rectangle") {
      this.slotarea.slot_coordinates = this.slotarea.slot_coordinates.replace(
        /\\/g,
        ""
      );
      this.slotarea.slot_coordinates = JSON.parse(
        this.slotarea.slot_coordinates
      );
      this.slotarea.slot_coordinates[0].north = parseFloat(
        this.slotarea.slot_coordinates[0].north
      );
      this.slotarea.slot_coordinates[0].south = parseFloat(
        this.slotarea.slot_coordinates[0].south
      );
      this.slotarea.slot_coordinates[0].east = parseFloat(
        this.slotarea.slot_coordinates[0].east
      );
      this.slotarea.slot_coordinates[0].west = parseFloat(
        this.slotarea.slot_coordinates[0].west
      );
      var rect = new google.maps.Rectangle({
        strokeColor: this.slotarea.slot_color,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: this.slotarea.slot_color,
        fillOpacity: 0.2,
        map: this.map,
        bounds: {
          north: this.slotarea.slot_coordinates[0].north,
          south: this.slotarea.slot_coordinates[0].south,
          east: this.slotarea.slot_coordinates[0].east,
          west: this.slotarea.slot_coordinates[0].west
        }
      });
      rect.setMap(this.map);
    } else if (this.slotarea.shape_type == "polygon") {
      if (this.slotarea.slot_coordinate) {
      } else {
        this.slotarea.slot_coordinates = this.slotarea.slot_coordinates.replace(
          /\\/g,
          ""
        );
        this.slotarea.slot_coordinates = JSON.parse(
          this.slotarea.slot_coordinates
        );

        this.slotarea.slot_coordinate = [];
        var cords: any = {};
        if (
          this.slotarea.slot_coordinates &&
          this.slotarea.slot_coordinates.length
        ) {
          for (var y = 0; y < this.slotarea.slot_coordinates.length; y++) {
            if (this.slotarea.slot_coordinates[y]) {
              this.slotarea.slot_coordinates[
                y
              ] = this.slotarea.slot_coordinates[y].split(",");
              cords = {
                lat: parseFloat(this.slotarea.slot_coordinates[y][0]),
                lng: parseFloat(this.slotarea.slot_coordinates[y][1])
              };
              this.slotarea.slot_coordinate.push(cords);
              this.slotarea.slot_coordinates[
                y
              ].lat = this.slotarea.slot_coordinates[y][0];
              this.slotarea.slot_coordinates[
                y
              ].lng = this.slotarea.slot_coordinates[y][1];
            }
          }
        }
      }

      var poly = new google.maps.Polygon({
        paths: this.slotarea.slot_coordinate,
        strokeColor: this.slotarea.slot_color,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: this.slotarea.slot_color,
        fillOpacity: 0.2
      });
      poly.setMap(this.map);
    } else if (this.slotarea.shape_type == "circle") {
      var circle = new google.maps.Circle({
        strokeColor: this.slotarea.slot_color,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: this.slotarea.slot_color,
        fillOpacity: 0.2,
        map: this.map,
        center: {
          lat: parseFloat(this.slotarea.slot_circle_lat),
          lng: parseFloat(this.slotarea.slot_circle_lng)
        },
        radius: parseFloat(this.slotarea.slot_circle_radious)
      });
      circle.setMap(this.map);
    }
    // let profileModal = this.modalCtrl.create("LocationonmapPage", {
    //   lat: lat,
    //   lng: lng,
    //   linked_slot_tothis_booking: linked_slot_tothis_booking,
    //   array_slots_data: array_slots_data
    // });
    // profileModal.present();
  }
  hidemapp() {
    this.display = "hidemap";
  }
  marker: any;
  addMarker(map: any, lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    this.marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: latlng
    });
  }
  updateMarker(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    this.marker.setPosition(latlng);
  }
  private _map: any;
  changeTilt(map) {
    this._map = map;
    this._map.setTilt(0);
  }
  getlocation() {
    let options = {
      enableHighAccuracy: true
    };
    this.geolocation
      .getCurrentPosition(options)
      .then(resp => {
        this.options.lat = resp.coords.latitude;
        this.options.lng = resp.coords.longitude;
        this.addMarker(this.map, this.options.lat, this.options.lng);
      })
      .catch(error => {
        this.auth.errtoast(error);
      });
    setTimeout(() => {
      let watch = this.geolocation.watchPosition(options);
      watch.subscribe(data => {
        // data can be a set of coordinates, or an error (if an error occurred).
        this.options.lat = data.coords.latitude;
        this.options.lng = data.coords.longitude;
        if (this.marker) {
          this.updateMarker(this.options.lat, this.options.lng);
        } else {
          this.addMarker(this.map, this.options.lat, this.options.lng);
        }
      });
    }, 2000);
  }
  gotodriving(
    linked_slot_tothis_booking,
    array_slots_data,
    lat,
    lng,
    drivingdirection
  ) {
    // this.navCtrl.push(page);
    // let profileModal = this.modalCtrl.create("DrivingdirectionsPage", {
    //   lat: lat,
    //   lng: lng,
    //   linked_slot_tothis_booking: linked_slot_tothis_booking,
    //   array_slots_data: array_slots_data,
    //   drivingdirection: drivingdirection
    // });
    // profileModal.present();
    if (this.platform.is("android")) {
      var drivingdirection = drivingdirection.split(",");
      window.open(
        "https://www.google.com/maps/dir/?api=1&origin=" +
          this.track.getlat() +
          "," +
          this.track.getlng() +
          "&destination=" +
          parseFloat(drivingdirection[0]) +
          "," +
          parseFloat(drivingdirection[1]) +
          "&travelmode=driving",
        "_blank",
        "location=no"
      );
    }
  }

  paged: any = 0;
  reservation: any = {};
  getuserReservation() {
    this.paged = this.paged + 1;
    if (this.paged == 1) {
      this.auth.startloader();
    }
    try {
      this.auth.reservation(this.auth.getuserId(), this.paged, 5).subscribe(
        data => {
          if (this.paged == 1) {
            this.auth.stoploader();
          }
          if (data && data.json() && data.json().status == 1) {
            if (this.paged == 1 && data.json().user_reservation_data) {
              if (data.json().user_reservation_data.while_loop_data) {
                this.reservation = data.json().user_reservation_data.while_loop_data;
                var cords: any = {};
                if (
                  data.json().user_reservation_data.while_loop_data &&
                  data.json().user_reservation_data.while_loop_data.length
                ) {
                  for (
                    var x = 0;
                    x <
                    data.json().user_reservation_data.while_loop_data.length;
                    x++
                  ) {
                    if (
                      this.reservation[x].property_details
                        .prop_area_shape_type == "polygon"
                    ) {
                      if (
                        typeof this.reservation[x].property_details
                          .prop_area_shape_cords == "string"
                      ) {
                        this.reservation[
                          x
                        ].property_details.prop_area_shape_cords = this.reservation[
                          x
                        ].property_details.prop_area_shape_cords.replace(
                          /\\/g,
                          ""
                        );
                        this.reservation[
                          x
                        ].property_details.prop_area_shape_cords = JSON.parse(
                          this.reservation[x].property_details
                            .prop_area_shape_cords
                        );
                        for (
                          var pz = 0;
                          pz <
                          this.reservation[x].property_details
                            .prop_area_shape_cords.length;
                          pz++
                        ) {
                          var pole: any = {};
                          this.reservation[
                            x
                          ].property_details.prop_area_shape_cords[
                            pz
                          ] = this.reservation[
                            x
                          ].property_details.prop_area_shape_cords[pz].split(
                            ","
                          );
                          pole = {
                            lat: parseFloat(
                              this.reservation[x].property_details
                                .prop_area_shape_cords[pz][0]
                            ),
                            lng: parseFloat(
                              this.reservation[x].property_details
                                .prop_area_shape_cords[pz][1]
                            )
                          };
                          if (pz == 0) {
                            this.reservation[
                              x
                            ].property_details.prop_area_shape_cord = [];
                          }
                          this.reservation[
                            x
                          ].property_details.prop_area_shape_cord.push(pole);
                        }
                      }
                    }
                    for (
                      var y = 0;
                      y < this.reservation[x].array_slots_data.length;
                      y++
                    ) {
                      if (
                        this.reservation[x].array_slots_data[y].shape_type ==
                        "polygon"
                      ) {
                        this.reservation[x].array_slots_data[
                          y
                        ].slot_coordinates = this.reservation[
                          x
                        ].array_slots_data[y].slot_coordinates.replace(
                          /\\/g,
                          ""
                        );
                        this.reservation[x].array_slots_data[
                          y
                        ].slot_coordinates = JSON.parse(
                          this.reservation[x].array_slots_data[y]
                            .slot_coordinates
                        );
                        if (
                          this.reservation[x].array_slots_data[y]
                            .slot_coordinates &&
                          this.reservation[x].array_slots_data[y]
                            .slot_coordinates.length
                        ) {
                          for (
                            var z = 0;
                            z <
                            this.reservation[x].array_slots_data[y]
                              .slot_coordinates.length;
                            z++
                          ) {
                            this.reservation[x].array_slots_data[
                              y
                            ].slot_coordinates[z] = this.reservation[
                              x
                            ].array_slots_data[y].slot_coordinates[z].split(
                              ","
                            );
                            cords = {
                              lat: parseFloat(
                                this.reservation[x].array_slots_data[y]
                                  .slot_coordinates[z][0]
                              ),
                              lng: parseFloat(
                                this.reservation[x].array_slots_data[y]
                                  .slot_coordinates[z][1]
                              )
                            };
                            if (z == 0) {
                              this.reservation[x].array_slots_data[
                                y
                              ].slot_coordinate = [];
                            }
                            this.reservation[x].array_slots_data[
                              y
                            ].slot_coordinate.push(cords);
                          }
                        }
                      }
                    }
                  }
                }
              }
            } else {
              if (
                data.json().user_reservation_data &&
                data.json().user_reservation_data.while_loop_data &&
                data.json().user_reservation_data.while_loop_data.length
              ) {
                // this.reservation = data.json().user_reservation_data.while_loop_data;
                var cordss: any = {};
                for (
                  var xxy = 0;
                  xxy <
                  data.json().user_reservation_data.while_loop_data.length;
                  xxy++
                ) {
                  var tis = this;
                  tis.reservation.push(
                    data.json().user_reservation_data.while_loop_data[xxy]
                  );
                  if (
                    xxy ==
                      data.json().user_reservation_data.while_loop_data.length -
                        1 &&
                    tis.reservation &&
                    tis.reservation.length
                  ) {
                    for (
                      var xx =
                        tis.reservation.length -
                        data.json().user_reservation_data.while_loop_data
                          .length;
                      xx < tis.reservation.length;
                      xx++
                    ) {
                      for (
                        var yy = 0;
                        yy < tis.reservation[xx].array_slots_data.length;
                        yy++
                      ) {
                        if (
                          tis.reservation[xx].array_slots_data[yy].shape_type ==
                          "polygon"
                        ) {
                          tis.reservation[xx].array_slots_data[
                            yy
                          ].slot_coordinates = tis.reservation[
                            xx
                          ].array_slots_data[yy].slot_coordinates.replace(
                            /\\/g,
                            ""
                          );
                          tis.reservation[xx].array_slots_data[
                            yy
                          ].slot_coordinates = JSON.parse(
                            tis.reservation[xx].array_slots_data[yy]
                              .slot_coordinates
                          );

                          if (
                            tis.reservation[xx].array_slots_data[yy]
                              .slot_coordinates &&
                            tis.reservation[xx].array_slots_data[yy]
                              .slot_coordinates.length
                          ) {
                            for (
                              var zz = 0;
                              zz <
                              tis.reservation[xx].array_slots_data[yy]
                                .slot_coordinates.length;
                              zz++
                            ) {
                              tis.reservation[xx].array_slots_data[
                                yy
                              ].slot_coordinates[zz] = tis.reservation[
                                xx
                              ].array_slots_data[yy].slot_coordinates[zz].split(
                                ","
                              );
                              cordss = {
                                lat: parseFloat(
                                  tis.reservation[xx].array_slots_data[yy]
                                    .slot_coordinates[zz][0]
                                ),
                                lng: parseFloat(
                                  tis.reservation[xx].array_slots_data[yy]
                                    .slot_coordinates[zz][1]
                                )
                              };
                              if (zz == 0) {
                                tis.reservation[xx].array_slots_data[
                                  yy
                                ].slot_coordinate = [];
                              }
                              tis.reservation[xx].array_slots_data[
                                yy
                              ].slot_coordinate.push(cordss);
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          }
        },
        errorHandler => {
          if (this.paged == 1) {
            this.auth.stoploader();
          }
          // this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
          this.confirmAlertreser();
        }
      );
    } catch (err) {
      if (this.paged == 1) {
        this.auth.stoploader();
      }
      this.confirmAlertreser();
      // this.auth.errtoast(err);
    }
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      infiniteScroll.complete();
      this.getuserReservation();
    }, 500);
  }

  confirmAlertreser() {
    let alert = this.alertCtrl.create({
      title: "Try again",
      message: "Not able to get reservations listing!",
      buttons: [
        {
          text: "OK",
          handler: () => {
            this.getuserReservation();
          }
        }
      ]
    });
    alert.present();
  }
}
