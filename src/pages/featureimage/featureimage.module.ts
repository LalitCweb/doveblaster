import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FeatureimagePage } from "./featureimage";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [FeatureimagePage],
  imports: [ComponentsModule, IonicPageModule.forChild(FeatureimagePage)]
})
export class FeatureimagePageModule {}
