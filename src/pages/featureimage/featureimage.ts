import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  ModalController
} from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";

@IonicPage()
@Component({
  selector: "page-featureimage",
  templateUrl: "featureimage.html"
})
export class FeatureimagePage {
  intial: any = 0;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sanitizer: DomSanitizer,
    private viewCtrl: ViewController,
    public modalCtrl: ModalController
  ) {
    if (
      this.navParams.get("initial") == 0 ||
      this.navParams.get("initial") > 0
    ) {
      this.intial = this.navParams.get("initial");
    } else {
      this.intial = 0;
    }
  }
  media: any;
  video: any;
  ionViewDidLoad() {
    this.media = this.navParams.get("medias");
    if (this.navParams.get("video")) {
      this.video = this.navParams.get("video");
      if (this.video) {
        this.makeurl();
      }
    }
  }

  makeurl() {
    if (this.video.embed_video_type === "youtube") {
      this.video.embed_video_id = this.sanitizer.bypassSecurityTrustResourceUrl(
        "https://www.youtube.com/embed/" + this.video.embed_video_id
      );
    } else if (this.video.embed_video_type === "vimeo") {
      this.video.embed_video_id = this.sanitizer.bypassSecurityTrustResourceUrl(
        "https://vimeo.com/" + this.video.embed_video_id
      );
    }
  }
  goto(page, media) {
    let mediaModal = this.modalCtrl.create(page, {
      medias: media,
      video: this.navParams.get("video")
    });
    mediaModal.present();

    mediaModal.onDidDismiss(data => {
      if (data.medias) {
        this.media = data.medias;
      }
      if (data.video) {
        this.video = data.video;
        this.makeurl();
      }
    });
    this.goback();
  }
  goback() {
    this.viewCtrl.dismiss({ medias: this.media, video: this.video });
  }
}
