import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddnewdatePage } from './addnewdate';

@NgModule({
  declarations: [
    AddnewdatePage,
  ],
  imports: [
    IonicPageModule.forChild(AddnewdatePage),
  ],
})
export class AddnewdatePageModule {}
