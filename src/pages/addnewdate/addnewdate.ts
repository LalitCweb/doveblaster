import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the AddnewdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-addnewdate",
  templateUrl: "addnewdate.html"
})
export class AddnewdatePage {
  slot1 = {
    start_date: "",
    end_date: "",
    price: ""
  };
  myForm1: FormGroup;
  property: any;
  index: string;
  max: any = "2099-10-31";
  min: any = new Date().toISOString();
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    public auth: AuthProvider,
    public viewCtrl: ViewController
  ) {
    this.myForm1 = this.fb.group({
      price: ["", Validators.required],
      start_date: ["", Validators.required],
      end_date: ["", Validators.required]
    });
    this.property = this.navParams.get("property_data");
    this.index = this.navParams.get("index");
  }

  ionViewDidLoad() {}
  chngedate() {
    this.myForm1.value.end_date = null;
  }
  addareas2() {
    if (this.myForm1.valid) {
      this.savedata();
    } else {
      this.auth.toast("Please fill complete form");
    }
  }

  goback() {
    this.viewCtrl.dismiss();
  }
  savedata() {
    this.auth.startloader();
    try {
      this.auth
        .editstep32(
          this.auth.getuserId(),
          this.property.property_id,
          this.property.all_areas[this.index].area_id,
          this.myForm1.value.start_date,
          this.myForm1.value.end_date,
          this.myForm1.value.price
        )
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              if (data.json().status == 1) {
                this.auth.stoploader();
                this.myForm1.reset();
                var dta: any = {
                  property: this.property,
                  index: this.index
                };
                this.viewCtrl.dismiss(dta);
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
}
