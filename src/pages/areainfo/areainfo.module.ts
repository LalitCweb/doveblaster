import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AreainfoPage } from './areainfo';

@NgModule({
  declarations: [
    AreainfoPage,
  ],
  imports: [
    IonicPageModule.forChild(AreainfoPage),
  ],
})
export class AreainfoPageModule {}
