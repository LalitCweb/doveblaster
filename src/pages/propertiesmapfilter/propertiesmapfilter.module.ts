import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertiesmapfilterPage } from './propertiesmapfilter';

@NgModule({
  declarations: [
    PropertiesmapfilterPage,
  ],
  imports: [
    IonicPageModule.forChild(PropertiesmapfilterPage),
  ],
})
export class PropertiesmapfilterPageModule {}
