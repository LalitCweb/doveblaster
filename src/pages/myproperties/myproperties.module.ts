import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MypropertiesPage } from "./myproperties";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [MypropertiesPage],
  imports: [IonicPageModule.forChild(MypropertiesPage), PipesModule]
})
export class MypropertiesPageModule {}
