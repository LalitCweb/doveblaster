import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  AlertController
} from "ionic-angular";
import * as $ from "jquery";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the MypropertiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-myproperties",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "myproperties.html"
})
export class MypropertiesPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public auth: AuthProvider,
    public alertController: AlertController
  ) {}

  ionViewDidLoad() {}
  ionViewWillEnter() {
    this.getpropertydetail();
  }
  goto(page) {
    this.navCtrl.push(page, { property_data: this.selected });
    this.clickEvent(null, null);
  }

  status: boolean = false;
  show: boolean = false;
  selected: any;
  index: any;
  clickEvent(value, index) {
    if (value) {
      this.selected = value;
      this.index = index;
    } else {
      this.selected = {};
      this.index = null;
    }
    this.status = !this.status;
    this.show = !this.show;
  }

  myproperties: any;
  getpropertydetail() {
    this.auth.startloader();
    try {
      // console.log("called");
      this.auth.myproperties(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              this.myproperties = data.json().properties;
              $(document).ready(function() {
                $("span")
                  .find("a")
                  .contents()
                  .unwrap();
              });
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          // this.dummydata();
        }
      );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
      // this.dummydata();
    }
  }

  async getpropertydetail1() {
    this.auth
      .myproperties(this.auth.getuserId())
      .subscribe(res => {}, err => {});
  }

  async presentAlertConfirm(data) {
    var message: any;
    if (data == "delete") {
      message = "Are you sure you want to delete this property";
    } else if (data == "disable") {
      message = "Are you sure you want to disable this property";
    } else if (data == "enable") {
      message = "Are you sure you want to publish this property";
    } else if (data == "feature") {
      message = "Are you sure you want to delete this property";
    }
    const alert = await this.alertController.create({
      message: message,
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {}
        },
        {
          text: "Yes",
          handler: () => {
            if (data == "delete") {
              this.deleteproperty();
            } else if (data == "disable") {
              this.disableproperty();
            } else if (data == "enable") {
              this.disableproperty();
            } else if (data == "feature") {
              this.featureproperty();
            }
          }
        }
      ]
    });

    await alert.present();
  }

  deleteproperty() {
    if (this.selected) {
      try {
        this.auth.startloader();
        this.auth
          .deleteproperties(this.auth.getuserId(), this.selected.property_id)
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data) {
                if (data.json().status == 1) {
                  this.auth.toast(data.json().properties);
                  this.myproperties.splice(this.index, 1);
                  this.clickEvent(null, null);
                } else if (data.json().status == 0) {
                  this.auth.toast(data.json().message);
                  this.clickEvent(null, null);
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.errtoast(errorHandler);
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
            }
          );
      } catch (e) {
        this.auth.stoploader();
        this.auth.toast(e);
      }
    }
  }

  disableproperty() {
    if (this.selected) {
      try {
        this.auth.startloader();
        this.auth
          .disableproperties(this.auth.getuserId(), this.selected.property_id)
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data) {
                if (data.json().status == 1) {
                  if (
                    this.myproperties[this.index].property_status == "publish"
                  ) {
                    this.myproperties[this.index].property_status = "disabled";
                  } else if (
                    this.myproperties[this.index].property_status == "disabled"
                  ) {
                    this.myproperties[this.index].property_status = "publish";
                  }
                  this.clickEvent(null, null);
                } else if (data.json().status == 0) {
                  this.auth.toast(data.json().message);
                  this.clickEvent(null, null);
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.errtoast(errorHandler);
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
            }
          );
      } catch (e) {
        this.auth.stoploader();
        this.auth.toast(e);
      }
    }
  }

  featureproperty() {
    if (this.selected) {
      try {
        this.auth.startloader();
        this.auth
          .featureproperties(this.auth.getuserId(), this.selected.property_id)
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data) {
                if (data.json().status == 1) {
                  this.myproperties[this.index].prop_featured = "1";
                  this.clickEvent(null, null);
                } else if (data.json().status == 0) {
                  this.auth.toast(data.json().message);
                  this.clickEvent(null, null);
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                }
              }
            },
            errorHandler => {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.errtoast(errorHandler);
            }
          );
      } catch (e) {
        this.auth.stoploader();
        this.auth.toast(e);
      }
    }
  }
}
