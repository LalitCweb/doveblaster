import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

/**
 * Generated class for the LandownerdtPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-landownerdt",
  templateUrl: "landownerdt.html"
})
export class LandownerdtPage {
  landown: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.landown = this.navParams.get("landowner");
  }

  ionViewDidLoad() {}
}
