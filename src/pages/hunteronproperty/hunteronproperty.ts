import {
  Component,
  ViewChild,
  ElementRef,
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  AlertController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { FcmProvider } from "../../providers/fcm/fcm";
import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from "rxjs";
import { Geolocation } from "@ionic-native/geolocation";
import * as firebase from "Firebase";
import { DatePipe } from "@angular/common";

@IonicPage()
@Component({
  selector: "page-hunteronproperty",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "hunteronproperty.html"
})
export class HunteronpropertyPage {
  options: any = {
    zoom: 16,
    lat: 0,
    lng: 0,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    strokeWeight: 3,
    strokePos: 0,
    image: "assets/images/red.png"
  };
  devices: any;
  check: any;
  checkSubscribe: any;
  @ViewChild("map") mapElement: ElementRef;
  map: any;
  ref = firebase.database().ref("/dayleasing_data/");
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public fcmp: FcmProvider,
    public afd: AngularFireDatabase,
    public platform: Platform,
    public datepipe: DatePipe,
    public alertCtrl: AlertController
  ) {
    if (this.auth.isOwner()) {
      var property_id: any;
      if (this.navParams.get("property_detail")) {
        property_id = this.navParams.get("property_detail").property_id;
        this.subscribeagain(property_id);
        this.propertydetail(
          property_id,
          this.datepipe.transform(new Date(), "yyyy-MM-dd")
        );
      }
    }

    platform.ready().then(() => {
      this.initMap();
    });
  }
  devicesSubscribe: any;
  subscribeagain(property_id) {
    var dd: any = this.fcmp.checkproperty(property_id).valueChanges();
    var ee = this;
    dd.subscribe(items => {
      ee.devices = items;
      console.log(ee.devices);
    });
  }
  ionViewDidLoad() {}
  ionViewWillLeave() {
    if (this.checkSubscribe) {
      this.checkSubscribe.unsubscribe();
    }
    if (this.devicesSubscribe) {
      this.devicesSubscribe.unsubscribe();
    }
  }
  //////////////////////////////////
  ///////////Check time/////////////
  /////////////////////////////////
  checktime(marker) {
    var timeDiff = Math.abs(
      new Date().getTime() - new Date(marker.datetime).getTime()
    );
    var diffDays = Math.ceil(timeDiff / 1000);
    if (diffDays > 180) {
      return false;
    } else {
      return true;
    }
  }
  ///////////////////////////
  ///////////Map/////////////
  ///////////////////////////
  markers = [];
  initMap() {}

  addMarker(location) {
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: "assets/images/red.png"
    });
    this.markers.push(marker);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }

  updateGeolocation(lat, lng) {}

  /******************/
  /***Touch Events***/
  /******************/
  touchstart(e) {
    if (e.cancelable) {
      e.preventDefault();
    }
  }
  ////////////////////////////////
  ////////Property Detail/////////
  ///////////////////////////////
  slotarea: any;
  option: any;
  propertydetail(property, date) {
    date = this.datepipe.transform(date, "yyyy-MM-dd");
    this.auth.startloader();
    try {
      this.auth.getsinglealldetail(property, this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            this.auth.stoploader();
            if (data.json().status == 1) {
              this.slotarea = data.json().events;
              this.options.lat = this.auth.convertToint(
                data.json().events.prop_area_center_lat
              );
              this.options.lng = this.auth.convertToint(
                data.json().events.prop_area_center_lng
              );
              var cords: any = {};
              if (this.slotarea.prop_area_shape_type == "polygon") {
                this.slotarea.prop_area_shape_cords = this.slotarea.prop_area_shape_cords.replace(
                  /\\/g,
                  ""
                );
                this.slotarea.prop_area_shape_cords = JSON.parse(
                  this.slotarea.prop_area_shape_cords
                );
                if (
                  this.slotarea.prop_area_shape_cords &&
                  this.slotarea.prop_area_shape_cords.length
                ) {
                  for (
                    var z = 0;
                    z < this.slotarea.prop_area_shape_cords.length;
                    z++
                  ) {
                    this.slotarea.prop_area_shape_cords[
                      z
                    ] = this.slotarea.prop_area_shape_cords[z].split(",");
                    cords = {
                      lat: parseFloat(
                        this.slotarea.prop_area_shape_cords[z][0]
                      ),
                      lng: parseFloat(this.slotarea.prop_area_shape_cords[z][1])
                    };
                    if (z == 0) {
                      this.slotarea.prop_area_shape_cord = [];
                    }
                    this.slotarea.prop_area_shape_cord.push(cords);
                  }
                }
              }

              /////////////////////////
              /////////Slots///////////
              /////////////////////////
              for (
                var slts = 0;
                slts < this.slotarea.all_areas.length;
                slts++
              ) {
                if (
                  this.slotarea.all_areas[slts].slots_full_details &&
                  this.slotarea.all_areas[slts].slots_full_details.length
                ) {
                  for (
                    var slt = 0;
                    slt <
                    this.slotarea.all_areas[slts].slots_full_details.length;
                    slt++
                  ) {
                    var cords: any = {};
                    if (
                      this.slotarea.all_areas[slts].slots_full_details[slt]
                        .shapetype == "polygon"
                    ) {
                      this.slotarea.all_areas[slts].slots_full_details[
                        slt
                      ].slot_coordinates = this.slotarea.all_areas[
                        slts
                      ].slots_full_details[slt].slot_coordinates.replace(
                        /\\/g,
                        ""
                      );
                      this.slotarea.all_areas[slts].slots_full_details[
                        slt
                      ].slot_coordinates = JSON.parse(
                        this.slotarea.all_areas[slts].slots_full_details[slt]
                          .slot_coordinates
                      );
                      if (
                        this.slotarea.all_areas[slts].slots_full_details[slt]
                          .slot_coordinates &&
                        this.slotarea.all_areas[slts].slots_full_details[slt]
                          .slot_coordinates.length
                      ) {
                        for (
                          var z = 0;
                          z <
                          this.slotarea.all_areas[slts].slots_full_details[slt]
                            .slot_coordinates.length;
                          z++
                        ) {
                          this.slotarea.all_areas[slts].slots_full_details[
                            slt
                          ].slot_coordinates[z] = this.slotarea.all_areas[
                            slts
                          ].slots_full_details[slt].slot_coordinates[z].split(
                            ","
                          );
                          cords = {
                            lat: parseFloat(
                              this.slotarea.all_areas[slts].slots_full_details[
                                slt
                              ].slot_coordinates[z][0]
                            ),
                            lng: parseFloat(
                              this.slotarea.all_areas[slts].slots_full_details[
                                slt
                              ].slot_coordinates[z][1]
                            )
                          };
                          if (z == 0) {
                            this.slotarea.all_areas[slts].slots_full_details[
                              slt
                            ].slot_coordinate = [];
                          }
                          this.slotarea.all_areas[slts].slots_full_details[
                            slt
                          ].slot_coordinate.push(cords);
                        }
                      }
                    }
                  }
                }
              }
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  private _map: any;
  changeTilt(map) {
    this._map = map;
    this._map.setTilt(0);
    map.setOptions({
      zoomControl: "true",
      zoomControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      streetViewControl: "true",
      streetViewControlOptions: {
        position: google.maps.ControlPosition.TOP_LEFT
      }
    });
  }
  getoken(token) {
    this.sendtoken(token);
  }
  sendtokenall() {
    const prompt = this.alertCtrl.create({
      title: "Notification",
      message: "Send message to hunters",
      inputs: [
        {
          name: "body",
          placeholder: "Message"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          handler: data => {}
        },
        {
          text: "Send",
          handler: data => {
            if (data.body) {
              var tkn = this;
              var dev: any = this.fcmp
                .checkproperty(
                  this.navParams.get("property_detail").property_id
                )
                .valueChanges();
              var devsubs = dev.subscribe(queriedItems => {
                queriedItems.forEach(function(tokens, index) {
                  if (tkn.checktime(tokens)) {
                    tkn.sendNotificationall(
                      queriedItems.length,
                      index,
                      tokens.token,
                      data.body
                    );
                  }
                  if (index == queriedItems.length - 1) {
                    if (devsubs) {
                      devsubs.unsubscribe();
                    }
                  }
                });
              });
            } else {
              this.auth.toast("Please type your message");
            }
          }
        }
      ]
    });
    prompt.present();
  }
  selecteduser: any = {};
  sendtoken(token) {
    this.selecteduser.token = token.token;
    const prompt = this.alertCtrl.create({
      title: "Notification",
      message: "Send message to " + token.userName,
      inputs: [
        {
          name: "body",
          placeholder: "Message"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          handler: data => {}
        },
        {
          text: "Send",
          handler: data => {
            if (data.body) {
              this.sendNotification(data.body);
            } else {
              this.auth.toast("Please type your message");
            }
          }
        }
      ]
    });
    prompt.present();
  }
  title: any;
  body: any;
  sendNotification(body) {
    this.auth.startloader();
    try {
      this.auth.sendpushnotification(this.selecteduser.token, body).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            this.title = "";
            this.body = "";
            var dataa: any = data.json();
            if (dataa.success == 1) {
              this.auth.toast("Notification send successfully");
            } else {
              this.auth.toast("Not able to send notification");
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  sendNotificationall(item, index, token, body) {
    if (body) {
      this.auth.startloader();
      try {
        this.auth.sendpushnotification(token, body).subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              this.title = "";
              this.body = "";
              var dataa: any = data.json();
              if (dataa.success == 1 && index == item - 1) {
                this.auth.toast("Notification send successfully");
              } else if (index == item - 1) {
                this.auth.toast("Not able to send notification");
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
          }
        );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    } else {
      this.auth.toast("Please type your message");
    }
  }

  cancelnotification() {
    this.selecteduser = {};
  }
}
