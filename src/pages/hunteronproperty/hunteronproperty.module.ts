import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { HunteronpropertyPage } from "./hunteronproperty";
import { AgmCoreModules } from "../../agm/core";

@NgModule({
  declarations: [HunteronpropertyPage],
  imports: [AgmCoreModules, IonicPageModule.forChild(HunteronpropertyPage)]
})
export class HunteronpropertyPageModule {}
