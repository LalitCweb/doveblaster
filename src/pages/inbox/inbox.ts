import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the InboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-inbox",
  templateUrl: "inbox.html"
})
export class InboxPage {
  reply: any = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {}

  ionViewWillEnter() {
    this.offset = 0;
    this.messages = [];
    this.auth.startloader();
    this.getallmessages();
  }
  ionViewDidLoad() {}

  offset: any = 0;
  messages: any = [];
  unreadmsgs: any;
  getallmessages() {
    try {
      this.offset = this.offset + 1;
      this.auth.getallmessage(this.offset, this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data && data.json() && data.json().status == 1) {
            var dataa: any = data.json().messages.final_messages_data[1]
              .messages;
            this.unreadmsgs = data.json().messages.final_messages_data[0].total_unread;
            for (var x = 0; x < dataa.length; x++) {
              this.messages.push(dataa[x]);
            }
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      infiniteScroll.complete();
      this.getallmessages();
    }, 500);
  }

  removemsg(mesg_id, index) {
    this.auth.startloader();
    try {
      this.auth.deletemessage(mesg_id, this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json() && data.json().status == 1) {
              this.messages.splice(index, 1);
              this.auth.vibalert();
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  replymessage(mesg_id, title, content, index) {
    if (mesg_id && title && content) {
      this.auth.startloader();
      try {
        this.auth
          .replymessage(this.auth.getuserId(), mesg_id, title, content)
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data) {
                if (data.json() && data.json().status == 1) {
                  var cord: any = {
                    content: content,
                    from: this.auth.getUsername("username"),
                    title: title
                  };
                  if (this.messages[index].arry_messages_replied_loop == null) {
                    this.messages[index].arry_messages_replied_loop = [];
                    this.messages[index].arry_messages_replied_loop.push(cord);
                  } else {
                    this.messages[index].arry_messages_replied_loop.push(cord);
                  }
                  this.messages[index].reply = false;
                  this.messages.title = "";
                  this.messages.content == "";
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                } else if (data.json() && data.json().status === 0) {
                  this.auth.clearcookie();
                  this.auth.toast(data.json().message);
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.errtoast(errorHandler);
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
            }
          );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    } else {
      this.auth.toast("Title and Message is mandatory to Reply.");
    }
  }
}
