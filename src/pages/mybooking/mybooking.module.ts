import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MybookingPage } from "./mybooking";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [MybookingPage],
  imports: [IonicPageModule.forChild(MybookingPage), PipesModule]
})
export class MybookingPageModule {}
