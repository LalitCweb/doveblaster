import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController
} from "ionic-angular";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";
import * as $ from "jquery";

/**
 * Generated class for the EntercodeverifyemailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-entercodeverifyemail",
  templateUrl: "entercodeverifyemail.html"
})
export class EntercodeverifyemailPage implements OnInit {
  // Validation form group definition
  phonumber: any;
  configs: FormGroup;
  user: any = { number1: "", number2: "", number3: "", number4: "" };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public auth: AuthProvider
  ) {
    // $(document).ready(function() {});
    if (this.navParams.get("phonumber")) {
      this.phonumber = this.navParams
        .get("phonumber")
        .substr(this.navParams.get("phonumber").length - 4);
    }
  }
  ngOnInit() {
    // Validations Definition
    this.configs = new FormGroup({
      number1: new FormControl("", [Validators.required]),
      number2: new FormControl("", [Validators.required]),
      number3: new FormControl("", [Validators.required]),
      number4: new FormControl("", [Validators.required])
    });

    $(document).ready(function() {
      $("input").keyup(function() {
        if ($(this).val().length == $(this).attr("maxlength")) {
          $(this)
            .next()
            .focus();
        } else if ($(this).val().length + 1 == $(this).attr("maxlength")) {
          $(this)
            .prev()
            .focus();
        }
      });
    });
  }
  ionViewDidLoad() {}
  verifyOtp() {
    if (this.configs.valid) {
      // this.navCtrl.push("LoginPage");
      this.resendEmail(
        this.configs.value.number1 +
          this.configs.value.number2 +
          this.configs.value.number3 +
          this.configs.value.number4
      );
    } else {
      this.auth.toast("Please provide otp send to your mobile number");
    }
  }
  resendEmail(otp) {
    this.auth.startloader();
    try {
      this.auth
        .signupphoneverify2(this.navParams.get("user_id"), otp)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data.json()) {
              if (data.json().status == 1) {
                if (data.json().message) {
                  this.auth.toast(data.json().message);
                }
                this.navCtrl.setRoot("LoginPage", {
                  userpass: this.navParams.get("userpass")
                });
                // this.makelogin()
              } else if (data.json().status == 0) {
                if (data.json().message) {
                  this.auth.toast(data.json().message);
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.toast("Currently not able to signup.");
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  makelogin() {}
}
