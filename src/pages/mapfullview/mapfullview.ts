import {
  Component,
  ViewChild,
  ElementRef,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ViewController,
  Platform
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { DatePipe } from "@angular/common";
import { IMyDpOptions } from "mydatepicker";
/**
 * Generated class for the MapfullviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;
@IonicPage()
@Component({
  selector: "page-mapfullview",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "mapfullview.html"
})
export class MapfullviewPage {
  @ViewChild("map_canvas") mapElements: ElementRef;
  myDatePickerOptions: IMyDpOptions = {
    // other options...
    // editableDateField: true,
    dateFormat: "mm-dd-yyyy",
    disableDateRanges: [
      {
        begin: { year: 1900, month: 1, day: 1 },
        end: { year: 2999, month: 12, day: 31 }
      }
    ], // disable all
    enableDays: []
  };

  // Initialized to specific date (09.10.2018).

  model: any = {
    date: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate()
    }
  };
  map: any;
  map_canvas: any;
  marker = "assets/images/stillmarker.gif";
  myDate: any;
  property: any = {};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public datepipe: DatePipe,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public platform: Platform
  ) {
    this.property = this.navParams.get("property_data");
    this.added = this.navParams.get("cart_data");
    if (this.navParams.get("property_date")) {
      this.myDate = this.navParams.get("property_date");
    } else {
      this.myDate = new Date().toISOString();
    }

    var el: any = this;
    this.getcart();
    this.propertydetail(this.property.ID, this.myDate);
    // if (this.property.prop_area_shape_type == "rectangle") {
    //   if (this.property.prop_area_shape_cords) {
    //     this.property.prop_area_shape_cords = JSON.parse(
    //       this.property.prop_area_shape_cords
    //     );
    //     var rect = new google.maps.Rectangle({
    //       strokeColor: this.property.prop_area_shape_fillcolor,
    //       strokeOpacity: 0.8,
    //       strokeWeight: 2,
    //       fillColor: this.property.prop_area_shape_fillcolor,
    //       fillOpacity: 0.35,
    //       map: this.map_canvas,
    //       bounds: {
    //         north: this.property.prop_area_shape_cords[0].north,
    //         south: this.property.prop_area_shape_cords[0].south,
    //         east: this.property.prop_area_shape_cords[0].east,
    //         west: this.property.prop_area_shape_cords[0].west
    //       }
    //     });
    //     rect.setMap(this.map_canvas);
    //   }
    // }
    // if (this.property.prop_area_shape_type == "polygon") {
    //   try {
    //     this.property.prop_area_shape_cords = JSON.parse(
    //       this.property.prop_area_shape_cords
    //     );
    //   } catch (e) {
    //     return;
    //   }
    //   this.property.prop_area_shape_cord = [];
    //   var cords: any = {};
    //   if (
    //     this.property.prop_area_shape_cords &&
    //     this.property.prop_area_shape_cords.length
    //   ) {
    //     for (var x = 0; x < this.property.prop_area_shape_cords.length; x++) {
    //       try {
    //         this.property.prop_area_shape_cords[
    //           x
    //         ] = this.property.prop_area_shape_cords[x].split(",");
    //       } catch (e) {
    //         // return;
    //       }
    //       cords = {
    //         lat: parseFloat(this.property.prop_area_shape_cords[x][0]),
    //         lng: parseFloat(this.property.prop_area_shape_cords[x][1])
    //       };
    //       this.property.prop_area_shape_cord.push(cords);
    //       this.property.prop_area_shape_cords[
    //         x
    //       ].lat = this.property.prop_area_shape_cords[x][0];
    //       this.property.prop_area_shape_cords[
    //         x
    //       ].lng = this.property.prop_area_shape_cords[x][1];
    //     }
    //     var poly = new google.maps.Polygon({
    //       paths: this.property.prop_area_shape_cord,
    //       strokeColor: this.property.prop_area_shape_fillcolor,
    //       strokeOpacity: 0.8,
    //       strokeWeight: 2,
    //       fillColor: this.property.prop_area_shape_fillcolor,
    //       fillOpacity: 0.35
    //     });
    //     poly.setMap(this.map_canvas);
    //   }
    // }
  }
  ngOnInit() {}
  fullview() {
    var prop: any = {
      property: this.property,
      mydate: this.myDate,
      cart: this.added
    };
    this.viewCtrl.dismiss(prop);
  }

  private _map: any;
  agm_map: any;
  changeTilt(map) {
    this._map = map;
    this._map.setTilt(0);
    this.agm_map = map;
    map.setOptions({
      zoomControl: "true",
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      streetViewControl: "true",
      streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      },
      mapTypeControl: true,
      mapTypeId: "hybrid",
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_CENTER
      },
      fullscreenControl: false
    });
  }

  propertydetail(property, date) {
    this.platform.ready().then(() => {
      // let mapOptions = {
      //   zoom: this.auth.convertToint(this.property.page_custom_zoom),
      //   center: new google.maps.LatLng(
      //     this.property.prop_area_center_lat,
      //     this.property.prop_area_center_lng
      //   ),
      //   tilt: 0,
      //   disableDefaultUI: true,
      //   zoomControl: true,
      //   zoomControlOptions: {
      //     position: google.maps.ControlPosition.RIGHT_CENTER
      //   },
      //   mapTypeId: "hybrid",
      //   mapTypeControlOptions: {
      //     style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      //     position: google.maps.ControlPosition.TOP_CENTER
      //   },
      //   mapTypeControl: true,
      //   fullscreenControl: false,
      //   streetViewControl: true,
      //   streetViewControlOptions: {
      //     position: google.maps.ControlPosition.LEFT_TOP
      //   }
      // };
      // this.map_canvas = new google.maps.Map(
      //   this.mapElements.nativeElement,
      //   mapOptions
      // );
      // var el: any = this;
      // console.log(this.auth.convertToint(el.property.prop_area_center_lat));
      // console.log(this.auth.convertToint(el.property.prop_area_center_lng));
      // this.mapcanvas = new google.maps.Map(document.getElementById("mapcanvas"), {
      //   zoom: 10,
      //   center: new google.maps.LatLng(20, 50),
      //   tilt: 0,
      //   disableDefaultUI: true,
      //   zoomControl: true,
      //   zoomControlOptions: {
      //     position: google.maps.ControlPosition.RIGHT_CENTER
      //   },
      //   mapTypeId: "hybrid",
      //   mapTypeControlOptions: {
      //     style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      //     position: google.maps.ControlPosition.TOP_CENTER
      //   },
      //   mapTypeControl: true,
      //   fullscreenControl: true,
      //   streetViewControl: true,
      //   streetViewControlOptions: {
      //     position: google.maps.ControlPosition.LEFT_TOP
      //   }
      // });
      if (this.property.prop_area_shape_type == "rectangle") {
        // var rect = new google.maps.Rectangle({
        //   strokeColor: "yellow",
        //   strokeOpacity: 0.8,
        //   strokeWeight: 2,
        //   fillColor: "yellow",
        //   fillOpacity: 0,
        //   map: this.map_canvas,
        //   bounds: {
        //     north: this.property.prop_area_shape_cords[0].north,
        //     south: this.property.prop_area_shape_cords[0].south,
        //     east: this.property.prop_area_shape_cords[0].east,
        //     west: this.property.prop_area_shape_cords[0].west
        //   }
        // });
        // rect.setMap(this.map_canvas);
      } else if (this.property.prop_area_shape_type == "polygon") {
        // var poly = new google.maps.Polygon({
        //   paths: this.property.prop_area_shape_cord,
        //   strokeColor: "yellow",
        //   strokeOpacity: 0.8,
        //   strokeWeight: 2,
        //   fillColor: "yellow",
        //   fillOpacity: 0
        // });
        // poly.setMap(this.map_canvas);
      }
      // if (this.property.slot_details && this.property.slot_details.length) {
      //   for (
      //     var propert = 0;
      //     propert < this.property.slot_details.length;
      //     propert++
      //   ) {
      //     if (this.property.slot_details[propert].slot_type == "circle") {
      //       var circle = new google.maps.Circle({
      //         strokeColor: this.property.slot_details[propert].slot_color,
      //         strokeOpacity: 0.8,
      //         strokeWeight: 2,
      //         fillColor: this.property.slot_details[propert].slot_color,
      //         fillOpacity: 0.4,
      //         map: this.map_canvas,
      //         center: {
      //           lat: this.property.slot_details[propert].slot_circle_lat,
      //           lng: this.property.slot_details[propert].slot_circle_lng
      //         },
      //         radius: this.property.slot_details[propert].slot_circle_radious,
      //         indexID: this.property.slot_details[propert]
      //       });
      //       circle.setMap(this.map_canvas);
      //       this.addListenersOnpoly(circle);
      //     } else if (
      //       this.property.slot_details[propert].slot_type == "rectangle"
      //     ) {
      //       var rect = new google.maps.Rectangle({
      //         strokeColor: this.property.slot_details[propert].slot_color,
      //         strokeOpacity: 0.8,
      //         strokeWeight: 2,
      //         fillColor: this.property.slot_details[propert].slot_color,
      //         fillOpacity: 0.4,
      //         map: this.map_canvas,
      //         bounds: {
      //           north: this.property.slot_details[propert].slot_coordinates[0]
      //             .north,
      //           south: this.property.slot_details[propert].slot_coordinates[0]
      //             .south,
      //           east: this.property.slot_details[propert].slot_coordinates[0]
      //             .east,
      //           west: this.property.slot_details[propert].slot_coordinates[0]
      //             .west
      //         },
      //         indexID: this.property.slot_details[propert]
      //       });
      //       rect.setMap(this.map_canvas);
      //       this.addListenersOnpoly(rect);
      //     } else if (
      //       this.property.slot_details[propert].slot_type == "polygon"
      //     ) {
      //       var poly = new google.maps.Polygon({
      //         paths: this.property.slot_details[propert].slot_coordinate,
      //         strokeColor: this.property.slot_details[propert].slot_color,
      //         strokeOpacity: 0.8,
      //         strokeWeight: 2,
      //         fillColor: this.property.slot_details[propert].slot_color,
      //         fillOpacity: 0.4,
      //         indexID: this.property.slot_details[propert]
      //       });
      //       poly.setMap(this.map_canvas);
      //       this.addListenersOnpoly(poly);
      //     }
      //   }
      // }
    });
  }
  ionViewWillLeave() {}
  ionViewWillEnter() {}

  ionViewDidLoad() {}

  propertydetail1(property, date) {
    date = this.datepipe.transform(date, "yyyy-MM-dd");
    this.auth.startloader();
    try {
      this.auth
        .getpropertydetail(this.auth.getuserId(), property, date)
        .subscribe(
          data => {
            if (data) {
              if (data.json().status == 1) {
                var dataa: any = data.json().property_extra_details;
                if (typeof dataa.available_dates_second_format == "string") {
                  this.property.available_dates_first_format = JSON.parse(
                    dataa.available_dates_second_format
                  );
                }
                this.property.slot_details = dataa.slot_details;

                /********************/
                /*****Date Picker****/
                /********************/
                this.property.available_dates_first_format1 = [];
                this.property.available_dates_first_format2 = [];
                var el = this;
                for (
                  var dt = 0;
                  dt < this.property.available_dates_first_format.length;
                  dt++
                ) {
                  el.property.available_dates_first_format2[
                    dt
                  ] = el.property.available_dates_first_format[dt].split("-");
                  el.property.available_dates_first_format[dt] =
                    el.property.available_dates_first_format2[dt][1] +
                    "-" +
                    el.property.available_dates_first_format2[dt][0] +
                    "-" +
                    el.property.available_dates_first_format2[dt][2];
                  var ar: any = {
                    year: "",
                    month: "",
                    day: ""
                  };
                  el.property.available_dates_first_format[
                    dt
                  ] = el.property.available_dates_first_format[dt].replace(
                    /-/g,
                    "/"
                  );
                  ar.year = new Date(
                    el.property.available_dates_first_format[dt]
                  ).getFullYear();
                  ar.day = new Date(
                    el.property.available_dates_first_format[dt]
                  ).getDate();
                  ar.month =
                    new Date(
                      el.property.available_dates_first_format[dt]
                    ).getMonth() + 1;
                  el.property.available_dates_first_format1.push(ar);
                }
                this.myDatePickerOptions.enableDays = this.property.available_dates_first_format1;
                if (
                  this.property.slot_details &&
                  this.property.slot_details.length
                ) {
                  for (var x = 0; x < this.property.slot_details.length; x++) {
                    if (
                      this.property.slot_details[x].slot_type == "rectangle"
                    ) {
                      // if (this.property.slot_details[x].slot_coordinates) {
                      //   this.property.slot_details[
                      //     x
                      //   ].slot_coordinates = this.property.slot_details[
                      //     x
                      //   ].slot_coordinates.replace(/\\/g, "");
                      //   this.property.slot_details[
                      //     x
                      //   ].slot_coordinates = JSON.parse(
                      //     this.property.slot_details[x].slot_coordinates
                      //   );
                      // }
                    }
                    if (this.property.slot_details[x].slot_type == "circle") {
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_radious = parseFloat(
                      //   this.property.slot_details[x].slot_circle_radious
                      // );
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_lat = parseFloat(
                      //   this.property.slot_details[x].slot_circle_lat
                      // );
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_lng = parseFloat(
                      //   this.property.slot_details[x].slot_circle_lng
                      // );
                    }
                    if (this.property.slot_details[x].slot_type == "polygon") {
                      // if (this.property.slot_details[x].slot_coordinates) {
                      //   this.property.slot_details[
                      //     x
                      //   ].slot_coordinates = this.property.slot_details[
                      //     x
                      //   ].slot_coordinates.replace(/\\/g, "");
                      //   this.property.slot_details[
                      //     x
                      //   ].slot_coordinates = JSON.parse(
                      //     this.property.slot_details[x].slot_coordinates
                      //   );
                      //   this.property.slot_details[x].slot_coordinate = [];
                      //   var cords: any = {};
                      //   if (
                      //     this.property.slot_details[x].slot_coordinates &&
                      //     this.property.slot_details[x].slot_coordinates.length
                      //   ) {
                      //     for (
                      //       var y = 0;
                      //       y <
                      //       this.property.slot_details[x].slot_coordinates
                      //         .length;
                      //       y++
                      //     ) {
                      //       if (
                      //         this.property.slot_details[x].slot_coordinates[y]
                      //       ) {
                      //         this.property.slot_details[x].slot_coordinates[
                      //           y
                      //         ] = this.property.slot_details[
                      //           x
                      //         ].slot_coordinates[y].split(",");
                      //         cords = {
                      //           lat: parseFloat(
                      //             this.property.slot_details[x]
                      //               .slot_coordinates[y][0]
                      //           ),
                      //           lng: parseFloat(
                      //             this.property.slot_details[x]
                      //               .slot_coordinates[y][1]
                      //           )
                      //         };
                      //         this.property.slot_details[
                      //           x
                      //         ].slot_coordinate.push(cords);
                      //         this.property.slot_details[x].slot_coordinates[
                      //           y
                      //         ].lat = this.property.slot_details[
                      //           x
                      //         ].slot_coordinates[y][0];
                      //         this.property.slot_details[x].slot_coordinates[
                      //           y
                      //         ].lng = this.property.slot_details[
                      //           x
                      //         ].slot_coordinates[y][1];
                      //       }
                      //     }
                      //   }
                      // }
                    }
                  }
                }
                this.propertydetail(null, null);
                // this.map = new google.maps.Map(document.getElementById("map"), {
                //   zoom: this.property.prop_area_currnt_zoom,
                //   center: new google.maps.LatLng(
                //     this.options.lat,
                //     this.options.lng
                //   ),
                //   tilt: 0,
                //   disableDefaultUI: true,
                //   zoomControl: true,
                //   zoomControlOptions: {
                //     position: google.maps.ControlPosition.RIGHT_CENTER
                //   },
                //   mapTypeId: "hybrid",
                //   mapTypeControlOptions: {
                //     style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                //     position: google.maps.ControlPosition.TOP_CENTER
                //   },
                //   mapTypeControl: true,
                //   fullscreenControl: false,
                //   streetViewControl: true,
                //   streetViewControlOptions: {
                //     position: google.maps.ControlPosition.LEFT_TOP
                //   }
                // });
                // if (this.property.prop_area_shape_type == "rectangle") {
                //   var rect = new google.maps.Rectangle({
                //     strokeColor: "yellow",
                //     strokeOpacity: 0.8,
                //     strokeWeight: 2,
                //     fillColor: "yellow",
                //     fillOpacity: 0,
                //     map: this.map,
                //     bounds: {
                //       north: this.property.prop_area_shape_cords[0].north,
                //       south: this.property.prop_area_shape_cords[0].south,
                //       east: this.property.prop_area_shape_cords[0].east,
                //       west: this.property.prop_area_shape_cords[0].west
                //     }
                //   });
                //   rect.setMap(this.map);
                // } else if (this.property.prop_area_shape_type == "polygon") {
                //   var poly = new google.maps.Polygon({
                //     paths: this.property.prop_area_shape_cord,
                //     strokeColor: "yellow",
                //     strokeOpacity: 0.8,
                //     strokeWeight: 2,
                //     fillColor: "yellow",
                //     fillOpacity: 0
                //   });
                //   poly.setMap(this.map);
                // }
                // if (
                //   this.property.slot_details &&
                //   this.property.slot_details.length
                // ) {
                //   for (
                //     var propert = 0;
                //     propert < this.property.slot_details.length;
                //     propert++
                //   ) {
                //     if (
                //       this.property.slot_details[propert].slot_type == "circle"
                //     ) {
                //       var circle = new google.maps.Circle({
                //         strokeColor: this.property.slot_details[propert]
                //           .slot_color,
                //         strokeOpacity: 0.8,
                //         strokeWeight: 2,
                //         fillColor: this.property.slot_details[propert]
                //           .slot_color,
                //         fillOpacity: 0.4,
                //         map: this.map,
                //         center: {
                //           lat: this.property.slot_details[propert]
                //             .slot_circle_lat,
                //           lng: this.property.slot_details[propert]
                //             .slot_circle_lng
                //         },
                //         radius: this.property.slot_details[propert]
                //           .slot_circle_radious,
                //         indexID: this.property.slot_details[propert]
                //       });
                //       circle.setMap(this.map);
                //       this.addListenersOnpoly(circle);
                //     } else if (
                //       this.property.slot_details[propert].slot_type ==
                //       "rectangle"
                //     ) {
                //       var rect = new google.maps.Rectangle({
                //         strokeColor: this.property.slot_details[propert]
                //           .slot_color,
                //         strokeOpacity: 0.8,
                //         strokeWeight: 2,
                //         fillColor: this.property.slot_details[propert]
                //           .slot_color,
                //         fillOpacity: 0.4,
                //         map: this.map,
                //         bounds: {
                //           north: this.property.slot_details[propert]
                //             .slot_coordinates[0].north,
                //           south: this.property.slot_details[propert]
                //             .slot_coordinates[0].south,
                //           east: this.property.slot_details[propert]
                //             .slot_coordinates[0].east,
                //           west: this.property.slot_details[propert]
                //             .slot_coordinates[0].west
                //         },
                //         indexID: this.property.slot_details[propert]
                //       });
                //       rect.setMap(this.map);
                //       this.addListenersOnpoly(rect);
                //     } else if (
                //       this.property.slot_details[propert].slot_type == "polygon"
                //     ) {
                //       var poly = new google.maps.Polygon({
                //         paths: this.property.slot_details[propert]
                //           .slot_coordinate,
                //         strokeColor: this.property.slot_details[propert]
                //           .slot_color,
                //         strokeOpacity: 0.8,
                //         strokeWeight: 2,
                //         fillColor: this.property.slot_details[propert]
                //           .slot_color,
                //         fillOpacity: 0.4,
                //         indexID: this.property.slot_details[propert]
                //       });
                //       poly.setMap(this.map);
                //       this.addListenersOnpoly(poly);
                //     }
                //   }
                // }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
            this.auth.stoploader();
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  selectedproperty: any = {};
  dynamicbutton: any;
  addListenersOnpoly(shape) {
    var el: any = this;
    new google.maps.event.addListener(shape, "click", function(event) {
      if (shape.indexID) {
        if (el.added != 0 && el.added && el.added.length) {
          for (var id = 0; id < el.added.length; id++) {
            if (
              shape.indexID.slot_id === el.added[id].slotid &&
              shape.indexID.date == el.added[id].date_of_product
            ) {
              el.dynamicbutton = "View Cart";
              el.selectedproperty = shape.indexID;
              el.openEvent();
              break;
            } else {
              el.dynamicbutton = "Add Slot";
              el.selectedproperty = shape.indexID;
              el.openEvent();
            }
          }
        } else {
          el.dynamicbutton = "Add Slot";
          el.selectedproperty = shape.indexID;
          el.openEvent();
        }
      }
    });
  }

  status: boolean = false;
  show: boolean = false;
  clickEvent() {
    this.status = !this.status;
    this.show = !this.show;
  }
  openEvent() {
    this.status = true;
    this.show = true;
  }
  added: any = [];

  addslot(selectedproperty) {
    if (
      selectedproperty.able_to_add_to_cart == "yes" &&
      this.dynamicbutton == "Add Slot"
    ) {
      this.auth.startloader();
      try {
        this.auth
          .addtocart(this.auth.getuserId(), selectedproperty.product_id)
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data) {
                if (data.json().status == 1) {
                  this.auth.toast(data.json().message);
                  // this.status = !this.status;
                  // this.show = !this.show;
                  this.getcart();
                  this.changeworld();
                  this.propertydetail1(
                    this.property.ID,
                    this.selectedproperty.date
                  );
                  this.dynamicbutton = "View Cart";
                } else if (data.json().status == 0) {
                  this.auth.toast(data.json().message);
                  // this.status = !this.status;
                  // this.show = !this.show;
                  this.getcart();
                  this.propertydetail1(
                    this.property.ID,
                    this.selectedproperty.date
                  );
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.errtoast(errorHandler);
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
            }
          );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    } else if (this.dynamicbutton == "View Cart") {
      this.fullview();
      this.goto("MycartPage", null);
    } else {
      this.auth.toast("Currently, you are not able to book this slot");
    }
  }

  getcart() {
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(data => {
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.added = data.json().more_data;
          } else {
            this.added = 0;
          }
        }
      });
    } catch (e) {
      this.auth.toast(e);
    }
  }
  goto(page, media) {
    if (this.property.embed_video_id) {
      var video: any = {
        embed_video_id: this.property.embed_video_id,
        embed_video_type: this.property.embed_video_type
      };
      this.navCtrl.push(page, { medias: media, video: video });
    } else {
      this.navCtrl.push(page, { medias: media, video: video });
    }
  }

  /*****************/
  clickslot(shape) {
    var el = this;
    if (shape) {
      if (el.added != 0 && el.added && el.added.length) {
        for (var id = 0; id < el.added.length; id++) {
          if (
            shape.slot_id === el.added[id].slotid &&
            shape.date == el.added[id].date_of_product
          ) {
            el.dynamicbutton = "View Cart";
            el.selectedproperty = shape;
            el.openEvent();
            break;
          } else {
            el.dynamicbutton = "Add Slot";
            el.selectedproperty = shape;
            el.openEvent();
          }
        }
      } else {
        el.dynamicbutton = "Add Slot";
        el.selectedproperty = shape;
        el.openEvent();
      }
    }
  }
  // AGM

  polygon: any;
  circle: any;
  rectangle: any;
  options: any = {
    lat: "",
    lng: "",
    zoom: 18,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    strokeWeight: 3,
    strokePos: 0
  };

  favorite(property_id, user_id) {
    try {
      this.auth.addtofavourite(property_id, user_id).subscribe(data => {
        if (data) {
          var dataa: any = data.json();
          if (dataa.added == false) {
            this.property.is_fav = 0;
            this.auth.vibalert();
          } else if (dataa.added == true) {
            this.property.is_fav = 1;
            this.auth.vibalert();
          }
        }
      });
    } catch (e) {
      this.auth.toast(e);
    }
  }

  datechange(date) {
    date.formatted = this.datepipe.transform(date.formatted, "yyyy-MM-dd");
    this.selectedproperty.date = date.formatted;
    date.formatted = new Date(date.formatted);
    if (date.formatted instanceof Date) {
      this.propertydetail(this.property.ID, date.formatted);
    } else {
    }
  }

  // timerdatetime: any;
  startimer(datetime) {
    this.auth.addedtocart(datetime);
    this.triggertimer(datetime);
  }
  triggertimer(datetime) {
    this.startimers(datetime);
  }
  changeworld() {
    this.startimer(new Date());
  }

  ////////////////////////////////
  ///////////Timer////////////////
  ///////////////////////////////
  endatetime: any;
  displaytimer: any = false;
  public startimers(dates) {
    this.displaytimer = true;
    var setdate = new Date(dates);
    var myTimeSpan = 5 * 60 * 1000;
    var currentdate = setdate.setTime(setdate.getTime() + myTimeSpan);
    this.endatetime = this.datepipe.transform(
      currentdate,
      "yyyy-MM-dd HH:mm:ss"
    );
    this.endatetime =
      new Date(this.endatetime).getTime() / 1000 - new Date().getTime() / 1000;
  }

  yourOwnFunction() {
    this.displaytimer = false;
    this.remove_getcart();
  }
  remove_getcart() {
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            if (data.json() && data.json().status === 1) {
              if (data.json().more_data && data.json().more_data.length) {
                for (var pro = 0; pro < data.json().more_data.length; pro++) {
                  this.removeproductcart(data.json().more_data[pro], pro);
                }
                this.getcart();
                this.added = [];
                this.auth.clearcartstorage();
              }
              this.auth.clearcookie();
            } else {
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.clearcookie();
      this.auth.toast(e);
    }
  }

  removeproductcart(product, index) {
    try {
      this.auth
        .removeproductcart(this.auth.getuserId(), product.product_id)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              if (data.json() && data.json().status === 1) {
                this.auth.clearcookie();
              } else if (data.json().status == 2) {
                this.auth.clearcookie();
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
              }
            }
          },
          errorHandler => {
            this.auth.clearcookie();
            this.auth.errtoast(errorHandler);
          }
        );
    } catch (e) {
      this.auth.clearcookie();
      this.auth.toast(e);
    }
  }
}
