import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the ChangepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-changepassword",
  templateUrl: "changepassword.html"
})
export class ChangepasswordPage {
  myForm: FormGroup;
  data: {
    old_password: "";
    new_password: "";
    confirm_password: "";
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    public auth: AuthProvider
  ) {
    this.myForm = this.fb.group({
      old_password: ["", Validators.required],
      new_password: ["", Validators.required],
      confirm_password: ["", Validators.required]
    });
  }

  ionViewDidLoad() {}

  checkform() {
    if (this.myForm.valid) {
      if (
        this.myForm.value.new_password == this.myForm.value.confirm_password
      ) {
        this.changepassword();
      } else {
        this.auth.toast("New Password and confirm password not matching");
      }
    } else {
      this.auth.toast("Please fill form carefully");
    }
  }
  changepassword() {
    this.auth.clearcookie();
    try {
      this.auth
        .changepassword(
          this.auth.getuserId(),
          this.myForm.value.new_password,
          this.myForm.value.old_password,
          this.myForm.value.confirm_password
        )
        .subscribe(data => {
          if (data) {
            if (data.json() && data.json().status == 1) {
              this.auth.toast(data.json().message);
              this.logout();
            } else if (data.json().status == 0) {
              this.auth.toast(data.json().message);
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            }
          }
        });
    } catch (e) {
      this.auth.toast(e);
    }
  }

  logout() {
    var el: any = this;
    setTimeout(function() {
      el.auth.logout();
      el.navCtrl.setRoot("LoginPage");
    }, 3000);
  }
}
