import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the ManagecouponsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-managecoupons",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "managecoupons.html"
})
export class ManagecouponsPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {}
  ionViewWillEnter() {
    this.auth.startloader();
    this.getallcoupon();
  }
  ionViewDidLoad() {}
  goto(page) {
    this.navCtrl.push(page);
  }

  status: boolean = false;
  show: boolean = false;
  selectedCoupon: any = {};
  clickEvent(coupon, index) {
    if (coupon && index) {
      this.selectedCoupon.name = coupon;
      this.selectedCoupon.index = index;
    } else {
      this.selectedCoupon.name = null;
      this.selectedCoupon.index = null;
    }
    this.status = !this.status;
    this.show = !this.show;
  }
  deletecoupon() {
    if (this.selectedCoupon) {
      this.removecoupon();
    }
  }
  removecoupon() {
    this.auth
      .removecoupon(this.auth.getuserId(), this.selectedCoupon.name)
      .subscribe(data => {
        if (data) {
          if (data.json().status == 1) {
            this.auth.toast(data.json().message);
            this.coupons.splice(this.selectedCoupon.index, 1);
            this.status = !this.status;
            this.show = !this.show;
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          } else {
            this.auth.toast("Currently not able to delete coupon");
          }
        }
      });
  }
  editcoupon() {
    if (this.selectedCoupon) {
      this.customizecoupon();
    }
  }
  customizecoupon() {
    this.auth
      .getcoupon(this.auth.getuserId(), this.selectedCoupon.name)
      .subscribe(data => {
        if (data) {
          if (data.json().status == 1) {
            var cpn: any = {};
            cpn = {
              coupon_id: this.selectedCoupon.name,
              coupon_name: data.json().single_coupon.coupon_name,
              data: data.json().single_coupon.data
            };
            this.navCtrl.push("CreatecouponPage", {
              single_coupon: cpn
            });
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          }
        }
      });
  }
  coupons: any;
  getallcoupon() {
    try {
      this.auth.getallcoupon(this.auth.getuserId()).subscribe(data => {
        this.auth.stoploader();
        if (data) {
          if (data.json().status == 1) {
            this.coupons = data.json().coupons;
          }
        }
      });
    } catch (e) {
      this.auth.stoploader();
    }
  }
}
