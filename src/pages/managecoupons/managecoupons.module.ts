import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManagecouponsPage } from './managecoupons';

@NgModule({
  declarations: [
    ManagecouponsPage,
  ],
  imports: [
    IonicPageModule.forChild(ManagecouponsPage),
  ],
})
export class ManagecouponsPageModule {}
