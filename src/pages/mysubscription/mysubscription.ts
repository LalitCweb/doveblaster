import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import {
  PayPal,
  PayPalPayment,
  PayPalConfiguration
} from "@ionic-native/paypal";

/**
 * Generated class for the MysubscriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-mysubscription",
  templateUrl: "mysubscription.html"
})
export class MysubscriptionPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    private payPal: PayPal
  ) {}

  ionViewDidLoad() {
    this.subscriptionList(this.auth.getuserId());
  }
  ionViewWillEnter() {
    this.getuserdata(this.auth.getUsername("username"));
  }
  //////////////////////////
  //////User Profile////////
  /////////////////////////
  getuserdata(user_id) {
    this.auth.clearcookie();
    this.auth.startloader();
    try {
      this.auth.getuserdata(user_id).subscribe(
        data => {
          this.auth.clearcookie();
          this.auth.stoploader();
          if (data) {
            var dataa: any = data.json().posts;
            // this.data = dataa;
            // if (dataa.first_name && dataa.first_name[0]) {
            //   this.data.first_name = dataa.first_name[0];
            // }
            // if (dataa.last_name && dataa.last_name[0]) {
            //   this.data.last_name = dataa.last_name[0];
            // }
            // if (dataa.phone && dataa.phone[0]) {
            //   this.data.phone = dataa.phone[0];
            // }
            // if (dataa.mobile && dataa.mobile[0]) {
            //   this.data.mobile = dataa.mobile[0];
            // }
            // if (dataa.paypal_payments_to && dataa.paypal_payments_to[0]) {
            //   this.data.paypal_payments_to = dataa.paypal_payments_to[0];
            // }
            // if (dataa.description && dataa.description[0]) {
            //   this.data.description = dataa.description[0];
            // }
            // if (dataa.live_in && dataa.live_in[0]) {
            //   this.data.live_in = dataa.live_in[0];
            // }
            // if (dataa.i_speak && dataa.i_speak[0]) {
            //   this.data.i_speak = dataa.i_speak[0];
            // }
            // if (dataa.facebook && dataa.facebook[0]) {
            //   this.data.facebook = dataa.facebook[0];
            // }
            // if (dataa.twitter && dataa.twitter[0]) {
            //   this.data.twitter = dataa.twitter[0];
            // }
            // if (dataa.user_email && dataa.user_email[0]) {
            //   this.data.user_email = dataa.user_email;
            // }
            localStorage.setItem(
              "dayleasing_user_type",
              JSON.stringify(data.json().posts)
            );
            this.auth.clearcookie();
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.logout();
      this.navCtrl.setRoot("LoginPage");
      this.auth.errtoast(err);
    }
  }

  /////////////////////////
  /////////////////////////
  /////////////////////////

  data: any = [];
  subscriptionList(user_id) {
    this.auth.subscriptionList(user_id).subscribe(data => {
      if (data) {
        var dataa: any = data.json().all_subscriptios;
        this.data = dataa;
        if (this.auth.getpackage_id()) {
          this.package = this.auth.getpackage_id();
        }
      }
    });
  }
  package: any;
  plan: any = {};
  paysubscription() {
    for (var sbsc = 0; sbsc < this.data.length; sbsc++) {
      if (this.package == this.data[sbsc].id) {
        this.plan = this.data[sbsc];
        this.auth
          .buysubscriptionList(
            this.auth.getuserId(),
            this.plan.id,
            this.plan.data_pick
          )
          .subscribe(
            data => {
              if (data) {
                if (data.json().status == 1) {
                  this.openpaypal(
                    parseInt(this.plan.data_price),
                    data.json().payment_approval_url,
                    data.json().token
                  );
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                } else if (data.json() && data.json().status === 0) {
                  this.auth.clearcookie();
                  this.auth.toast(data.json().message);
                }
              }
            },
            errorHandler => {
              this.auth.errtoast(errorHandler);
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
            }
          );
        break;
      }
    }
  }

  openpaypal(payment_amount, notify_url, token) {
    this.payPal
      .init({
        PayPalEnvironmentProduction:
          "AXIOkOsNIRFESq7Pz-y6MUaBo_YmbWTjaoErEJ6aIdeLdKpq4pHZ6v8kipbVF7Cp5IWmJGJoWdx5j_Oh",
        PayPalEnvironmentSandbox:
          "AXIOkOsNIRFESq7Pz-y6MUaBo_YmbWTjaoErEJ6aIdeLdKpq4pHZ6v8kipbVF7Cp5IWmJGJoWdx5j_Oh"
      })
      .then(
        () => {
          // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
          this.payPal
            .prepareToRender(
              "PayPalEnvironmentSandbox",
              new PayPalConfiguration({
                // Only needed if you get an "Internal Service Error" after PayPal login!
                //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
              })
            )
            .then(
              () => {
                let payment = new PayPalPayment(
                  payment_amount,
                  "USD",
                  "Description",
                  "sale"
                );
                payment.custom = JSON.stringify({
                  notify_url: notify_url,
                  token: token
                });
                this.payPal.renderSinglePaymentUI(payment).then(
                  response => {
                    debugger;
                    if (response.response.state == "approved") {
                      this.auth.toast("Subscribed successfully");
                    }
                    // Successfully paid
                    // Example sandbox response
                    //
                    // {
                    //   "client": {
                    //     "environment": "sandbox",
                    //     "product_name": "PayPal iOS SDK",
                    //     "paypal_sdk_version": "2.16.0",
                    //     "platform": "iOS"
                    //   },
                    //   "response_type": "payment",
                    //   "response": {
                    //     "id": "PAY-1AB23456CD789012EF34GHIJ",
                    //     "state": "approved",
                    //     "create_time": "2016-10-03T13:33:33Z",
                    //     "intent": "sale"
                    //   }
                    // }
                  },
                  () => {
                    // Error or render dialog closed without being successful
                  }
                );
              },
              () => {
                // Error in configuration
              }
            );
        },
        () => {
          // Error in initialization, maybe PayPal isn't supported or something else
        }
      );
  }
}
