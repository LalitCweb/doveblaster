import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-info",
  templateUrl: "info.html"
})
export class InfoPage {
  legal: any;
  policy: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if (this.navParams.get("info") == "legal") {
      this.legal = true;
      this.policy = false;
    } else if (this.navParams.get("info") == "policy") {
      this.policy = true;
      this.legal = false;
    }
  }

  ionViewDidLoad() {}
}
