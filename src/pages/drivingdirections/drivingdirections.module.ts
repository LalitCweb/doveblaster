import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { DrivingdirectionsPage } from "./drivingdirections";
// import { AgmCoreModules } from "../../agm/core";
import { AgmCoreModule } from "@agm/core";
import { AgmDirectionModule } from "agm-direction";
@NgModule({
  declarations: [DrivingdirectionsPage],
  imports: [
    AgmCoreModule,
    // AgmCoreModules,
    AgmDirectionModule,
    IonicPageModule.forChild(DrivingdirectionsPage)
  ]
})
export class DrivingdirectionsPageModule {}
