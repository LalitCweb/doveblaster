import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  FabContainer
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the DrivingdirectionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-drivingdirections",
  templateUrl: "drivingdirections.html"
})
export class DrivingdirectionsPage {
  lat: Number;
  lng: Number;
  direct: any = false;
  zoom: Number = 24;
  option: any = {
    lat: 0,
    lng: 0,
    zoom: 18,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    strokeWeight: 3,
    strokePos: 0,
    icon: "assets/images/marker.png"
  };
  marker: any;
  bookingslotid: any;
  arrayslotdata: any;
  origin: any = { lat: 0, lng: 0 };
  destination = { lat: 0, lng: 0 };
  slotarea: any;
  public renderOptions = {
    suppressMarkers: true
  };

  public markerOptions = {
    origin: {
      icon: "assets/images/green.png"
    },
    destination: {
      icon: "assets/images/marker.png"
    }
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation: Geolocation,
    private toastCtrl: ToastController,
    public auth: AuthProvider
  ) {
    this.bookingslotid = navParams.get("linked_slot_tothis_booking");
    this.arrayslotdata = navParams.get("array_slots_data");
    this.lat = parseFloat(navParams.get("lat"));
    this.lng = parseFloat(navParams.get("lng"));
    for (var x = 0; x < this.arrayslotdata.length; x++) {
      if (this.arrayslotdata[x].slot_id == this.bookingslotid) {
        this.slotarea = this.arrayslotdata[x];
      }
    }
    var drivingdirection = navParams.get("drivingdirection").split(",");
    this.destination.lat = parseFloat(drivingdirection[0]);
    this.destination.lng = parseFloat(drivingdirection[1]);
    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        if (resp) {
          this.lat = resp.coords.latitude;
          this.lng = resp.coords.longitude;
          // this.lat = this.options.lat + 0.01;
        }
      })
      .catch(error => {
        this.auth.errtoast(error);
      });
  }
  ionViewWillEnter() {
    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        if (resp) {
          this.option.lat = resp.coords.latitude;
          this.option.lng = resp.coords.longitude;
          this.origin.lat = resp.coords.latitude;
          this.origin.lng = resp.coords.longitude;
        }
      })
      .catch(error => {
        this.auth.errtoast(error);
      });
    let options = {
      enableHighAccuracy: true
    };
    let watch = this.geolocation.watchPosition(options);
    watch.subscribe(data => {
      // data can be a set of coordinates, or an error (if an error occurred).
      if (this.direct) {
        this.option.lat = data.coords.latitude;
        this.option.lng = data.coords.longitude;
      }
      this.lat = data.coords.latitude;
      this.lng = data.coords.longitude;
      if (data.coords.latitude && data.coords.longitude) {
        this.origin.lat = data.coords.latitude;
        this.origin.lng = data.coords.longitude;
      } else {
        this.presentToast("Connection Lost");
      }
    });
  }
  ionViewDidEnter() {}

  private _map: any;
  changeTilt(map) {
    this._map = map;
    this._map.setTilt(0);
  }
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "top"
    });

    toast.onDidDismiss(() => {});

    toast.present();
  }

  currentposition(fab: FabContainer) {
    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        if (resp) {
          this.option.lat = resp.coords.latitude;
          this.option.lng = resp.coords.longitude;
          this.origin.lat = resp.coords.latitude;
          this.origin.lng = resp.coords.longitude;
        }
      })
      .catch(error => {
        this.auth.errtoast(error);
      });
    fab.close();
  }
  direction() {
    this.direct = true;
  }
  stopdirection() {
    this.direct = false;
  }
  reroute(fab: FabContainer) {
    this.origin.lat = this.lat;
    this.origin.lng = this.lng;
    fab.close();
  }
}
