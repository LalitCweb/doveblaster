import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PropertieslistviewPage } from "./propertieslistview";
import { ComponentsModule } from "../../components/components.module";
import { AgmCoreModules } from "../../agm/core";
import { AgmJsMarkerClustererModule } from "@agm/js-marker-clusterer";
import { OrderModule } from "ngx-order-pipe";
import { MyDatePickerModule } from "mydatepicker";
import { PipesModule } from "../../pipes/pipes.module";
@NgModule({
  declarations: [PropertieslistviewPage],
  imports: [
    MyDatePickerModule,
    ComponentsModule,
    AgmCoreModules,
    OrderModule,
    AgmJsMarkerClustererModule,
    IonicPageModule.forChild(PropertieslistviewPage),
    PipesModule
  ]
})
export class PropertieslistviewPageModule {}
