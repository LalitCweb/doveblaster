import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  group,
  ViewChild,
  ElementRef
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  AlertController,
  ViewController
} from "ionic-angular";
import * as $ from "jquery";
import { AuthProvider } from "../../providers/auth/auth";
import { DatePipe } from "@angular/common";
/**
 * Generated class for the AddslotpropertyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-addslotproperty",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "addslotproperty.html"
})
export class AddslotpropertyPage {
  @ViewChild("map_canvas") mapElements: ElementRef;
  drawingManager: any;
  map: any;
  map_canvas: any;
  marker = "assets/images/stillmarker.gif";
  relationship: any = "friends";
  property: any = {};
  areaindex: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public auth: AuthProvider,
    public datepipe: DatePipe,
    public alertController: AlertController,
    private viewCtrl: ViewController
  ) {
    if (
      (this.navParams.get("property_data") &&
        this.navParams.get("property_data").index == 0) ||
      (this.navParams.get("property_data") &&
        this.navParams.get("property_data").index > 0)
    ) {
      this.callproperty(
        this.navParams.get("property_data").property,
        this.navParams.get("property_data").index
      );
    } else {
      this.property = this.navParams.get("property_data");
      this.areaindex = 0;
      this.getpropertydetail(this.property.property_id);
    }
  }

  ngOnInit() {}

  callproperty(property, index) {
    this.property = property;
    this.areaindex = index;
    this.getpropertydetail(this.property.property_id);
  }

  selectedArea: any = {};
  goback() {
    this.viewCtrl.dismiss();
  }

  resetdrawingmanager() {
    var polyOptions = {
      fillColor: this.selectedArea.area_color,
      strokeColor: this.selectedArea.area_color,
      strokeWeight: 1,
      fillOpacity: 0.45,
      editable: true,
      draggable: true,
      clickable: true
    };

    this.drawingManager.set("polygonOptions", polyOptions);
    this.drawingManager.set("circleOptions", polyOptions);
    this.drawingManager.set("rectangleOptions", polyOptions);
    // this.drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
  }

  productdetail: any;
  getpropertydetail(marker) {
    this.auth.getsinglealldetail(marker, this.auth.getuserId()).subscribe(
      data => {
        if (data) {
          if (data.json().status == 1) {
            this.productdetail = data.json().events;
            this.property = data.json().events;
            this.productdetail.ID = marker;
            this.selectedArea = this.productdetail.all_areas[this.areaindex];
            this.selectedArea.slots = [];
            this.selectedArea.index = 0 + 1;
            this.property.area = this.productdetail.prop_total_area;
            this.property.mapaddress = this.productdetail.property_address;
            this.property.wpestate_title = this.productdetail.property_name;
            this.property.prop_category = this.productdetail.property_category;
            this.property.property_city = this.productdetail.city;
            setTimeout(() => {
              $(document).ready(function() {
                $("span")
                  .find("a")
                  .contents()
                  .unwrap();
              });
            }, 100);
            this.initializeMap();
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      },
      errorHandler => {
        this.auth.logout();
        this.navCtrl.setRoot("LoginPage");
        this.auth.errtoast(errorHandler);
      }
    );
  }

  ionViewDidLoad() {}
  status: boolean = false;
  show: boolean = false;
  slotoptions(slot) {
    this.clickEvent(slot);
  }
  clickEvent(value) {
    if (value == undefined || value == "" || value == null) {
    } else {
      this.clearSelection(this.selectedShape);
      this.selectedShape = this.all_overlays[value - 1];
      this.selectedShape.setEditable(true);
      this.index = value - 1;
    }
    this.status = !this.status;
    this.show = !this.show;
  }
  status1: boolean = false;
  show1: boolean = false;
  index: any;
  selectedslot: any;
  clickEvent1(slot, index, action) {
    if (slot) {
      if (action == "select") {
        this.selectedslot = slot;
        this.selectedslot.index = index;
      } else if (action == "delete") {
        this.deleteSlotfromdb();
      }
    }
    this.status1 = !this.status1;
    this.show1 = !this.show1;
  }
  deleteSlotfromdb() {
    if (this.selectedslot) {
      this.auth.startloader();
      try {
        this.auth
          .deleteslot(this.auth.getuserId(), this.selectedslot.slot_id)
          .subscribe(data => {
            if (data.json()) {
            }
          });
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    }
  }
  copySlot() {
    // if (this.index) {
    //   this.selectedArea.slots[this.index].slotname
    //   this.selectedArea.slots.push(this.selectedArea.slots[this.index]);
    // }
    // this.selectedShape.zIndex = this.all_overlays.length;
    // this.setSelection(this.selectedShape);
    // this.all_overlays.push(this.selectedShape);
  }

  // Map by id
  selectedColor: any;
  selectColor(color) {
    this.selectedColor = color;
  }

  selectedShape: any;
  selectedcordShape: any;
  all_overlays: any = [];
  clearSelection = (shape): void => {
    if (this.selectedShape) {
      this.selectedShape.setEditable(false);
      this.selectedShape = null;
    }
  };
  resetSelection = (shape): void => {
    if (this.selectedShape) {
      this.selectedShape.setEditable(false);
      shape.setEditable(true);
    }
  };
  editslot() {
    debugger;
    this.show1 = false;
    this.status1 = false;
    if (this.selectedslot.index != this.areaindex) {
      this.areaindex = this.selectedslot.index;
      this.getpropertydetail(this.property.property_id);
    }
  }
  onItemChange(item, i) {
    debugger;
    this.getSelecteditem(item, i);
  }
  getSelecteditem(item, i) {
    this.selectedArea = item;
    this.selectedArea.index = i;
    this.deleteAllShapes();
    // this.resetdrawingmanager();
    this.changeAreaselection(item, i);
  }
  changeAreaselection(item, i) {
    this.callproperty(this.property, i);
  }

  updatevents(newShape) {
    google.maps.event.addListener(newShape, "click", () => {
      this.checkcords(newShape);
      this.editSelection(newShape);
    });
    google.maps.event.addListener(newShape, "mouseup", () => {
      this.checkcords(newShape);
      this.editSelection(newShape);
    });
    google.maps.event.addListener(newShape, "dragend", () => {
      this.checkcords(newShape);
      this.editSelection(newShape);
    });
    google.maps.event.addListener(newShape, "resize", () => {
      this.checkcords(newShape);
      this.editSelection(newShape);
    });
    google.maps.event.addListener(newShape, "bounds_changed", () => {
      this.checkcords(newShape);
      this.editSelection(newShape);
    });
    // google.maps.event.addListener(newShape, "radius_changed", () => {
    //   console.log("radius_changed");
    //   this.checkcords(newShape);
    //   this.editSelection(newShape);
    // });
    google.maps.event.addListener(newShape, "center_changed", () => {
      this.checkcords(newShape);
      this.editSelection(newShape);
    });
    google.maps.event.addListener(newShape, "dblclick", () => {
      this.checkcords(newShape);
      this.setSelection(newShape, null);
    });
    google.maps.event.addListener(this.map_canvas, "click", () => {
      this.clearSelection(this.selectedShape);
    });
  }

  shapecords: any;
  area: any;
  initializeMap = (): void => {
    this.platform.ready().then(() => {
      let mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(
          this.property.prop_area_center_lat,
          this.property.prop_area_center_lng
        ),
        tilt: 0,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
        },
        mapTypeId: "hybrid",
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_CENTER
        },
        mapTypeControl: true,
        fullscreenControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP
        }
      };
      this.map_canvas = new google.maps.Map(
        this.mapElements.nativeElement,
        mapOptions
      );
      ////////////////////////////////////
      ////////Property Outline///////////
      ///////////////////////////////////
      if (this.property.prop_area_shape_type == "rectangle") {
        if (typeof this.property.prop_area_shape_cords == "string") {
          this.property.prop_area_shape_cords = JSON.parse(
            this.property.prop_area_shape_cords
          );
          var rect = new google.maps.Rectangle({
            strokeColor: this.property.prop_area_shape_fillcolor,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: this.property.prop_area_shape_fillcolor,
            fillOpacity: 0.2,
            map: this.map_canvas,
            bounds: {
              north: this.property.prop_area_shape_cords[0].north,
              south: this.property.prop_area_shape_cords[0].south,
              east: this.property.prop_area_shape_cords[0].east,
              west: this.property.prop_area_shape_cords[0].west
            }
          });
          rect.setMap(this.map_canvas);
        }
      } else if (this.property.prop_area_shape_type == "polygon") {
        if (typeof this.property.prop_area_shape_cords == "string") {
          this.property.prop_area_shape_cords = JSON.parse(
            this.property.prop_area_shape_cords
          );
          this.property.prop_area_shape_cord = [];
          var cords: any = {};
          for (var x = 0; x < this.property.prop_area_shape_cords.length; x++) {
            try {
              this.property.prop_area_shape_cords[
                x
              ] = this.property.prop_area_shape_cords[x].split(",");
            } catch (e) {
              // return;
            }
            cords = {
              lat: parseFloat(this.property.prop_area_shape_cords[x][0]),
              lng: parseFloat(this.property.prop_area_shape_cords[x][1])
            };
            this.property.prop_area_shape_cord.push(cords);
            this.property.prop_area_shape_cords[
              x
            ].lat = this.property.prop_area_shape_cords[x][0];
            this.property.prop_area_shape_cords[
              x
            ].lng = this.property.prop_area_shape_cords[x][1];
          }

          var poly = new google.maps.Polygon({
            paths: this.property.prop_area_shape_cord,
            strokeColor: this.property.selectedColor,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: this.property.selectedColor,
            fillOpacity: 0.2
          });
          poly.setMap(this.map_canvas);
        }
      }

      ////////////////////////////////////
      /////////////Slot Outline///////////
      ///////////////////////////////////
      if (
        this.property.all_areas &&
        this.property.all_areas[this.areaindex] &&
        this.property.all_areas[this.areaindex].slots_full_details.length
      ) {
        for (
          var areas = 0;
          areas <
          this.property.all_areas[this.areaindex].slots_full_details.length;
          areas++
        ) {
          if (
            this.property.all_areas[this.areaindex].slots_full_details[areas]
              .shapetype == "rectangle"
          ) {
            if (
              typeof this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinates == "string"
            ) {
              this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinates = this.property.all_areas[
                this.areaindex
              ].slots_full_details[areas].slot_coordinates.replace(/\\/g, "");
              this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinates = JSON.parse(
                this.property.all_areas[this.areaindex].slots_full_details[
                  areas
                ].slot_coordinates
              );
            }
            var rect = new google.maps.Rectangle({
              strokeColor: this.property.all_areas[this.areaindex]
                .slots_full_details[areas].slot_color,
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: this.property.all_areas[this.areaindex]
                .slots_full_details[areas].slot_color,
              fillOpacity: 0.2,
              map: this.map_canvas,
              bounds: {
                north: this.property.all_areas[this.areaindex]
                  .slots_full_details[areas].slot_coordinates[0].north,
                south: this.property.all_areas[this.areaindex]
                  .slots_full_details[areas].slot_coordinates[0].south,
                east: this.property.all_areas[this.areaindex]
                  .slots_full_details[areas].slot_coordinates[0].east,
                west: this.property.all_areas[this.areaindex]
                  .slots_full_details[areas].slot_coordinates[0].west
              },
              editable: true,
              draggable: true,
              type: "rectangle"
            });
            rect.setMap(this.map_canvas);
            this.all_overlays.push(rect);
            this.selectedShape = rect;
            this.setSelection(this.selectedShape, areas);
            this.updatevents(this.selectedShape);
          } else if (
            this.property.all_areas[this.areaindex].slots_full_details[areas]
              .shapetype == "polygon"
          ) {
            if (
              typeof this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinates == "string"
            ) {
              this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinates = this.property.all_areas[
                this.areaindex
              ].slots_full_details[areas].slot_coordinates.replace(/\\/g, "");
              this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinates = JSON.parse(
                this.property.all_areas[this.areaindex].slots_full_details[
                  areas
                ].slot_coordinates
              );
            }
            this.property.all_areas[this.areaindex].slots_full_details[
              areas
            ].slot_coordinate = [];
            var cords: any = {};
            for (
              var x = 0;
              x <
              this.property.all_areas[this.areaindex].slots_full_details[areas]
                .slot_coordinates.length;
              x++
            ) {
              try {
                this.property.all_areas[this.areaindex].slots_full_details[
                  areas
                ].slot_coordinates[x] = this.property.all_areas[
                  this.areaindex
                ].slots_full_details[areas].slot_coordinates[x].split(",");
              } catch (e) {
                // return;
              }
              cords = {
                lat: parseFloat(
                  this.property.all_areas[this.areaindex].slots_full_details[
                    areas
                  ].slot_coordinates[x][0]
                ),
                lng: parseFloat(
                  this.property.all_areas[this.areaindex].slots_full_details[
                    areas
                  ].slot_coordinates[x][1]
                )
              };
              this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinate.push(cords);
              this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinates[x].lat = this.property.all_areas[
                this.areaindex
              ].slots_full_details[areas].slot_coordinates[x][0];
              this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinates[x].lng = this.property.all_areas[
                this.areaindex
              ].slots_full_details[areas].slot_coordinates[x][1];
            }
            var poly = new google.maps.Polygon({
              paths: this.property.all_areas[this.areaindex].slots_full_details[
                areas
              ].slot_coordinate,
              strokeColor: this.property.all_areas[this.areaindex]
                .slots_full_details[areas].slot_color,
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: this.property.all_areas[this.areaindex]
                .slots_full_details[areas].slot_color,
              fillOpacity: 0.2,
              editable: true,
              draggable: true,
              type: "polygon"
            });
            poly.setMap(this.map_canvas);
            this.all_overlays.push(poly);
            this.selectedShape = poly;
            this.setSelection(this.selectedShape, areas);
            this.updatevents(this.selectedShape);
          } else if (
            this.property.all_areas[this.areaindex].slots_full_details[areas]
              .shapetype == "circle"
          ) {
            var circle = new google.maps.Circle({
              strokeColor: this.property.all_areas[this.areaindex]
                .slots_full_details[areas].slot_color,
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: this.property.all_areas[this.areaindex]
                .slots_full_details[areas].slot_color,
              fillOpacity: 0.2,
              map: this.map,
              center: {
                lat: this.auth.convertToint(
                  this.property.all_areas[this.areaindex].slots_full_details[
                    areas
                  ].slot_circle_lat
                ),
                lng: this.auth.convertToint(
                  this.property.all_areas[this.areaindex].slots_full_details[
                    areas
                  ].slot_circle_lng
                )
              },
              radius: this.auth.convertToint(
                this.property.all_areas[this.areaindex].slots_full_details[
                  areas
                ].slot_circle_radious
              ),
              indexID: this.property.all_areas[this.areaindex]
                .slots_full_details[areas],
              editable: true,
              draggable: true,
              type: "circle"
            });
            circle.setMap(this.map_canvas);
            this.all_overlays.push(circle);
            this.selectedShape = circle;
            this.setSelection(this.selectedShape, areas);
            this.updatevents(this.selectedShape);
          }
        }
      }
      var polyOptions = {
        fillColor: this.selectedArea.area_color,
        strokeColor: this.selectedArea.area_color,
        strokeWeight: 1,
        fillOpacity: 0.45,
        editable: true,
        draggable: true,
        clickable: true
      };

      this.drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.LEFT_BOTTOM,
          drawingModes: [
            google.maps.drawing.OverlayType.POLYGON,
            google.maps.drawing.OverlayType.RECTANGLE,
            google.maps.drawing.OverlayType.CIRCLE
          ]
        },
        polylineOptions: {
          editable: true,
          draggable: true,
          clickable: true
        },
        polygonOptions: polyOptions,
        rectangleOptions: polyOptions,
        circleOptions: polyOptions,
        map: this.map_canvas
      });

      google.maps.event.addListener(
        this.drawingManager,
        "overlaycomplete",
        (e: any) => {
          this.all_overlays.push(e.overlay);

          if (e.type != google.maps.drawing.OverlayType.MARKER) {
            // Switch back to non-drawing mode after drawing a shape.
            this.drawingManager.setDrawingMode(null);
            var newShape = e.overlay;
            newShape.type = e.type;
            this.checkcords(newShape);
            google.maps.event.addListener(newShape, "click", () => {
              this.checkcords(newShape);
              this.editSelection(newShape);
            });

            google.maps.event.addListener(newShape, "mouseup", () => {
              this.checkcords(newShape);
              this.editSelection(newShape);
            });
            google.maps.event.addListener(newShape, "dragend", () => {
              this.checkcords(newShape);
              this.editSelection(newShape);
            });
            google.maps.event.addListener(newShape, "resize", () => {
              this.checkcords(newShape);
              this.editSelection(newShape);
            });
            google.maps.event.addListener(newShape, "bounds_changed", () => {
              this.checkcords(newShape);
              this.editSelection(newShape);
            });
            // google.maps.event.addListener(newShape, "radius_changed", () => {
            //   console.log("radius_changed");
            //   this.checkcords(newShape);
            //   this.editSelection(newShape);
            // });
            // google.maps.event.addListener(newShape, "center_changed", () => {
            //   console.log("center_changed");
            //   this.checkcords(newShape);
            //   this.editSelection(newShape);
            // });
            google.maps.event.addListener(newShape, "dblclick", () => {
              this.checkcords(newShape);
              this.setSelection(newShape, null);
            });
            this.setSelection(newShape, null);
          }
        }
      );
      google.maps.event.addListener(this.map_canvas, "click", () => {
        this.clearSelection(this.selectedShape);
      });
    });
  };

  /**************************/
  /*****Check Coordinates****/
  /**************************/

  checkcords(newShape) {
    // ShapeChords
    if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
      document.getElementById("info").innerHTML = "";
      document.getElementById("info").innerHTML += "[";
      for (var i = 0; i < newShape.getPath().getLength(); i++) {
        if (i == newShape.getPath().getLength() - 1) {
          document.getElementById("info").innerHTML += JSON.stringify(
            newShape
              .getPath()
              .getAt(i)
              .toUrlValue(6)
          );
        } else {
          document.getElementById("info").innerHTML +=
            JSON.stringify(
              newShape
                .getPath()
                .getAt(i)
                .toUrlValue(6)
            ) + ",";
        }
      }
      document.getElementById("info").innerHTML += "]";
    } else if (newShape.type === google.maps.drawing.OverlayType.RECTANGLE) {
      var bounds = newShape.getBounds();
      var aNorth = bounds.getNorthEast().lat();
      var aEast = bounds.getNorthEast().lng();
      var aSouth = bounds.getSouthWest().lat();
      var aWest = bounds.getSouthWest().lng();
      this.shapecords = {
        south: aSouth,
        west: aWest,
        north: aNorth,
        east: aEast
      };
      document.getElementById("output").innerHTML = bounds.toString();
    } else if (newShape.type === google.maps.drawing.OverlayType.CIRCLE) {
      this.selectedArea.slot_circle_lat = newShape.getCenter().lat();
      this.selectedArea.slot_circle_lng = newShape.getCenter().lng();
    }
  }

  /*************************/
  /******Set Selection******/
  /*************************/
  setSelection(shape, index) {
    this.clearSelection(shape);
    this.selectedShape = shape;
    // this.selectedShape.setEditable(true);
    this.selectColor(shape.get("fillColor") || shape.get("strokeColor"));
    if (this.selectedShape.type == "polygon") {
      this.area = google.maps.geometry.spherical.computeArea(
        this.selectedShape.getPath()
      );
      this.selectedcordShape = this.selectedShape;
      this.area = this.area / 4046.856;
      if (typeof this.shapecords == "string" && this.shapecords != "") {
        this.shapecords = this.shapecords.replace(/\\/g, "");
        this.shapecords = JSON.parse(this.shapecords);

        if (this.shapecords.length && this.shapecords[0].south) {
          if (document.getElementById("info").innerHTML == "") {
            for (var i = 0; i < shape.getPath().getLength(); i++) {
              if (i != shape.getPath().getLength() - 1) {
                document.getElementById("info").innerHTML +=
                  shape
                    .getPath()
                    .getAt(i)
                    .toUrlValue(6) + '","';
              } else if (i == shape.getPath().getLength() - 1) {
                document.getElementById("info").innerHTML += shape
                  .getPath()
                  .getAt(i)
                  .toUrlValue(6);
              }
            }
            // this.shapecords = document.getElementById("info").innerHTML;
            this.shapecords =
              '["' + document.getElementById("info").innerHTML + '"]';
          } else {
            this.shapecords = document.getElementById("info").innerHTML;
            this.shapecords = JSON.stringify(this.shapecords);
          }
        }
      } else {
        if (document.getElementById("info").innerHTML == "") {
          for (var i = 0; i < shape.getPath().getLength(); i++) {
            if (i != shape.getPath().getLength() - 1) {
              document.getElementById("info").innerHTML +=
                shape
                  .getPath()
                  .getAt(i)
                  .toUrlValue(6) + '","';
            } else if (i == shape.getPath().getLength() - 1) {
              document.getElementById("info").innerHTML += shape
                .getPath()
                .getAt(i)
                .toUrlValue(6);
            }
          }
          this.shapecords =
            '["' + document.getElementById("info").innerHTML + '"]';
          this.shapecords = document.getElementById("info").innerHTML;
        } else {
          if (document.getElementById("info").innerHTML.substr(1, 1) == "[") {
            this.shapecords = document.getElementById("info").innerHTML;
          } else {
            // this.shapecords =
            //   '["' + document.getElementById("info").innerHTML + '"]';
            this.shapecords = document.getElementById("info").innerHTML;
          }
        }
      }
      var slot_customcenter =
        "[" +
        this.map_canvas.getCenter().lat() +
        "," +
        this.map_canvas.getCenter().lng() +
        "]";
      if (typeof this.shapecords == "object") {
        this.shapecords = '["' + this.shapecords + '"]';
        // this.shapecords = "[" + JSON.stringify(this.shapecords) + "]";
      } else if (this.shapecords.substr(0, 1) != "[") {
        this.shapecords = '["' + this.shapecords + '"]';
      }
      debugger;
      // Insert Slot
      this.insertSlot(
        this.area,
        this.shapecords,
        this.selectedcordShape,
        this.all_overlays,
        this.selectedArea,
        slot_customcenter,
        index
      );
    } else if (this.selectedShape.type == "rectangle") {
      var sw = shape.getBounds().getSouthWest();
      var ne = shape.getBounds().getNorthEast();
      var southWest = new google.maps.LatLng(sw.lat(), sw.lng());
      var northEast = new google.maps.LatLng(ne.lat(), ne.lng());
      var southEast = new google.maps.LatLng(sw.lat(), ne.lng());
      var northWest = new google.maps.LatLng(ne.lat(), sw.lng());
      this.area = google.maps.geometry.spherical.computeArea([
        northEast,
        northWest,
        southWest,
        southEast
      ]);
      this.selectedcordShape = this.selectedShape;
      // this.getlatlng();
      this.area = this.area / 4046.856;
      // this.area = this.area * 0.0002471054;
      if (
        this.selectedArea.slots_full_details[index] &&
        this.selectedArea.slots_full_details[index].slot_coordinates
      ) {
        this.shapecords = this.selectedArea.slots_full_details[
          index
        ].slot_coordinates;
      }
      if (typeof this.shapecords == "string") {
        this.shapecords = this.selectedArea.slots_full_details[
          index
        ].slot_coordinates.replace(/\\/g, "");
      } else {
        this.shapecords = JSON.stringify(this.shapecords);
        this.shapecords = "[" + this.shapecords + "]";
      }
      var slot_customcenter =
        "[" +
        this.map_canvas.getCenter().lat() +
        "," +
        this.map_canvas.getCenter().lng() +
        "]";

      // Insert Slot
      this.insertSlot(
        this.area,
        this.shapecords,
        this.selectedcordShape,
        this.all_overlays,
        this.selectedArea,
        slot_customcenter,
        index
      );
    } else if (this.selectedShape.type == "circle") {
      this.selectedcordShape = this.selectedShape;
      this.area =
        this.selectedShape.getRadius() *
        this.selectedShape.getRadius() *
        Math.PI *
        0.0002471054;
      this.area = (this.area * 100) / 100;
      this.shapecords = "";
      var slot_customcenter =
        "[" +
        this.map_canvas.getCenter().lat() +
        "," +
        this.map_canvas.getCenter().lng() +
        "]";
      // Insert Slot
      this.insertSlot(
        this.area,
        this.shapecords,
        this.selectedcordShape,
        this.all_overlays,
        this.selectedArea,
        slot_customcenter,
        index
      );
    }
  }

  /*************************/
  /******Edit Selection*****/
  /*************************/
  editSelection(shape) {
    this.clearSelection(shape);
    this.selectedShape = shape;
    this.selectedShape.setEditable(true);
    if (this.selectedShape.type == "polygon") {
      this.area = google.maps.geometry.spherical.computeArea(
        this.selectedShape.getPath()
      );
      this.selectedcordShape = this.selectedShape;
      // this.getlatlng();
      this.area = this.area / 4046.856;
      // this.area = this.area * 0.0002471054;
      if (typeof this.shapecords == "string" && this.shapecords != "") {
        this.shapecords = this.shapecords.replace(/\\/g, "");
        this.shapecords = JSON.parse(this.shapecords);
        if (this.shapecords.length && this.shapecords[0].south) {
          this.shapecords = document.getElementById("info").innerHTML;
          this.shapecords = JSON.stringify(this.shapecords);
        }
      } else {
        this.shapecords = document.getElementById("info").innerHTML;
      }

      if (typeof this.shapecords == "object") {
        this.shapecords = document.getElementById("info").innerHTML;
      }
      // Update Slot
      this.updateSlot(this.shapecords, this.area);
    } else if (this.selectedShape.type == "rectangle") {
      var sw = shape.getBounds().getSouthWest();
      var ne = shape.getBounds().getNorthEast();
      var southWest = new google.maps.LatLng(sw.lat(), sw.lng());
      var northEast = new google.maps.LatLng(ne.lat(), ne.lng());
      var southEast = new google.maps.LatLng(sw.lat(), ne.lng());
      var northWest = new google.maps.LatLng(ne.lat(), sw.lng());
      this.area = google.maps.geometry.spherical.computeArea([
        northEast,
        northWest,
        southWest,
        southEast
      ]);
      this.selectedcordShape = this.selectedShape;
      this.area = this.area / 4046.856;
      // this.area = this.area * 0.0002471054;
      if (typeof this.shapecords == "string") {
      } else {
        this.shapecords = JSON.stringify(this.shapecords);
        this.shapecords = "[" + this.shapecords + "]";
      }

      // Update Slot
      this.updateSlot(this.shapecords, this.area);
    } else if (this.selectedShape.type == "circle") {
      this.selectedcordShape = this.selectedShape;
      this.area =
        this.selectedShape.getRadius() *
        this.selectedShape.getRadius() *
        Math.PI *
        0.0002471054;
      this.area = (this.area * 100) / 100 + " Acres";

      if (typeof this.shapecords == "string") {
      } else {
        this.shapecords = "";
      }
      var slot_customcenter =
        "[" +
        this.selectedShape.getCenter().lat() +
        "," +
        this.selectedShape.getCenter().lng() +
        "]";
      // Update Slot
      this.updateSlot(this.shapecords, this.area);
    }
  }

  /*************************/
  /******Insert Slot********/
  /*************************/
  insertSlot(
    area,
    shapecords,
    shape,
    overlays,
    selectedarea,
    slot_customcenter,
    index
  ) {
    if (shape.radius) {
      shapecords = "";
    } else {
      shape.radius = "";
    }
    if (shapecords) {
    } else {
      shapecords = "";
    }

    if (
      index == 0 ||
      (index > 0 && selectedarea.slots_full_details[index].id)
    ) {
      var indexs = this.areaindex + 1;
      var slotstr: any;
      if (
        selectedarea.slots_full_details &&
        selectedarea.slots_full_details[index] &&
        selectedarea.slots_full_details[index].slotname
      ) {
        debugger;
        slotstr = selectedarea.slots_full_details[index].slotname;
      } else {
        if (this.selectedArea.slots.length == 0) {
          debugger;
          slotstr = "A" + indexs + "-" + 1;
        } else {
          debugger;
          slotstr = this.auth.convertArea(
            this.selectedArea.slots[this.selectedArea.slots.length - 1].slotname
          );
          slotstr = "A" + indexs + "-" + slotstr;
        }
      }

      this.setslotname(
        selectedarea,
        index,
        shapecords,
        slot_customcenter,
        shape,
        slotstr,
        area,
        indexs
      );
    } else {
      var indexs = this.areaindex + 1;
      var slotstr: any;
      if (
        selectedarea.slots_full_details &&
        selectedarea.slots_full_details[index] &&
        selectedarea.slots_full_details[index].slotname
      ) {
        slotstr = selectedarea.slots_full_details[index].slotname;
      } else {
        if (this.selectedArea.slots.length == 0) {
          debugger;
          slotstr = "A" + indexs + "-" + 1;
        } else {
          debugger;
          slotstr = this.auth.convertArea(
            this.selectedArea.slots[this.selectedArea.slots.length - 1].slotname
          );
          slotstr = "A" + indexs + "-" + slotstr;
        }
      }

      this.setslotname1(
        selectedarea,
        index,
        shapecords,
        slot_customcenter,
        shape,
        slotstr,
        area,
        indexs
      );
    }
  }

  setslotname(
    selectedarea,
    index,
    shapecords,
    slot_customcenter,
    shape,
    slotstr,
    area,
    indexs
  ) {
    debugger;
    var slot: any = {
      edit: true,
      id: selectedarea.slots_full_details[index].id,
      area_id: selectedarea.area_id,
      prop_id: this.property.property_id,
      slot_location: "",
      slot_coordinates: shapecords,
      slot_customcenter: slot_customcenter,
      slot_circle_lat: selectedarea.slots_full_details[index].slot_circle_lat,
      slot_circle_lng: selectedarea.slots_full_details[index].slot_circle_lng,
      slot_circle_radious: shape.radius,
      shapetype: shape.type,
      slotname: slotstr,
      slot_area: area + " Acres",
      slot_status: 1,
      slot_color: shape.strokeColor,
      slot_datecreated: this.datepipe.transform(
        new Date(),
        "yyyy-MM-dd hh:mm:ss"
      ),
      slot_createdby: this.auth.getuserId()
    };
    this.selectedArea.slots.push(slot);
    this.shapecords = "";
  }
  setslotname1(
    selectedarea,
    index,
    shapecords,
    slot_customcenter,
    shape,
    slotstr,
    area,
    indexs
  ) {
    debugger;
    if (shape.type == "circle") {
      var slot: any = {
        area_id: selectedarea.area_id,
        prop_id: this.property.property_id,
        slot_location: "",
        slot_coordinates: shapecords,
        slot_customcenter: slot_customcenter,
        slot_circle_lat: selectedarea.slot_circle_lat,
        slot_circle_lng: selectedarea.slot_circle_lng,
        slot_circle_radious: shape.radius,
        shapetype: shape.type,
        slotname: slotstr,
        slot_area: area + " Acres",
        slot_status: 1,
        slot_color: shape.strokeColor,
        slot_datecreated: this.datepipe.transform(
          new Date(),
          "yyyy-MM-dd hh:mm:ss"
        ),
        slot_createdby: this.auth.getuserId()
      };
      this.selectedArea.slots.push(slot);
    } else {
      var slot: any = {
        area_id: selectedarea.area_id,
        prop_id: this.property.property_id,
        slot_location: "",
        slot_coordinates: shapecords,
        slot_customcenter: slot_customcenter,
        slot_circle_lat: "",
        slot_circle_lng: "",
        slot_circle_radious: shape.radius,
        shapetype: shape.type,
        slotname: slotstr,
        slot_area: area + " Acres",
        slot_status: 1,
        slot_color: shape.strokeColor,
        slot_datecreated: this.datepipe.transform(
          new Date(),
          "yyyy-MM-dd hh:mm:ss"
        ),
        slot_createdby: this.auth.getuserId()
      };
      this.selectedArea.slots.push(slot);
      this.shapecords = "";
    }
  }
  /*************************/
  /******Update Slot********/
  /*************************/

  updateSlot(slot_coordinates, area) {
    for (var c = 0; c < this.all_overlays.length; c++) {
      if (this.selectedShape == this.all_overlays[c]) {
        // Slot coordinate changed
        if (slot_coordinates) {
          this.selectedArea.slots[c].slot_coordinates = slot_coordinates;
        }
        // Slot area changed
        this.selectedArea.slots[c].slot_area = area;
        if (this.selectedArea.slots[c].shapetype == "circle") {
          this.selectedArea.slots[
            c
          ].slot_circle_lat = this.selectedShape.getCenter().lat();
          this.selectedArea.slots[
            c
          ].slot_circle_lng = this.selectedShape.getCenter().lng();
          this.selectedArea.slots[
            c
          ].slot_circle_radious = this.selectedShape.getRadius();
        } else {
          this.selectedArea.slots[c].slot_circle_lat = "";
          this.selectedArea.slots[c].slot_circle_lng = "";
        }
      }
    }
  }

  /**************************************/
  /********Send Data to save slot********/
  /**************************************/

  saveSlot() {
    this.postdata();
  }
  postdata() {
    debugger;
    if (this.selectedArea.slots && this.selectedArea.slots.length) {
      this.auth.startloader();
      try {
        this.auth
          .createprop4(
            this.auth.getuserId(),
            this.selectedArea.slots,
            this.property.property_id
          )
          .subscribe(data => {
            this.auth.stoploader();
            if (data) {
              if (data.json().status == 1) {
                var ele = this;
                if (
                  data.json().inserted_slots &&
                  data.json().inserted_slots.length
                ) {
                  for (
                    var slt = 0;
                    slt < data.json().inserted_slots.length;
                    slt++
                  ) {
                    ele.selectedArea.slots[slt].id = data.json().inserted_slots[
                      slt
                    ];
                    ele.selectedArea.slots[slt].edit = true;
                  }
                  // ele.pushdata();
                }
                ele.auth.toast(data.json().message);
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else {
                this.auth.toast(data.json().message);
              }
            }
          });
      } catch (e) {
        this.auth.stoploader();
        this.auth.toast(e);
      }
    }
  }
  pushdata() {
    for (
      var aslot = 0;
      aslot < this.selectedArea.slots_full_details.length;
      aslot++
    ) {
      for (var bslot = 0; bslot < this.selectedArea.slots.length; bslot++) {
        if (aslot == bslot) {
        } else {
          this.selectedArea.slots_full_details.push(
            this.selectedArea.slots[aslot]
          );
        }
      }
    }
  }
  deleteSelectedShape = (): void => {
    if (this.selectedShape) {
      for (var x = 0; x < this.all_overlays.length; x++) {
        if (this.selectedShape == this.all_overlays[x]) {
          this.deleteSlot(this.all_overlays[x], x);
          // this.clickEvent(this.selectedArea.slots[x]);
        }
      }
    }
    this.status = !this.status;
    this.show = !this.show;
  };
  /**************************************/
  /**************Delete slot*************/
  /**************************************/

  async deleteSlot(id, x) {
    const alert = await this.alertController.create({
      message: "Are you sure you want to delete this slot ?",
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {}
        },
        {
          text: "Yes",
          handler: () => {
            this.deleteslot(id, x);
          }
        }
      ]
    });

    await alert.present();
  }
  deleteslot(id, x) {
    if (this.selectedArea.slots_full_details[x]) {
      this.auth.startloader();
      try {
        this.auth
          .deleteslot(
            this.auth.getuserId(),
            this.selectedArea.slots_full_details[x].id
          )
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data) {
                if (data.json().status == 1) {
                  this.all_overlays.splice(x, 1);
                  this.selectedArea.slots.splice(x, 1);
                  this.selectedShape.setMap(null);
                  this.auth.toast(data.json().message);
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                } else {
                  this.auth.toast(data.json().message);
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.errtoast(errorHandler);
            }
          );
      } catch (e) {
        this.auth.stoploader();
        this.auth.toast(e);
      }
    } else {
      // this.clearSelection(id);
      this.deleteSelectedShapes(id, x);
    }
  }

  deletedata() {
    if (this.selectedArea.slots && this.selectedArea.slots.length) {
      try {
        this.auth
          .createprop4(this.auth.getuserId(), this.selectedArea.slots, null)
          .subscribe(data => {
            if (data) {
              if (data.json().status == 1) {
                if (
                  data.json().inserted_slots &&
                  data.json().inserted_slots.length
                ) {
                  for (
                    var slt = 0;
                    slt < data.json().inserted_slots.length;
                    slt++
                  ) {
                    this.selectedArea.slots[
                      slt
                    ].id = data.json().inserted_slots[slt];
                  }
                }
                this.auth.toast(data.json().message);
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else {
                this.auth.toast(data.json().message);
              }
            }
          });
      } catch (e) {
        this.auth.toast(e);
      }
    }
  }

  /**************************************/
  /**********Delete All Shapes***********/
  /**************************************/
  deleteAllShapes() {
    for (var i = 0; i < this.all_overlays.length; i++) {
      this.all_overlays[i].setMap(null);
    }
    this.all_overlays = [];
    this.selectedArea.slots = [];
  }

  /**************************************/
  /**********Delete selected Shape***********/
  /**************************************/
  deleteSelectedShapes(id, x) {
    for (var i = 0; i < this.all_overlays.length; i++) {
      this.all_overlays[x].setMap(null);
    }
    this.all_overlays.splice(x, 1);
    this.selectedArea.slots.splice(x, 1);
  }

  goto(page) {
    // this.navCtrl.push(page, { property_data: this.property });
    this.viewCtrl.dismiss({ push: true, currpage: page });
  }
}
