import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AddslotpropertyPage } from "./addslotproperty";
import { AgmCoreModules } from "../../agm/core";

@NgModule({
  declarations: [AddslotpropertyPage],
  imports: [AgmCoreModules, IonicPageModule.forChild(AddslotpropertyPage)]
})
export class AddslotpropertyPageModule {}
