import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the PasswordassistancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-passwordassistance",
  templateUrl: "passwordassistance.html"
})
export class PasswordassistancePage {
  reset: any = false;
  otp: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {
    if (this.navParams.get("page") == "true") {
      this.reset = true;
    } else if (this.navParams.get("page") == "false") {
      this.reset = false;
    }
  }

  ionViewDidLoad() {}

  submitreset() {
    if (this.otp == "" || null || undefined) {
      this.auth.toast("Please Enter pin number here. ");
    } else {
      this.navCtrl.push("ResetpasswordPage");
    }
  }

  enter() {
    this.navCtrl.setRoot("LoginPage");
  }
}
