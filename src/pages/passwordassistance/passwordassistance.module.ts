import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PasswordassistancePage } from "./passwordassistance";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [PasswordassistancePage],
  imports: [ComponentsModule, IonicPageModule.forChild(PasswordassistancePage)]
})
export class PasswordassistancePageModule {}
