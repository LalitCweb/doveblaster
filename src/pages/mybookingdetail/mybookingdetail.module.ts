import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MybookingdetailPage } from "./mybookingdetail";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [MybookingdetailPage],
  imports: [IonicPageModule.forChild(MybookingdetailPage), PipesModule]
})
export class MybookingdetailPageModule {}
