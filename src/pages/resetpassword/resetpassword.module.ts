import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ResetpasswordPage } from "./resetpassword";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [ResetpasswordPage],
  imports: [ComponentsModule, IonicPageModule.forChild(ResetpasswordPage)]
})
export class ResetpasswordPageModule {}
