import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  MenuController
} from "ionic-angular";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";
import { FcmProvider } from "../../providers/fcm/fcm";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage implements OnInit {
  // Validation form group definition
  rememberme: any = true;
  configs: FormGroup;
  user: any = { email: "", password: "" };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public auth: AuthProvider,
    public fcmp: FcmProvider,
    public platform: Platform
  ) {
    // Disable Side Menu
    this.menuCtrl.enable(false, "myMenu");
    if (this.navParams.get("dayleasing_logout")) {
      if (this.navParams.get("dayleasing_logout") == "logout") {
        this.auth.startloader();
        setTimeout(() => {
          this.auth.stoploader();
          // window.location.reload();
        }, 2000);
      }
    }
    if (this.navParams.get("userpass")) {
      this.gologin(
        this.navParams.get("userpass").username,
        this.navParams.get("userpass").password
      );
    }
  }

  ngOnInit() {
    // Validations Definition
    // let EMAILPATTERNS = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.configs = new FormGroup({
      email: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    });
  }
  ionViewDidLoad() {}
  ionViewWillEnter() {
    if (this.auth.havelogincredentials()) {
      this.user.email = this.auth.getlogincredentials().username;
      this.user.password = this.auth.getlogincredentials().password;
    }
  }
  pleaseUnsubscribe() {
    if (this.platform.is("cordova")) {
      if (this.logintokenSubscribe) {
        this.logintokenSubscribe.unsubscribe();
      }
      if (this.logintokenuserSubscribe) {
        this.logintokenuserSubscribe.unsubscribe();
      }
    }
  }

  goto(page) {
    this.navCtrl.push(page);
  }

  gotoinfo(data) {
    this.navCtrl.push("InfoPage", { info: data });
  }

  owner: any = {
    user_email: "owner@gmail.com",
    fname: "Owner",
    lname: "Name"
  };

  login() {
    if (this.configs.valid) {
      this.gologin(this.configs.value.email, this.configs.value.password);
    } else {
      for (let i in this.configs.controls)
        this.configs.controls[i].markAsTouched();
    }
  }

  options: any;
  gologin(email, password) {
    this.auth.clearcookie();
    this.auth.startloader();
    this.auth.postlogin(email, password).subscribe(
      data => {
        var user = data.json();
        if (user.token != undefined) {
          localStorage.setItem("dayleasing_user", JSON.stringify(user));
          this.auth.clearcookie();
          this.getdetail(email);
          this.auth.stoploader();
          if (this.rememberme) {
            this.auth.savelogincredentials({
              username: email,
              password: password
            });
          } else {
            this.auth.removelogincredentials();
          }
        } else if (user.data && user.data.status && user.data.status == 403) {
          if (user.message) {
            this.auth.toast(user.message);
          } else {
            this.auth.toast("Something problem with login ");
          }
          this.auth.stoploader();
        }
        // localStorage.setItem("dayleasing_user", "");
      },
      errorHandler => {
        if (
          errorHandler.json().data &&
          errorHandler.json().data.status &&
          errorHandler.json().data.status == 403
        ) {
          if (errorHandler.json().code == "[jwt_auth] invalid_username") {
            this.auth.toast("Invalid username.");
          } else if (
            errorHandler.json().code == "[jwt_auth] incorrect_password"
          ) {
            this.auth.toast("The password you entered is incorrect..");
          } else {
            this.auth.toast("Something problem with login ");
          }
          this.auth.stoploader();
        }
      },
      () => {
        this.auth.stoploader();
      }
    );
  }

  logintoken: any;
  logintokenuser: any;
  logintokenSubscribe: any;
  logintokenuserSubscribe: any;
  generatefcm() {
    this.auth.clearcookie();
    if (this.platform.is("cordova")) {
      this.logintoken = this.fcmp.checkusers().valueChanges();
      this.logintokenSubscribe = this.logintoken.subscribe(queriedItems => {
        if (queriedItems.length == 0) {
          this.fcmp.getToken();
          this.pleaseUnsubscribe();
          this.navCtrl.setRoot("PropertieslistviewPage");
        } else {
          this.logintokenuser = this.fcmp.checkusers().snapshotChanges();
          this.logintokenuserSubscribe = this.logintokenuser.subscribe(
            queriedItems => {
              queriedItems.forEach(action => {
                this.fcmp.updateToken(action.key);
                this.pleaseUnsubscribe();
              });
            }
          );
          this.navCtrl.setRoot("PropertieslistviewPage");
        }
        if (this.auth.isLogin() == false) {
          this.pleaseUnsubscribe();
        } else {
        }
      });
    }
  }

  getdetail(cname) {
    this.auth.clearcookie();
    try {
      this.auth.getuserdata(cname).subscribe(
        data => {
          if (data.json()) {
            if (data.json().status == 1) {
              this.auth.clearcookie();
              localStorage.setItem(
                "dayleasing_user_type",
                JSON.stringify(data.json().posts)
              );
              this.auth.clearcookie();
              this.generatefcm();
              this.auth.clearcookie();
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.toast("Not able to get user data");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
}
