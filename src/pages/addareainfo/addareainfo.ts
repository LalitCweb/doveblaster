import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the AddareainfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-addareainfo",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "addareainfo.html"
})
export class AddareainfoPage {
  slots: any = [];
  savedslots: any = [];
  today: any = new Date().toISOString();
  property: any;
  slot = {
    prop_area_name: "",
    prop_area_slot_details: "",
    prop_area_slot_color: "#000",
    detail: [
      {
        prop_area_start_date: "",
        prop_area_end_date: "",
        prop_area_cost: ""
      }
    ]
  };
  max: any = "2099-10-31";
  myForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    public auth: AuthProvider,
    public modalCtrl: ModalController
  ) {
    this.myForm = this.fb.group({
      prop_area_name: ["", Validators.required],
      prop_area_slot_details: ["", Validators.required],
      prop_area_slot_color: ["#000", Validators.required],
      detail: this.fb.array([])
    });
    this.property = this.navParams.get("property_data");
    this.setSlot();
    this.getpropertydata(this.property.property_id);
  }

  getpropertydata(propertyid) {
    try {
      this.auth.getsinglealldetail(propertyid, this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            if (data.json() && data.json().status == 1) {
              if (
                data.json() &&
                data.json().events &&
                data.json().events.all_areas
              ) {
                debugger;
                this.savedslots = data.json().events.all_areas;
              } else {
              }
            }
          }
        },
        errorHandler => {
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (err) {
      this.auth.errtoast(err);
    }
  }
  chngedate(value) {
    if (value.value.prop_area_start_date) {
    }
  }

  chngedatemin(value) {
    if (value.value.prop_area_start_date) {
    }
  }

  setSlot() {
    let control = <FormArray>this.myForm.controls.detail;
    if (control.valid == true) {
      if (control.value && control.value.length) {
        control.push(
          this.fb.group({
            prop_area_cost: ["", Validators.required],
            prop_area_start_date: ["", Validators.required],
            prop_area_end_date: ["", Validators.required],
            min: this.auth.checkdate(
              control.value[control.value.length - 1].prop_area_end_date,
              1
            )
          })
        );
      } else {
        control.push(
          this.fb.group({
            prop_area_cost: ["", Validators.required],
            prop_area_start_date: ["", Validators.required],
            prop_area_end_date: ["", Validators.required],
            min: new Date().toISOString()
          })
        );
      }
    } else {
      this.auth.toast("Please fill all fields");
    }
  }

  deleteall() {
    let control = <FormArray>this.myForm.controls.detail;
    for (var x = control.length; x >= 0; x--) {
      control.removeAt(x);
    }
    control.push(
      this.fb.group({
        prop_area_cost: ["", Validators.required],
        prop_area_start_date: ["", Validators.required],
        prop_area_end_date: ["", Validators.required],
        min: new Date().toISOString()
      })
    );
  }
  deletedates(controls, index) {
    controls.removeAt(index);
  }

  ionViewDidLoad() {}

  goto(page) {
    if (page == "AddslotpropertyPage") {
      let profileModal = this.modalCtrl.create(page, {
        property_data: {
          index: 0,
          property: this.property
        }
      });
      profileModal.present();
      profileModal.onDidDismiss(data => {
        if (data && data.push) {
          this.navCtrl.push(data.currpage, { property_data: this.property });
        } else {
          // if (this.property && this.property.property_id) {
          //   this.getsingledetailpage(this.property.property_id);
          // }
        }
      });
    } else {
      if (this.slots && this.slots.length) {
        this.property.slots = this.slots;
        this.navCtrl.push(page, {
          property_data: this.property
        });
      }
    }
  }

  status: boolean = false;
  show: boolean = false;
  currentSelected: any = {};
  clickEvent(slot, index) {
    if (slot) {
      this.currentSelected = slot;
      this.currentSelected.index = index;
    }
    this.status = !this.status;
    this.show = !this.show;
  }

  addareas() {
    if (this.myForm.valid) {
      var slt = [];
      slt.push(this.myForm.value);
      this.continue(slt, this.myForm.value);
    } else {
      this.auth.toast("Please fill complete form");
    }
  }

  copy() {
    var slts = [];
    slts.push(this.currentSelected);
    this.continue(slts, this.currentSelected);
    this.clickEvent(null, null);
  }
  delete() {
    this.slots.splice(this.currentSelected.index, 1);
    this.clickEvent(null, null);
  }

  continue(slots, form) {
    this.auth.startloader();
    try {
      if (slots && slots.length) {
        this.auth
          .createprop3(
            this.auth.getuserId(),
            this.property.property_id,
            this.property.property_id,
            slots
          )
          .subscribe(data => {
            this.auth.stoploader();
            if (data) {
              if (data.json().status == 1) {
                this.slots.push(form);
                this.deleteall();
                this.myForm.reset();
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              }
            }
          });
      }
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
}
