import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddareainfoPage } from './addareainfo';

@NgModule({
  declarations: [
    AddareainfoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddareainfoPage),
  ],
})
export class AddareainfoPageModule {}
