import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  Platform
} from "ionic-angular";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
/**
 * Generated class for the PropertiesmapviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-propertiesmapview",
  templateUrl: "propertiesmapview.html"
})
export class PropertiesmapviewPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public popoverCtrl: PopoverController,
    private nativePageTransitions: NativePageTransitions
  ) {}
  ionViewWillEnter() {
    if (this.platform.is("cordova")) {
      let options: NativeTransitionOptions = {
        direction: "up",
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60
      };

      this.nativePageTransitions
        .slide(options)
        .then()
        .catch();
    }
  }
  ionViewDidLoad() {}

  listviewpop() {
    const popover = this.popoverCtrl.create(
      "ListviewpopPage",
      {},
      { cssClass: "listviewpop" }
    );
    popover.present();
  }

  dateviewpop() {
    const popover = this.popoverCtrl.create(
      "DateviewpopPage",
      {},
      { cssClass: "dateviewpop" }
    );
    popover.present();
  }

  moreviewpop() {
    const popover = this.popoverCtrl.create(
      "MoreviewpopPage",
      {},
      { cssClass: "moreviewpop" }
    );
    popover.present();
  }

  propertiesmapfilter() {
    const popover = this.popoverCtrl.create(
      "PropertiesmapfilterPage",
      {},
      { cssClass: "propertiesmapfilter" }
    );
    popover.present();
  }
}
