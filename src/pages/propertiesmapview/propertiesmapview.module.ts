import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PropertiesmapviewPage } from "./propertiesmapview";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [PropertiesmapviewPage],
  imports: [ComponentsModule, IonicPageModule.forChild(PropertiesmapviewPage)]
})
export class PropertiesmapviewPageModule {}
