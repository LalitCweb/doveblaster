import { Component, ViewChild, ElementRef } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { TracklocationProvider } from "../../providers/tracklocation/tracklocation";

declare var navigator: any;
@IonicPage()
@Component({
  selector: "page-locationonmap",
  templateUrl: "locationonmap.html"
})
export class LocationonmapPage {
  @ViewChild("map") mapElement: ElementRef;
  lat: Number;
  lng: Number;
  options: any = {
    lat: Number,
    lng: Number,
    zoom: 18,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    strokeWeight: 3,
    strokePos: 0
  };
  marker: any;
  bookingslotid: any;
  arrayslotdata: any;
  slotarea: any;
  map: any;
  watch: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    public auth: AuthProvider,
    private track: TracklocationProvider
  ) {
    this.bookingslotid = navParams.get("linked_slot_tothis_booking");
    this.arrayslotdata = navParams.get("array_slots_data");
    this.lat = parseFloat(navParams.get("lat"));
    this.lng = parseFloat(navParams.get("lng"));
    if (this.arrayslotdata.property_details.prop_area_shape_type == "polygon") {
      if (this.arrayslotdata.property_details.prop_area_shape_cord) {
      } else {
        this.arrayslotdata.property_details.prop_area_shape_cords = this.arrayslotdata.property_details.prop_area_shape_cords.replace(
          /\\/g,
          ""
        );
        this.arrayslotdata.property_details.prop_area_shape_cords = JSON.parse(
          this.arrayslotdata.property_details.prop_area_shape_cords
        );

        this.arrayslotdata.property_details.prop_area_shape_cord = [];
        var cords: any = {};
        if (
          this.arrayslotdata.property_details.prop_area_shape_cords &&
          this.arrayslotdata.property_details.prop_area_shape_cords.length
        ) {
          for (
            var y = 0;
            y <
            this.arrayslotdata.property_details.prop_area_shape_cords.length;
            y++
          ) {
            if (this.arrayslotdata.property_details.prop_area_shape_cords[y]) {
              this.arrayslotdata.property_details.prop_area_shape_cords[
                y
              ] = this.arrayslotdata.property_details.prop_area_shape_cords[
                y
              ].split(",");
              cords = {
                lat: parseFloat(
                  this.arrayslotdata.property_details.prop_area_shape_cords[
                    y
                  ][0]
                ),
                lng: parseFloat(
                  this.arrayslotdata.property_details.prop_area_shape_cords[
                    y
                  ][1]
                )
              };
              this.arrayslotdata.property_details.prop_area_shape_cord.push(
                cords
              );
              this.arrayslotdata.property_details.prop_area_shape_cords[
                y
              ].lat = this.arrayslotdata.property_details.prop_area_shape_cords[
                y
              ][0];
              this.arrayslotdata.property_details.prop_area_shape_cords[
                y
              ].lng = this.arrayslotdata.property_details.prop_area_shape_cords[
                y
              ][1];
            }
          }
        }
      }
    } else if (
      this.arrayslotdata.property_details.prop_area_shape_type == "rectangle"
    ) {
    }
    for (var x = 0; x < this.arrayslotdata.array_slots_data.length; x++) {
      if (
        this.arrayslotdata.array_slots_data[x].slot_id == this.bookingslotid
      ) {
        this.slotarea = this.arrayslotdata.array_slots_data[x];
      }
    }
  }

  ionViewDidLoad() {}

  ionViewWillEnter() {
    this.track.getlocation();
  }
  ionViewWillLeave() {
    if (this.watch) {
      navigator.geolocation.clearWatch(this.watch);
    }
  }
  /******************/
  /***Touch Events***/
  /******************/
  touchstart(e) {
    if (e.cancelable) {
      e.preventDefault();
    }
  }
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: "top"
    });

    toast.onDidDismiss(() => {});

    toast.present();
  }

  private _map: any;
  changeTilt(map) {
    this._map = map;
    this._map.setTilt(0);
    map.setOptions({
      zoomControl: "true",
      zoomControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      streetViewControl: "true",
      streetViewControlOptions: {
        position: google.maps.ControlPosition.TOP_LEFT
      }
    });
  }
  navme() {
    if (this.track.getlat() && this.track.getlng()) {
      this.lat = this.track.getlat();
      this.lng = this.track.getlng();
    } else {
      this.auth.toast("Currently not having your location");
    }
  }
}
