import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { LocationonmapPage } from "./locationonmap";
import { AgmCoreModule } from "@agm/core";

@NgModule({
  declarations: [LocationonmapPage],
  imports: [AgmCoreModule, IonicPageModule.forChild(LocationonmapPage)]
})
export class LocationonmapPageModule {}
