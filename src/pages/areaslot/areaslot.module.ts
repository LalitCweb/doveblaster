import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AreaslotPage } from './areaslot';

@NgModule({
  declarations: [
    AreaslotPage,
  ],
  imports: [
    IonicPageModule.forChild(AreaslotPage),
  ],
})
export class AreaslotPageModule {}
