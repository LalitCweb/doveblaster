import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

/**
 * Generated class for the AreaslotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-areaslot",
  templateUrl: "areaslot.html"
})
export class AreaslotPage {
  slot1: any = "test";
  slot2: any = "test";
  slot3: any = "test";
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {}
  // change(data) {
  //   if (data == "slot1") {
  //     if (this.slot1 == "test") {
  //       this.slot1 = "availability";
  //     } else if (this.slot1 == "availability") {
  //       this.slot1 = "test";
  //     }
  //   }
  // }

  goto(page) {
    this.navCtrl.push(page);
  }
}
