import { Component, ViewChild, ElementRef } from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import {
  NativeGeocoder,
  NativeGeocoderReverseResult,
  NativeGeocoderOptions
} from "@ionic-native/native-geocoder";
import * as $ from "jquery";

/**
 * Generated class for the EditareaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-editarea",
  templateUrl: "editarea.html"
})
export class EditareaPage {
  @ViewChild("map_canvas") mapElements: ElementRef;
  drawingManager: any;
  map: any;
  map_canvas: any;
  property: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public auth: AuthProvider,
    private nativeGeocoder: NativeGeocoder
  ) {
    this.property = this.navParams.get("property_data");
    this.getpropertydata(this.property.property_id);
    this.initializeMap();
  }

  ionViewDidLoad() {}
  updatevents(newShape) {
    google.maps.event.addListener(newShape, "mouseup", () => {
      this.setSelection(newShape);
      // ShapeChords
      var polygonArray: any = [];
      if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
        document.getElementById("info").innerHTML = "";
        document.getElementById("info").innerHTML += "[";
        for (var i = 0; i < newShape.getPath().getLength(); i++) {
          if (i == newShape.getPath().getLength() - 1) {
            document.getElementById("info").innerHTML += JSON.stringify(
              newShape
                .getPath()
                .getAt(i)
                .toUrlValue(6)
            );
          } else {
            document.getElementById("info").innerHTML +=
              JSON.stringify(
                newShape
                  .getPath()
                  .getAt(i)
                  .toUrlValue(6)
              ) + ",";
          }
        }
        document.getElementById("info").innerHTML += "]";
        polygonArray.push(newShape);
      } else if (newShape.type === google.maps.drawing.OverlayType.RECTANGLE) {
        var bounds = newShape.getBounds();
        var aNorth = bounds.getNorthEast().lat();
        var aEast = bounds.getNorthEast().lng();
        var aSouth = bounds.getSouthWest().lat();
        var aWest = bounds.getSouthWest().lng();
        this.shapecords = {
          south: aSouth,
          west: aWest,
          north: aNorth,
          east: aEast
        };
        document.getElementById("output").innerHTML = bounds.toString();
      }
    });
    google.maps.event.addListener(newShape, "dragend", () => {
      this.setSelection(newShape);
      // ShapeChords
      var polygonArray: any = [];
      if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
        document.getElementById("info").innerHTML = "";
        document.getElementById("info").innerHTML += "[";
        for (var i = 0; i < newShape.getPath().getLength(); i++) {
          if (i == newShape.getPath().getLength() - 1) {
            document.getElementById("info").innerHTML += JSON.stringify(
              newShape
                .getPath()
                .getAt(i)
                .toUrlValue(6)
            );
          } else {
            document.getElementById("info").innerHTML +=
              JSON.stringify(
                newShape
                  .getPath()
                  .getAt(i)
                  .toUrlValue(6)
              ) + ",";
          }
        }
        document.getElementById("info").innerHTML += "]";
        polygonArray.push(newShape);
      } else if (newShape.type === google.maps.drawing.OverlayType.RECTANGLE) {
        var bounds = newShape.getBounds();
        var aNorth = bounds.getNorthEast().lat();
        var aEast = bounds.getNorthEast().lng();
        var aSouth = bounds.getSouthWest().lat();
        var aWest = bounds.getSouthWest().lng();
        this.shapecords = {
          south: aSouth,
          west: aWest,
          north: aNorth,
          east: aEast
        };
        document.getElementById("output").innerHTML = bounds.toString();
      }
    });
    google.maps.event.addListener(newShape, "resize", () => {
      this.setSelection(newShape);
      // ShapeChords
      var polygonArray: any = [];
      if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
        document.getElementById("info").innerHTML = "";
        document.getElementById("info").innerHTML += "[";
        for (var i = 0; i < newShape.getPath().getLength(); i++) {
          if (i == newShape.getPath().getLength() - 1) {
            document.getElementById("info").innerHTML += JSON.stringify(
              newShape
                .getPath()
                .getAt(i)
                .toUrlValue(6)
            );
          } else {
            document.getElementById("info").innerHTML +=
              JSON.stringify(
                newShape
                  .getPath()
                  .getAt(i)
                  .toUrlValue(6)
              ) + ",";
          }
        }
        document.getElementById("info").innerHTML += "]";
        polygonArray.push(newShape);
      } else if (newShape.type === google.maps.drawing.OverlayType.RECTANGLE) {
        var bounds = newShape.getBounds();
        var aNorth = bounds.getNorthEast().lat();
        var aEast = bounds.getNorthEast().lng();
        var aSouth = bounds.getSouthWest().lat();
        var aWest = bounds.getSouthWest().lng();
        this.shapecords = {
          south: aSouth,
          west: aWest,
          north: aNorth,
          east: aEast
        };
        document.getElementById("output").innerHTML = bounds.toString();
      }
    });
    google.maps.event.addListener(newShape, "bounds_changed", () => {
      this.setSelection(newShape);
      // ShapeChords
      var polygonArray: any = [];
      if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
        document.getElementById("info").innerHTML = "";
        document.getElementById("info").innerHTML += "[";
        for (var i = 0; i < newShape.getPath().getLength(); i++) {
          if (i == newShape.getPath().getLength() - 1) {
            document.getElementById("info").innerHTML += JSON.stringify(
              newShape
                .getPath()
                .getAt(i)
                .toUrlValue(6)
            );
          } else {
            document.getElementById("info").innerHTML +=
              JSON.stringify(
                newShape
                  .getPath()
                  .getAt(i)
                  .toUrlValue(6)
              ) + ",";
          }
        }
        document.getElementById("info").innerHTML += "]";
        polygonArray.push(newShape);
      } else if (newShape.type === google.maps.drawing.OverlayType.RECTANGLE) {
        var bounds = newShape.getBounds();
        var aNorth = bounds.getNorthEast().lat();
        var aEast = bounds.getNorthEast().lng();
        var aSouth = bounds.getSouthWest().lat();
        var aWest = bounds.getSouthWest().lng();
        this.shapecords = {
          south: aSouth,
          west: aWest,
          north: aNorth,
          east: aEast
        };
        document.getElementById("output").innerHTML = bounds.toString();
      }
    });
  }
  getpropertydata(propertyid) {
    try {
      this.auth.getsinglealldetail(propertyid, this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            if (data.json() && data.json().status == 1) {
              this.property = data.json().events;
              if (this.property.prop_area_currnt_zoom != undefined) {
                this.property.prop_area_currnt_zoom =
                  parseInt(this.property.prop_area_currnt_zoom) - 2;
              } else {
                this.property.prop_area_currnt_zoom = 18;
              }
              if (this.property.prop_area_shape_type == "rectangle") {
                if (this.property.prop_area_shape_cords) {
                  if (typeof this.property.prop_area_shape_cords == "string") {
                    this.property.prop_area_shape_cords = this.property.prop_area_shape_cords.replace(
                      /\\/g,
                      ""
                    );
                    this.property.prop_area_shape_cords = JSON.parse(
                      this.property.prop_area_shape_cords
                    );
                  }
                  var rect = new google.maps.Rectangle({
                    strokeColor: this.property.prop_area_shape_fillcolor,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: this.property.prop_area_shape_fillcolor,
                    fillOpacity: 0.35,
                    map: this.map_canvas,
                    editable: true,
                    bounds: {
                      north: this.property.prop_area_shape_cords[0].north,
                      south: this.property.prop_area_shape_cords[0].south,
                      east: this.property.prop_area_shape_cords[0].east,
                      west: this.property.prop_area_shape_cords[0].west
                    },
                    type: "rectangle"
                  });
                  rect.setMap(this.map_canvas);
                  this.all_overlays.push(rect);
                  this.selectedShape = rect;
                  this.setSelection(this.selectedShape);
                  this.updatevents(this.selectedShape);
                }
              }
              if (this.property.prop_area_shape_type == "polygon") {
                try {
                  this.property.prop_area_shape_cords = this.property.prop_area_shape_cords.replace(
                    /\\/g,
                    ""
                  );
                  this.property.prop_area_shape_cords = JSON.parse(
                    this.property.prop_area_shape_cords
                  );
                } catch (e) {
                  return;
                }
                this.property.prop_area_shape_cord = [];
                var cords: any = {};
                if (
                  this.property.prop_area_shape_cords &&
                  this.property.prop_area_shape_cords.length
                ) {
                  for (
                    var x = 0;
                    x < this.property.prop_area_shape_cords.length;
                    x++
                  ) {
                    try {
                      this.property.prop_area_shape_cords[
                        x
                      ] = this.property.prop_area_shape_cords[x].split(",");
                    } catch (e) {
                      // return;
                    }
                    cords = {
                      lat: parseFloat(
                        this.property.prop_area_shape_cords[x][0]
                      ),
                      lng: parseFloat(this.property.prop_area_shape_cords[x][1])
                    };
                    this.property.prop_area_shape_cord.push(cords);
                    this.property.prop_area_shape_cords[
                      x
                    ].lat = this.property.prop_area_shape_cords[x][0];
                    this.property.prop_area_shape_cords[
                      x
                    ].lng = this.property.prop_area_shape_cords[x][1];
                  }
                  var poly = new google.maps.Polygon({
                    paths: this.property.prop_area_shape_cord,
                    strokeColor: this.property.prop_area_shape_fillcolor,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: this.property.prop_area_shape_fillcolor,
                    fillOpacity: 0.35,
                    editable: true,
                    type: "polygon"
                  });
                  poly.setMap(this.map_canvas);
                  this.all_overlays.push(poly);
                  this.selectedShape = poly;
                  this.setSelection(this.selectedShape);
                  this.updatevents(this.selectedShape);
                }
              }
            }
          }
        },
        errorHandler => {
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (err) {
      this.auth.errtoast(err);
    }
  }
  // Map by id

  colors = ["#1E90FF", "#FF1493", "#32CD32", "#FF8C00", "#4B0082"];
  colorButtons = {};
  selectedColor: any;
  color_palette: any;
  colorPalette: any;
  buildColorPalette() {
    var vm = this;
    $(document).ready(function() {
      for (var i = 0; i < vm.colors.length; ++i) {
        var currColor = vm.colors[i];
        var colorButton = vm.makeColorButton(currColor);
        $("#color-palette").append(colorButton);
        vm.colorButtons[currColor] = colorButton;
      }
      vm.selectColor(vm.colors[0]);
    });
  }
  selectColor(color) {
    this.selectedColor = color;
    for (var i = 0; i < this.colors.length; ++i) {
      var currColor = this.colors[i];
      this.colorButtons[currColor].style.border =
        currColor == color ? "2px solid #789" : "2px solid #fff";
    }
    var rectangleOptions = this.drawingManager.get("rectangleOptions");
    rectangleOptions.fillColor = color;
    this.drawingManager.set("rectangleOptions", rectangleOptions);

    var polygonOptions = this.drawingManager.get("polygonOptions");
    polygonOptions.fillColor = color;
    this.drawingManager.set("polygonOptions", polygonOptions);
  }
  setSelectedShapeColor(color) {
    if (this.selectedShape) {
      if (this.selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
        this.selectedShape.set("strokeColor", color);
      } else {
        this.selectedShape.set("fillColor", color);
      }
    }
  }
  makeColorButton(color) {
    var el = this;
    var button = document.createElement("span");
    button.className = "color-button";
    button.style.backgroundColor = color;
    google.maps.event.addDomListener(button, "click", function() {
      el.selectColor(color);
      el.setSelectedShapeColor(color);
    });
    return button;
  }

  selectedShape: any;
  selectedcordShape: any;
  all_overlays: any = [];
  clearSelection = (shape): void => {
    if (this.selectedShape) {
      this.selectedShape.setEditable(false);
      this.selectedShape = null;
    }
  };

  setSelection(shape) {
    this.clearSelection(shape);
    this.selectedShape = shape;
    this.selectedShape.setEditable(true);
    this.selectColor(shape.get("fillColor") || shape.get("strokeColor"));
    if (this.selectedShape.type == "polygon") {
      this.property.prop_total_area = google.maps.geometry.spherical.computeArea(
        this.selectedShape.getPath()
      );
      this.selectedcordShape = this.selectedShape;
      this.getlatlng();
      this.property.prop_total_area = this.property.prop_total_area / 4046.856;
    } else {
      var sw = shape.getBounds().getSouthWest();
      var ne = shape.getBounds().getNorthEast();
      var southWest = new google.maps.LatLng(sw.lat(), sw.lng());
      var northEast = new google.maps.LatLng(ne.lat(), ne.lng());
      var southEast = new google.maps.LatLng(sw.lat(), ne.lng());
      var northWest = new google.maps.LatLng(ne.lat(), sw.lng());
      this.property.prop_total_area = google.maps.geometry.spherical.computeArea(
        [northEast, northWest, southWest, southEast]
      );
      this.selectedcordShape = this.selectedShape;
      this.getlatlng();
      this.property.prop_total_area = this.property.prop_total_area / 4046.856;
    }
  }

  deleteSelectedShape = (): void => {
    if (this.selectedShape) {
      this.drawingManager.setOptions({
        drawingControl: true
      });
      this.selectedShape.setMap(null);
      this.all_overlays = [];
      this.shapecords = null;
      this.property.prop_total_area = "";
      this.drawingManager.setOptions({
        drawingControl: true
      });
      this.drawingManager.setDrawingMode(
        google.maps.drawing.OverlayType.POLYGON
      );
    }
  };

  deleteAllShapes() {
    for (var i = 0; i < this.all_overlays.length; i++) {
      this.all_overlays[i].overlay.setMap(null);
    }
    this.all_overlays = [];
  }
  area: any;
  initializeMap = (): void => {
    this.platform.ready().then(() => {
      let mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(
          this.property.prop_area_center_lat,
          this.property.prop_area_center_lng
        ),
        tilt: 0,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
        },
        mapTypeId: "hybrid",
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_CENTER
        },
        mapTypeControl: true,
        fullscreenControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP
        }
      };

      this.map_canvas = new google.maps.Map(
        this.mapElements.nativeElement,
        mapOptions
      );

      var polyOptions = {
        strokeWeight: 0,
        fillOpacity: 0.45,
        editable: true,
        draggable: true,
        clickable: true
      };

      this.drawingManager = new google.maps.drawing.DrawingManager({
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.LEFT_BOTTOM,
          drawingModes: [
            google.maps.drawing.OverlayType.POLYGON,
            google.maps.drawing.OverlayType.RECTANGLE
          ]
        },
        polylineOptions: {
          editable: true,
          draggable: true,
          clickable: true
        },
        polygonOptions: polyOptions,
        rectangleOptions: polyOptions,
        map: this.map_canvas
      });

      google.maps.event.addListener(
        this.drawingManager,
        "overlaycomplete",
        (e: any) => {
          this.all_overlays.push(e.overlay);
          if (e.type != google.maps.drawing.OverlayType.MARKER) {
            // Switch back to non-drawing mode after drawing a shape.
            this.drawingManager.setDrawingMode(null);
            this.drawingManager.setOptions({
              drawingControl: false
            });
            // Add an event listener that selects the newly-drawn shape when the user
            // mouses down on it.
            var newShape = e.overlay;
            newShape.type = e.type;
            // ShapeChords
            var polygonArray: any = [];
            if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
              document.getElementById("info").innerHTML = "";
              document.getElementById("info").innerHTML += "[";
              for (var i = 0; i < e.overlay.getPath().getLength(); i++) {
                if (i == e.overlay.getPath().getLength() - 1) {
                  document.getElementById("info").innerHTML += JSON.stringify(
                    e.overlay
                      .getPath()
                      .getAt(i)
                      .toUrlValue(6)
                  );
                } else {
                  document.getElementById("info").innerHTML +=
                    JSON.stringify(
                      e.overlay
                        .getPath()
                        .getAt(i)
                        .toUrlValue(6)
                    ) + ",";
                }
              }
              document.getElementById("info").innerHTML += "]";
              polygonArray.push(e.overlay);
            } else if (
              newShape.type === google.maps.drawing.OverlayType.RECTANGLE
            ) {
              var bounds = e.overlay.getBounds();
              var aNorth = bounds.getNorthEast().lat();
              var aEast = bounds.getNorthEast().lng();
              var aSouth = bounds.getSouthWest().lat();
              var aWest = bounds.getSouthWest().lng();
              this.shapecords = {
                south: aSouth,
                west: aWest,
                north: aNorth,
                east: aEast
              };
              document.getElementById("output").innerHTML = bounds.toString();
            }

            google.maps.event.addListener(newShape, "click", e => {
              // console.log(newShape.getPath().getAt(e));
              this.setSelection(newShape);
              // console.log(newShape.latLngs());
              // ShapeChords
              var polygonArray: any = [];
              if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
                document.getElementById("info").innerHTML = "";
                document.getElementById("info").innerHTML += "[";
                for (var i = 0; i < newShape.getPath().getLength(); i++) {
                  if (i == newShape.getPath().getLength() - 1) {
                    document.getElementById("info").innerHTML += JSON.stringify(
                      newShape
                        .getPath()
                        .getAt(i)
                        .toUrlValue(6)
                    );
                  } else {
                    document.getElementById("info").innerHTML +=
                      JSON.stringify(
                        newShape
                          .getPath()
                          .getAt(i)
                          .toUrlValue(6)
                      ) + ",";
                  }
                }
                document.getElementById("info").innerHTML += "]";
                polygonArray.push(newShape);
              } else if (
                newShape.type === google.maps.drawing.OverlayType.RECTANGLE
              ) {
                var bounds = newShape.getBounds();
                var aNorth = bounds.getNorthEast().lat();
                var aEast = bounds.getNorthEast().lng();
                var aSouth = bounds.getSouthWest().lat();
                var aWest = bounds.getSouthWest().lng();
                this.shapecords = {
                  south: aSouth,
                  west: aWest,
                  north: aNorth,
                  east: aEast
                };
                document.getElementById("output").innerHTML = bounds.toString();
              }
            });
            google.maps.event.addListener(newShape, "mouseup", () => {
              this.setSelection(newShape);
              // ShapeChords
              var polygonArray: any = [];
              if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
                document.getElementById("info").innerHTML = "";
                document.getElementById("info").innerHTML += "[";
                for (var i = 0; i < newShape.getPath().getLength(); i++) {
                  if (i == newShape.getPath().getLength() - 1) {
                    document.getElementById("info").innerHTML += JSON.stringify(
                      newShape
                        .getPath()
                        .getAt(i)
                        .toUrlValue(6)
                    );
                  } else {
                    document.getElementById("info").innerHTML +=
                      JSON.stringify(
                        newShape
                          .getPath()
                          .getAt(i)
                          .toUrlValue(6)
                      ) + ",";
                  }
                }
                document.getElementById("info").innerHTML += "]";
                polygonArray.push(newShape);
              } else if (
                newShape.type === google.maps.drawing.OverlayType.RECTANGLE
              ) {
                var bounds = e.overlay.getBounds();
                var aNorth = bounds.getNorthEast().lat();
                var aEast = bounds.getNorthEast().lng();
                var aSouth = bounds.getSouthWest().lat();
                var aWest = bounds.getSouthWest().lng();
                this.shapecords = {
                  south: aSouth,
                  west: aWest,
                  north: aNorth,
                  east: aEast
                };
                document.getElementById("output").innerHTML = bounds.toString();
              }
            });
            google.maps.event.addListener(newShape, "dragend", () => {
              this.setSelection(newShape);
              // ShapeChords
              var polygonArray: any = [];
              if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
                document.getElementById("info").innerHTML = "";
                document.getElementById("info").innerHTML += "[";
                for (var i = 0; i < newShape.getPath().getLength(); i++) {
                  if (i == newShape.getPath().getLength() - 1) {
                    document.getElementById("info").innerHTML += JSON.stringify(
                      newShape
                        .getPath()
                        .getAt(i)
                        .toUrlValue(6)
                    );
                  } else {
                    document.getElementById("info").innerHTML +=
                      JSON.stringify(
                        newShape
                          .getPath()
                          .getAt(i)
                          .toUrlValue(6)
                      ) + ",";
                  }
                }
                document.getElementById("info").innerHTML += "]";
                polygonArray.push(newShape);
              } else if (
                newShape.type === google.maps.drawing.OverlayType.RECTANGLE
              ) {
                var bounds = e.overlay.getBounds();
                var aNorth = bounds.getNorthEast().lat();
                var aEast = bounds.getNorthEast().lng();
                var aSouth = bounds.getSouthWest().lat();
                var aWest = bounds.getSouthWest().lng();
                this.shapecords = {
                  south: aSouth,
                  west: aWest,
                  north: aNorth,
                  east: aEast
                };
                document.getElementById("output").innerHTML = bounds.toString();
              }
            });
            google.maps.event.addListener(newShape, "resize", () => {
              this.setSelection(newShape);
              // ShapeChords
              var polygonArray: any = [];
              if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
                document.getElementById("info").innerHTML = "";
                document.getElementById("info").innerHTML += "[";
                for (var i = 0; i < newShape.getPath().getLength(); i++) {
                  if (i == newShape.getPath().getLength() - 1) {
                    document.getElementById("info").innerHTML += JSON.stringify(
                      newShape
                        .getPath()
                        .getAt(i)
                        .toUrlValue(6)
                    );
                  } else {
                    document.getElementById("info").innerHTML +=
                      JSON.stringify(
                        newShape
                          .getPath()
                          .getAt(i)
                          .toUrlValue(6)
                      ) + ",";
                  }
                }
                document.getElementById("info").innerHTML += "]";
                polygonArray.push(newShape);
              } else if (
                newShape.type === google.maps.drawing.OverlayType.RECTANGLE
              ) {
                var bounds = e.overlay.getBounds();
                var aNorth = bounds.getNorthEast().lat();
                var aEast = bounds.getNorthEast().lng();
                var aSouth = bounds.getSouthWest().lat();
                var aWest = bounds.getSouthWest().lng();
                this.shapecords = {
                  south: aSouth,
                  west: aWest,
                  north: aNorth,
                  east: aEast
                };
                document.getElementById("output").innerHTML = bounds.toString();
              }
            });
            google.maps.event.addListener(newShape, "bounds_changed", () => {
              this.setSelection(newShape);
              // ShapeChords
              var polygonArray: any = [];
              if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
                document.getElementById("info").innerHTML = "";
                document.getElementById("info").innerHTML += "[";
                for (var i = 0; i < newShape.getPath().getLength(); i++) {
                  if (i == newShape.getPath().getLength() - 1) {
                    document.getElementById("info").innerHTML += JSON.stringify(
                      newShape
                        .getPath()
                        .getAt(i)
                        .toUrlValue(6)
                    );
                  } else {
                    document.getElementById("info").innerHTML +=
                      JSON.stringify(
                        newShape
                          .getPath()
                          .getAt(i)
                          .toUrlValue(6)
                      ) + ",";
                  }
                }
                document.getElementById("info").innerHTML += "]";
                polygonArray.push(newShape);
              } else if (
                newShape.type === google.maps.drawing.OverlayType.RECTANGLE
              ) {
                var bounds = e.overlay.getBounds();
                var aNorth = bounds.getNorthEast().lat();
                var aEast = bounds.getNorthEast().lng();
                var aSouth = bounds.getSouthWest().lat();
                var aWest = bounds.getSouthWest().lng();
                this.shapecords = {
                  south: aSouth,
                  west: aWest,
                  north: aNorth,
                  east: aEast
                };
                document.getElementById("output").innerHTML = bounds.toString();
              }
            });
            google.maps.event.addListener(newShape, "dblclick", () => {
              this.setSelection(newShape);
              // ShapeChords
              var polygonArray: any = [];
              if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
                document.getElementById("info").innerHTML = "";
                document.getElementById("info").innerHTML += "[";
                for (var i = 0; i < newShape.getPath().getLength(); i++) {
                  if (i == newShape.getPath().getLength() - 1) {
                    document.getElementById("info").innerHTML += JSON.stringify(
                      newShape
                        .getPath()
                        .getAt(i)
                        .toUrlValue(6)
                    );
                  } else {
                    document.getElementById("info").innerHTML +=
                      JSON.stringify(
                        newShape
                          .getPath()
                          .getAt(i)
                          .toUrlValue(6)
                      ) + ",";
                  }
                }
                document.getElementById("info").innerHTML += "]";
                polygonArray.push(newShape);
              } else if (
                newShape.type === google.maps.drawing.OverlayType.RECTANGLE
              ) {
                var bounds = e.overlay.getBounds();
                var aNorth = bounds.getNorthEast().lat();
                var aEast = bounds.getNorthEast().lng();
                var aSouth = bounds.getSouthWest().lat();
                var aWest = bounds.getSouthWest().lng();
                this.shapecords = {
                  south: aSouth,
                  west: aWest,
                  north: aNorth,
                  east: aEast
                };
                document.getElementById("output").innerHTML = bounds.toString();
              }
            });
            google.maps.event.addListener(newShape, "maptypeid_changed", () => {
              this.setSelection(newShape);
              // ShapeChords
              var polygonArray: any = [];
              if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
                document.getElementById("info").innerHTML = "";
                document.getElementById("info").innerHTML += "[";
                for (var i = 0; i < newShape.getPath().getLength(); i++) {
                  if (i == newShape.getPath().getLength() - 1) {
                    document.getElementById("info").innerHTML += JSON.stringify(
                      newShape
                        .getPath()
                        .getAt(i)
                        .toUrlValue(6)
                    );
                  } else {
                    document.getElementById("info").innerHTML +=
                      JSON.stringify(
                        newShape
                          .getPath()
                          .getAt(i)
                          .toUrlValue(6)
                      ) + ",";
                  }
                }
                document.getElementById("info").innerHTML += "]";
                polygonArray.push(newShape);
              } else if (
                newShape.type === google.maps.drawing.OverlayType.RECTANGLE
              ) {
                var bounds = e.overlay.getBounds();
                var aNorth = bounds.getNorthEast().lat();
                var aEast = bounds.getNorthEast().lng();
                var aSouth = bounds.getSouthWest().lat();
                var aWest = bounds.getSouthWest().lng();
                this.shapecords = {
                  south: aSouth,
                  west: aWest,
                  north: aNorth,
                  east: aEast
                };
                document.getElementById("output").innerHTML = bounds.toString();
              }
            });

            this.setSelection(newShape);
          }
        }
      );
      google.maps.event.addListener(this.map_canvas, "click", () => {
        this.clearSelection(this.selectedShape);
      });
    });
    this.buildColorPalette();
  };

  getlatlng() {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.nativeGeocoder
      .reverseGeocode(
        this.map_canvas.getCenter().lat(),
        this.map_canvas.getCenter().lng(),
        options
      )
      .then((result: NativeGeocoderReverseResult[]) => {
        if (result[0]) {
          this.mapaddress =
            result[0].subAdministrativeArea +
            ", " +
            result[0].administrativeArea +
            ", " +
            result[0].countryName +
            ", " +
            result[0].postalCode;
        }
        // this.mapaddress = this.mapaddress.replace(/,/, "");
        this.mapaddress = this.mapaddress.replace(/^,|,$/g, "");
      })
      .catch((error: any) => {
        this.auth.errtoast(error);
      });
  }
  shapecords: any;
  mapaddress: any = "";
  goto(page) {
    if (this.selectedcordShape.type == "rectangle") {
      if (typeof this.shapecords == "string") {
      } else {
        this.shapecords = JSON.stringify(this.shapecords);
        this.shapecords = "[" + this.shapecords + "]";
      }
    } else if (this.selectedcordShape.type == "polygon") {
      if (typeof this.shapecords == "string") {
      } else {
        this.shapecords = document.getElementById("info").innerHTML;
      }
    }
    this.property.area = this.property.prop_total_area;
    this.property.shapetype = this.selectedShape.type;
    this.property.shapecords = this.shapecords;
    this.property.selectedColor = this.selectedColor;
    this.property.mapaddress = this.mapaddress;
    this.property.arealat = this.map_canvas.getCenter().lat();
    this.property.arealng = this.map_canvas.getCenter().lng();
    this.property.zoom = this.map_canvas.getZoom();
    this.auth.startloader();
    try {
      this.auth
        .createprop2(
          this.auth.getuserId(),
          this.property.property_id,
          this.property.prop_total_area,
          this.selectedShape.type,
          this.shapecords,
          this.selectedColor,
          this.mapaddress,
          this.map_canvas.getCenter().lat(),
          this.map_canvas.getCenter().lng(),
          this.map_canvas.getZoom()
        )
        .subscribe(data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              if (data.json().message) {
                this.auth.toast(data.json().message);
              } else {
                this.auth.toast("Data Saved");
              }
              this.property.prop_area_center_lat = this.map_canvas
                .getCenter()
                .lat();
              this.property.prop_area_center_lng = this.map_canvas
                .getCenter()
                .lng();
              this.navCtrl.push(page, {
                property_data: this.property
              });
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            }
          }
        });
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
}
