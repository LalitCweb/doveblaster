import {
  Component,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate,
  group,
  ViewChild
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  AlertController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { SocialSharing } from "@ionic-native/social-sharing";
import * as $ from "jquery";
import { DatePipe } from "@angular/common";
import { IMyDpOptions } from "mydatepicker";
/**
 * Generated class for the PropertiesdetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var $: any;
declare var google: any;
declare var Keyboard: any;
@IonicPage()
@Component({
  selector: "page-propertiesdetail",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "propertiesdetail.html"
})
export class PropertiesdetailPage implements OnInit {
  myDatePickerOptions: IMyDpOptions = {
    // other options...
    // editableDateField: true,
    dateFormat: "mm-dd-yyyy",
    disableDateRanges: [
      {
        begin: { year: 1900, month: 1, day: 1 },
        end: { year: 2999, month: 12, day: 31 }
      }
    ], // disable all
    enableDays: []
  };

  // Initialized to specific date (09.10.2018).

  model: any = {
    date: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate()
    }
  };
  map: any;
  marker = "assets/images/stillmarker.gif";
  myDate: any = new Date().toISOString();
  property: any = {};
  mapview: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    private socialSharing: SocialSharing,
    public datepipe: DatePipe,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController
  ) {
    if (
      this.navParams.get("property_detail") &&
      this.navParams.get("property_data")
    ) {
      this.mapview = true;
      this.property = this.navParams.get("property_data");

      var availableDates = ["01-24-2019", "01-25-2019"];
      if (this.property.available_dates_first_format1) {
        this.property.available_dates_first_format1 = null;
        this.property.available_dates_first_format2 = null;
        $(document).ready(function() {
          var d = new Date();
          var currDay = d.getDate();
          var currMonth = d.getMonth();
          var currYear = d.getFullYear();
          var startDate = new Date(currYear, currMonth, currDay);
          $("#datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            beforeShowDay: function(d) {
              var dmy = d.getMonth() + 1;
              if (d.getMonth() < 9) dmy = "0" + dmy;
              dmy += "-";

              if (d.getDate() < 10) dmy += "0";
              dmy += d.getDate() + "-" + d.getFullYear();

              if ($.inArray(dmy, availableDates) != -1) {
                return [true, "", "Available"];
              } else {
                return [false, "", "unAvailable"];
              }
            }
          });
          $("#datepicker").datepicker("setDate", startDate);
        });
      } else {
        $(document).ready(function() {
          var d = new Date();
          var currDay = d.getDate();
          var currMonth = d.getMonth();
          var currYear = d.getFullYear();
          var startDate = new Date(currYear, currMonth, currDay);
          $("#datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            beforeShowDay: function(d) {
              var dmy = d.getMonth() + 1;
              if (d.getMonth() < 9) dmy = "0" + dmy;
              dmy += "-";

              if (d.getDate() < 10) dmy += "0";
              dmy += d.getDate() + "-" + d.getFullYear();

              if ($.inArray(dmy, availableDates) != -1) {
                return [true, "", "Available"];
              } else {
                return [false, "", "unAvailable"];
              }
            }
          });
          $("#datepicker").datepicker("setDate", startDate);
        });
      }

      this.options.lat = parseFloat(this.property.prop_area_center_lat);
      this.options.lng = parseFloat(this.property.prop_area_center_lng);
      if (this.property.prop_area_currnt_zoom != undefined) {
        this.property.prop_area_currnt_zoom =
          parseInt(this.property.prop_area_currnt_zoom) - 2;
      } else {
        this.property.prop_area_currnt_zoom = 18;
      }
      this.propertydetail(this.property.ID, this.myDate);
      // if (this.property.prop_area_shape_type == "rectangle") {
      //   if (this.property.prop_area_shape_cords) {
      //     try {
      //       this.property.prop_area_shape_cords = JSON.parse(
      //         this.property.prop_area_shape_cords
      //       );
      //     } catch (e) {
      //       return;
      //     }
      //   }
      // }
      if (this.property.prop_area_shape_type == "polygon") {
        if (this.property.prop_area_shape_cords) {
          try {
            this.property.prop_area_shape_cords = JSON.parse(
              this.property.prop_area_shape_cords
            );
          } catch (e) {
            return;
          }
        }
        this.property.prop_area_shape_cord = [];
        var cords: any = {};
        if (
          this.property.prop_area_shape_cords &&
          this.property.prop_area_shape_cords.length
        ) {
          for (var x = 0; x < this.property.prop_area_shape_cords.length; x++) {
            try {
              this.property.prop_area_shape_cords[
                x
              ] = this.property.prop_area_shape_cords[x].split(",");
            } catch (e) {
              // return;
            }
            cords = {
              lat: parseFloat(this.property.prop_area_shape_cords[x][0]),
              lng: parseFloat(this.property.prop_area_shape_cords[x][1])
            };
            this.property.prop_area_shape_cord.push(cords);
            this.property.prop_area_shape_cords[
              x
            ].lat = this.property.prop_area_shape_cords[x][0];
            this.property.prop_area_shape_cords[
              x
            ].lng = this.property.prop_area_shape_cords[x][1];
          }
        }
      }
    } else if (
      !this.navParams.get("property_detail") &&
      this.navParams.get("property_data")
    ) {
      this.mapview = false;
      this.property = this.navParams.get("property_data");
      var availableDates = ["01-24-2019", "01-25-2019"];
      if (this.property.available_dates_first_format1) {
        this.property.available_dates_first_format1 = null;
        this.property.available_dates_first_format2 = null;
        $(document).ready(function() {
          var d = new Date();
          var currDay = d.getDate();
          var currMonth = d.getMonth();
          var currYear = d.getFullYear();
          var startDate = new Date(currYear, currMonth, currDay);
          $("#datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            beforeShowDay: function(d) {
              var dmy = d.getMonth() + 1;
              if (d.getMonth() < 9) dmy = "0" + dmy;
              dmy += "-";

              if (d.getDate() < 10) dmy += "0";
              dmy += d.getDate() + "-" + d.getFullYear();

              if ($.inArray(dmy, availableDates) != -1) {
                return [true, "", "Available"];
              } else {
                return [false, "", "unAvailable"];
              }
            }
          });
          $("#datepicker").datepicker("setDate", startDate);
        });
      } else {
        this.property.available_dates_first_format1 = null;
        this.property.available_dates_first_format2 = null;
        $(document).ready(function() {
          var d = new Date();
          var currDay = d.getDate();
          var currMonth = d.getMonth();
          var currYear = d.getFullYear();
          var startDate = new Date(currYear, currMonth, currDay);
          $("#datepicker").datepicker({
            dateFormat: "dd-mm-yy",
            beforeShowDay: function(d) {
              var dmy = d.getMonth() + 1;
              if (d.getMonth() < 9) dmy = "0" + dmy;
              dmy += "-";

              if (d.getDate() < 10) dmy += "0";
              dmy += d.getDate() + "-" + d.getFullYear();

              if ($.inArray(dmy, availableDates) != -1) {
                return [true, "", "Available"];
              } else {
                return [false, "", "unAvailable"];
              }
            }
          });
          $("#datepicker").datepicker("setDate", startDate);
        });
      }

      this.options.lat = parseFloat(this.property.prop_area_center_lat);
      this.options.lng = parseFloat(this.property.prop_area_center_lng);
      if (this.property.prop_area_currnt_zoom != undefined) {
        this.property.prop_area_currnt_zoom =
          parseInt(this.property.prop_area_currnt_zoom) - 2;
      } else {
        this.property.prop_area_currnt_zoom = 18;
      }
      this.propertydetail(this.property.ID, this.myDate);
      if (this.property.prop_area_shape_type == "rectangle") {
        if (this.property.prop_area_shape_cords) {
          // console.log(typeof this.property.prop_area_shape_cords);
          // if (typeof this.property.prop_area_shape_cords == "string") {
          //   this.property.prop_area_shape_cords = JSON.parse(
          //     this.property.prop_area_shape_cords
          //   );
          // } else {
          // }
          // var rect = new google.maps.Rectangle({
          //   strokeColor: this.property.prop_area_shape_fillcolor,
          //   strokeOpacity: 0.8,
          //   strokeWeight: 2,
          //   fillColor: this.property.prop_area_shape_fillcolor,
          //   fillOpacity: 0.35,
          //   map: this.map,
          //   bounds: {
          //     north: this.property.prop_area_shape_cords[0].north,
          //     south: this.property.prop_area_shape_cords[0].south,
          //     east: this.property.prop_area_shape_cords[0].east,
          //     west: this.property.prop_area_shape_cords[0].west
          //   }
          // });
          // rect.setMap(this.map);
        }
      }
      if (this.property.prop_area_shape_type == "polygon") {
        try {
          this.property.prop_area_shape_cords = JSON.parse(
            this.property.prop_area_shape_cords
          );
        } catch (e) {
          return;
        }
        this.property.prop_area_shape_cord = [];
        var cords: any = {};
        if (
          this.property.prop_area_shape_cords &&
          this.property.prop_area_shape_cords.length
        ) {
          for (var x = 0; x < this.property.prop_area_shape_cords.length; x++) {
            try {
              this.property.prop_area_shape_cords[
                x
              ] = this.property.prop_area_shape_cords[x].split(",");
            } catch (e) {
              // return;
            }
            cords = {
              lat: parseFloat(this.property.prop_area_shape_cords[x][0]),
              lng: parseFloat(this.property.prop_area_shape_cords[x][1])
            };
            this.property.prop_area_shape_cord.push(cords);
            this.property.prop_area_shape_cords[
              x
            ].lat = this.property.prop_area_shape_cords[x][0];
            this.property.prop_area_shape_cords[
              x
            ].lng = this.property.prop_area_shape_cords[x][1];
          }
          // var poly = new google.maps.Polygon({
          //   paths: this.property.prop_area_shape_cord,
          //   strokeColor: this.property.prop_area_shape_fillcolor,
          //   strokeOpacity: 0.8,
          //   strokeWeight: 2,
          //   fillColor: this.property.prop_area_shape_fillcolor,
          //   fillOpacity: 0.35,
          //   map: this.map
          // });
          // poly.setMap(this.map);
        }
      }
    }
    setTimeout(() => {
      $(document).ready(function() {
        $("span")
          .find("a")
          .contents()
          .unwrap();
      });
    }, 100);
  }
  ngOnInit() {}
  showloadingonmap: boolean = false;
  propertydetail(property, date) {
    date = this.datepipe.transform(date, "yyyy-MM-dd");
    // debugger;
    this.showloadingonmap = true;
    try {
      this.auth
        .getpropertydetail(this.auth.getuserId(), property, date)
        .subscribe(
          data => {
            this.showloadingonmap = false;
            if (data) {
              if (data.json().status == 1) {
                var dataa: any = data.json().property_extra_details;
                if (typeof dataa.available_dates_second_format == "string") {
                  this.property.available_dates_first_format = JSON.parse(
                    dataa.available_dates_second_format
                  );
                }
                this.property.slot_details = dataa.slot_details;

                if (
                  this.property.slot_details &&
                  this.property.slot_details.length
                ) {
                  for (var x = 0; x < this.property.slot_details.length; x++) {
                    if (
                      this.property.slot_details[x].slot_type == "rectangle"
                    ) {
                      // if (this.property.slot_details[x].slot_coordinates) {
                      //   this.property.slot_details[
                      //     x
                      //   ].slot_coordinates = this.property.slot_details[
                      //     x
                      //   ].slot_coordinates.replace(/\\/g, "");
                      //   this.property.slot_details[
                      //     x
                      //   ].slot_coordinates = JSON.parse(
                      //     this.property.slot_details[x].slot_coordinates
                      //   );
                      // }
                    }
                    if (this.property.slot_details[x].slot_type == "circle") {
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_radious = parseFloat(
                      //   this.property.slot_details[x].slot_circle_radious
                      // );
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_lat = parseFloat(
                      //   this.property.slot_details[x].slot_circle_lat
                      // );
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_lng = parseFloat(
                      //   this.property.slot_details[x].slot_circle_lng
                      // );
                    }
                    if (this.property.slot_details[x].slot_type == "polygon") {
                      if (this.property.slot_details[x].slot_coordinates) {
                        this.property.slot_details[
                          x
                        ].slot_coordinates = this.property.slot_details[
                          x
                        ].slot_coordinates.replace(/\\/g, "");
                        this.property.slot_details[
                          x
                        ].slot_coordinates = JSON.parse(
                          this.property.slot_details[x].slot_coordinates
                        );
                        this.property.slot_details[x].slot_coordinate = [];
                        var cords: any = {};
                        if (
                          this.property.slot_details[x].slot_coordinates &&
                          this.property.slot_details[x].slot_coordinates.length
                        ) {
                          for (
                            var y = 0;
                            y <
                            this.property.slot_details[x].slot_coordinates
                              .length;
                            y++
                          ) {
                            if (
                              this.property.slot_details[x].slot_coordinates[y]
                            ) {
                              this.property.slot_details[x].slot_coordinates[
                                y
                              ] = this.property.slot_details[
                                x
                              ].slot_coordinates[y].split(",");
                              cords = {
                                lat: parseFloat(
                                  this.property.slot_details[x]
                                    .slot_coordinates[y][0]
                                ),
                                lng: parseFloat(
                                  this.property.slot_details[x]
                                    .slot_coordinates[y][1]
                                )
                              };
                              this.property.slot_details[
                                x
                              ].slot_coordinate.push(cords);
                              this.property.slot_details[x].slot_coordinates[
                                y
                              ].lat = this.property.slot_details[
                                x
                              ].slot_coordinates[y][0];
                              this.property.slot_details[x].slot_coordinates[
                                y
                              ].lng = this.property.slot_details[
                                x
                              ].slot_coordinates[y][1];
                            }
                          }
                        }
                      }
                    }
                  }
                }
                setTimeout(() => {
                  this.resetmap();
                }, 1000);

                /********************/
                /*****Date Picker****/
                /********************/
                var el = this;
                if (this.property.available_dates_first_format1) {
                  this.myDatePickerOptions.enableDays = this.property.available_dates_first_format1;
                } else {
                  this.property.available_dates_first_format1 = [];
                  this.property.available_dates_first_format2 = [];
                  for (
                    var dt = 0;
                    dt < this.property.available_dates_first_format.length;
                    dt++
                  ) {
                    el.property.available_dates_first_format2[
                      dt
                    ] = el.property.available_dates_first_format[dt].split("-");
                    el.property.available_dates_first_format[dt] =
                      el.property.available_dates_first_format2[dt][1] +
                      "-" +
                      el.property.available_dates_first_format2[dt][0] +
                      "-" +
                      el.property.available_dates_first_format2[dt][2];
                    var ar: any = {
                      year: "",
                      month: "",
                      day: ""
                    };
                    el.property.available_dates_first_format[
                      dt
                    ] = el.property.available_dates_first_format[dt].replace(
                      /-/g,
                      "/"
                    );
                    ar.year = new Date(
                      el.property.available_dates_first_format[dt]
                    ).getFullYear();
                    ar.day = new Date(
                      el.property.available_dates_first_format[dt]
                    ).getDate();
                    ar.month =
                      new Date(
                        el.property.available_dates_first_format[dt]
                      ).getMonth() + 1;
                    el.property.available_dates_first_format1.push(ar);
                  }
                  this.myDatePickerOptions.enableDays = this.property.available_dates_first_format1;
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.showloadingonmap = false;
            this.auth.errtoast(errorHandler);
          }
        );
    } catch (err) {
      this.showloadingonmap = false;
      this.auth.errtoast(err);
    }
  }
  resetmap() {
    // this.map = new google.maps.Map(document.getElementById("map"), {
    //   zoom: this.property.prop_area_currnt_zoom,
    //   center: new google.maps.LatLng(this.options.lat, this.options.lng),
    //   tilt: 0,
    //   disableDefaultUI: true,
    //   zoomControl: true,
    //   zoomControlOptions: {
    //     position: google.maps.ControlPosition.RIGHT_CENTER
    //   },
    //   mapTypeId: "hybrid",
    //   mapTypeControlOptions: {
    //     style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
    //     position: google.maps.ControlPosition.TOP_CENTER
    //   },
    //   mapTypeControl: true,
    //   fullscreenControl: false,
    //   streetViewControl: true,
    //   streetViewControlOptions: {
    //     position: google.maps.ControlPosition.LEFT_TOP
    //   }
    // });
    // if (this.property.prop_area_shape_type == "rectangle") {
    //   var rect = new google.maps.Rectangle({
    //     strokeColor: "yellow",
    //     strokeOpacity: 0.8,
    //     strokeWeight: 2,
    //     fillColor: "yellow",
    //     fillOpacity: 0,
    //     map: this.map,
    //     bounds: {
    //       north: this.property.prop_area_shape_cords[0].north,
    //       south: this.property.prop_area_shape_cords[0].south,
    //       east: this.property.prop_area_shape_cords[0].east,
    //       west: this.property.prop_area_shape_cords[0].west
    //     }
    //   });
    //   rect.setMap(this.map);
    // } else if (this.property.prop_area_shape_type == "polygon") {
    //   var poly = new google.maps.Polygon({
    //     paths: this.property.prop_area_shape_cord,
    //     strokeColor: "yellow",
    //     strokeOpacity: 0.8,
    //     strokeWeight: 2,
    //     fillColor: "yellow",
    //     fillOpacity: 0,
    //     map: this.map
    //   });
    //   poly.setMap(this.map);
    // }
    // if (this.property.slot_details && this.property.slot_details.length) {
    //   for (
    //     var propert = 0;
    //     propert < this.property.slot_details.length;
    //     propert++
    //   ) {
    //     if (this.property.slot_details[propert].slot_type == "circle") {
    //       var circle = new google.maps.Circle({
    //         strokeColor: this.property.slot_details[propert].slot_color,
    //         strokeOpacity: 0.8,
    //         strokeWeight: 2,
    //         fillColor: this.property.slot_details[propert].slot_color,
    //         fillOpacity: 0.4,
    //         map: this.map,
    //         center: {
    //           lat: this.property.slot_details[propert].slot_circle_lat,
    //           lng: this.property.slot_details[propert].slot_circle_lng
    //         },
    //         radius: this.property.slot_details[propert].slot_circle_radious,
    //         indexID: this.property.slot_details[propert]
    //       });
    //       circle.setMap(this.map);
    //       this.addListenersOnpoly(circle, propert);
    //     } else if (
    //       this.property.slot_details[propert].slot_type == "rectangle"
    //     ) {
    //       var rect = new google.maps.Rectangle({
    //         strokeColor: this.property.slot_details[propert].slot_color,
    //         strokeOpacity: 0.8,
    //         strokeWeight: 2,
    //         fillColor: this.property.slot_details[propert].slot_color,
    //         fillOpacity: 0.4,
    //         map: this.map,
    //         bounds: {
    //           north: this.property.slot_details[propert].slot_coordinates[0]
    //             .north,
    //           south: this.property.slot_details[propert].slot_coordinates[0]
    //             .south,
    //           east: this.property.slot_details[propert].slot_coordinates[0]
    //             .east,
    //           west: this.property.slot_details[propert].slot_coordinates[0].west
    //         },
    //         indexID: this.property.slot_details[propert]
    //       });
    //       rect.setMap(this.map);
    //       this.addListenersOnpoly(rect, propert);
    //     } else if (this.property.slot_details[propert].slot_type == "polygon") {
    //       var poly = new google.maps.Polygon({
    //         map: this.map,
    //         paths: this.property.slot_details[propert].slot_coordinate,
    //         strokeColor: this.property.slot_details[propert].slot_color,
    //         strokeOpacity: 0.8,
    //         strokeWeight: 2,
    //         fillColor: this.property.slot_details[propert].slot_color,
    //         fillOpacity: 0.4,
    //         indexID: this.property.slot_details[propert]
    //       });
    //       poly.setMap(this.map);
    //       this.addListenersOnpoly(poly, propert);
    //     }
    //   }
    // }
  }
  ionViewWillLeave() {
    // if (this.property.prop_area_shape_cords) {
    //   this.property.prop_area_shape_cords = JSON.stringify(
    //     this.property.prop_area_shape_cords
    //   );
    // }
    this.property.prop_area_currnt_zoom = 18;
    this.show = false;
    google.maps.event.trigger(this.map, "resize");
  }
  ionViewWillEnter() {
    google.maps.event.trigger(this.map, "resize");
    this.getcart();
  }

  getuserdetail(property_id) {
    this.auth.startloader();
    try {
      this.auth
        .getsinglealldetail(property_id, this.auth.getuserId())
        .subscribe(
          data => {
            if (data.json()) {
              if (data.json().status == 1) {
                this.property = data.json().events;
                var dataa: any = data.json().property_extra_details;
                if (typeof dataa.available_dates_second_format == "string") {
                  this.property.available_dates_first_format = JSON.parse(
                    dataa.available_dates_second_format
                  );
                }
                this.property.slot_details = dataa.slot_details;

                if (
                  this.property.slot_details &&
                  this.property.slot_details.length
                ) {
                  for (var x = 0; x < this.property.slot_details.length; x++) {
                    if (
                      this.property.slot_details[x].slot_type == "rectangle"
                    ) {
                      // if (this.property.slot_details[x].slot_coordinates) {
                      //   this.property.slot_details[
                      //     x
                      //   ].slot_coordinates = this.property.slot_details[
                      //     x
                      //   ].slot_coordinates.replace(/\\/g, "");
                      //   this.property.slot_details[
                      //     x
                      //   ].slot_coordinates = JSON.parse(
                      //     this.property.slot_details[x].slot_coordinates
                      //   );
                      // }
                    }
                    if (this.property.slot_details[x].slot_type == "circle") {
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_radious = parseFloat(
                      //   this.property.slot_details[x].slot_circle_radious
                      // );
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_lat = parseFloat(
                      //   this.property.slot_details[x].slot_circle_lat
                      // );
                      // this.property.slot_details[
                      //   x
                      // ].slot_circle_lng = parseFloat(
                      //   this.property.slot_details[x].slot_circle_lng
                      // );
                    }
                    if (this.property.slot_details[x].slot_type == "polygon") {
                      if (this.property.slot_details[x].slot_coordinates) {
                        this.property.slot_details[
                          x
                        ].slot_coordinates = this.property.slot_details[
                          x
                        ].slot_coordinates.replace(/\\/g, "");
                        this.property.slot_details[
                          x
                        ].slot_coordinates = JSON.parse(
                          this.property.slot_details[x].slot_coordinates
                        );

                        this.property.slot_details[x].slot_coordinate = [];
                        var cords: any = {};
                        if (
                          this.property.slot_details[x].slot_coordinates &&
                          this.property.slot_details[x].slot_coordinates.length
                        ) {
                          for (
                            var y = 0;
                            y <
                            this.property.slot_details[x].slot_coordinates
                              .length;
                            y++
                          ) {
                            if (
                              this.property.slot_details[x].slot_coordinates[y]
                            ) {
                              this.property.slot_details[x].slot_coordinates[
                                y
                              ] = this.property.slot_details[
                                x
                              ].slot_coordinates[y].split(",");
                              cords = {
                                lat: parseFloat(
                                  this.property.slot_details[x]
                                    .slot_coordinates[y][0]
                                ),
                                lng: parseFloat(
                                  this.property.slot_details[x]
                                    .slot_coordinates[y][1]
                                )
                              };
                              this.property.slot_details[
                                x
                              ].slot_coordinate.push(cords);
                              this.property.slot_details[x].slot_coordinates[
                                y
                              ].lat = this.property.slot_details[
                                x
                              ].slot_coordinates[y][0];
                              this.property.slot_details[x].slot_coordinates[
                                y
                              ].lng = this.property.slot_details[
                                x
                              ].slot_coordinates[y][1];
                            }
                          }
                        }
                      }
                    }
                  }
                }

                // this.map = new google.maps.Map(document.getElementById("map"), {
                //   zoom: this.property.prop_area_currnt_zoom,
                //   center: new google.maps.LatLng(
                //     this.options.lat,
                //     this.options.lng
                //   ),
                //   tilt: 0,
                //   disableDefaultUI: true,
                //   zoomControl: true,
                //   zoomControlOptions: {
                //     position: google.maps.ControlPosition.RIGHT_CENTER
                //   },
                //   mapTypeId: "hybrid",
                //   mapTypeControlOptions: {
                //     style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                //     position: google.maps.ControlPosition.TOP_CENTER
                //   },
                //   mapTypeControl: true,
                //   fullscreenControl: true,
                //   streetViewControl: true,
                //   streetViewControlOptions: {
                //     position: google.maps.ControlPosition.LEFT_TOP
                //   }
                // });
                if (this.property.prop_area_shape_type == "rectangle") {
                  // var rect = new google.maps.Rectangle({
                  //   strokeColor: "yellow",
                  //   strokeOpacity: 0.8,
                  //   strokeWeight: 2,
                  //   fillColor: "yellow",
                  //   fillOpacity: 0,
                  //   map: this.map,
                  //   bounds: {
                  //     north: this.property.prop_area_shape_cords[0].north,
                  //     south: this.property.prop_area_shape_cords[0].south,
                  //     east: this.property.prop_area_shape_cords[0].east,
                  //     west: this.property.prop_area_shape_cords[0].west
                  //   }
                  // });
                  // rect.setMap(this.map);
                } else if (this.property.prop_area_shape_type == "polygon") {
                  // var poly = new google.maps.Polygon({
                  //   paths: this.property.prop_area_shape_cord,
                  //   strokeColor: "yellow",
                  //   strokeOpacity: 0.8,
                  //   strokeWeight: 2,
                  //   fillColor: "yellow",
                  //   fillOpacity: 0,
                  //   map: this.map
                  // });
                  // poly.setMap(this.map);
                }
                if (
                  this.property.slot_details &&
                  this.property.slot_details.length
                ) {
                  for (
                    var propert = 0;
                    propert < this.property.slot_details.length;
                    propert++
                  ) {
                    if (
                      this.property.slot_details[propert].slot_type == "circle"
                    ) {
                      // var circle = new google.maps.Circle({
                      //   strokeColor: this.property.slot_details[propert]
                      //     .slot_color,
                      //   strokeOpacity: 0.8,
                      //   strokeWeight: 2,
                      //   fillColor: this.property.slot_details[propert]
                      //     .slot_color,
                      //   fillOpacity: 0.4,
                      //   map: this.map,
                      //   center: {
                      //     lat: this.property.slot_details[propert]
                      //       .slot_circle_lat,
                      //     lng: this.property.slot_details[propert]
                      //       .slot_circle_lng
                      //   },
                      //   radius: this.property.slot_details[propert]
                      //     .slot_circle_radious,
                      //   indexID: this.property.slot_details[propert]
                      // });
                      // circle.setMap(this.map);
                      // this.addListenersOnpoly(circle, propert);
                    } else if (
                      this.property.slot_details[propert].slot_type ==
                      "rectangle"
                    ) {
                      // var rect = new google.maps.Rectangle({
                      //   strokeColor: this.property.slot_details[propert]
                      //     .slot_color,
                      //   strokeOpacity: 0.8,
                      //   strokeWeight: 2,
                      //   fillColor: this.property.slot_details[propert]
                      //     .slot_color,
                      //   fillOpacity: 0.4,
                      //   map: this.map,
                      //   bounds: {
                      //     north: this.property.slot_details[propert]
                      //       .slot_coordinates[0].north,
                      //     south: this.property.slot_details[propert]
                      //       .slot_coordinates[0].south,
                      //     east: this.property.slot_details[propert]
                      //       .slot_coordinates[0].east,
                      //     west: this.property.slot_details[propert]
                      //       .slot_coordinates[0].west
                      //   },
                      //   indexID: this.property.slot_details[propert]
                      // });
                      // rect.setMap(this.map);
                      // this.addListenersOnpoly(rect, propert);
                    } else if (
                      this.property.slot_details[propert].slot_type == "polygon"
                    ) {
                      // var poly = new google.maps.Polygon({
                      //   paths: this.property.slot_details[propert]
                      //     .slot_coordinate,
                      //   strokeColor: this.property.slot_details[propert]
                      //     .slot_color,
                      //   strokeOpacity: 0.8,
                      //   strokeWeight: 2,
                      //   fillColor: this.property.slot_details[propert]
                      //     .slot_color,
                      //   fillOpacity: 0.4,
                      //   indexID: this.property.slot_details[propert],
                      //   map: this.map
                      // });
                      // poly.setMap(this.map);
                      // this.addListenersOnpoly(poly, propert);
                    }
                  }
                }

                this.auth.stoploader();
                /********************/
                /*****Date Picker****/
                /********************/
                this.myDatePickerOptions.enableDays = [];
                this.property.available_dates_first_format1 = [];
                this.property.available_dates_first_format2 = [];
                var el = this;
                for (
                  var dt = 0;
                  dt < this.property.available_dates_first_format.length;
                  dt++
                ) {
                  el.property.available_dates_first_format2[
                    dt
                  ] = el.property.available_dates_first_format[dt].split("-");
                  el.property.available_dates_first_format[dt] =
                    el.property.available_dates_first_format2[dt][1] +
                    "-" +
                    el.property.available_dates_first_format2[dt][0] +
                    "-" +
                    el.property.available_dates_first_format2[dt][2];
                  var ar: any = {
                    year: "",
                    month: "",
                    day: ""
                  };
                  ar.year = new Date(
                    el.property.available_dates_first_format[dt]
                  ).getFullYear();
                  ar.day = new Date(
                    el.property.available_dates_first_format[dt]
                  ).getDate();
                  ar.month =
                    new Date(
                      el.property.available_dates_first_format[dt]
                    ).getMonth() + 1;
                  el.property.available_dates_first_format1.push(ar);
                }
                this.myDatePickerOptions.enableDays = this.property.available_dates_first_format1;
              } else if (data.json().status == 2) {
                this.auth.stoploader();
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.stoploader();
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.auth.errtoast(errorHandler);
            this.auth.stoploader();
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  ionViewDidLoad() {}
  selectedproperty: any = {};
  dynamicbutton: any;
  addListenersOnpoly(shape, index) {
    var el = this;
    google.maps.event.addListener(shape, "click", function(event) {});
  }

  /*****************/
  clickslot(shape) {
    var el = this;
    if (shape) {
      if (el.added != 0 && el.added && el.added.length) {
        for (var id = 0; id < el.added.length; id++) {
          if (
            shape.slot_id === el.added[id].slotid &&
            shape.date == el.added[id].date_of_product
          ) {
            el.dynamicbutton = "View Cart";
            el.selectedproperty = shape;
            el.openEvent();
            break;
          } else {
            el.dynamicbutton = "Add Slot";
            el.selectedproperty = shape;
            el.openEvent();
          }
        }
      } else {
        el.dynamicbutton = "Add Slot";
        el.selectedproperty = shape;
        el.openEvent();
      }
    }
  }
  /******************/
  /***Touch Events***/
  /******************/
  touchstart(e) {
    if (e.cancelable) {
      e.preventDefault();
    }
  }

  /*********************/
  /***Property Share****/
  /*********************/
  propertyshare(property) {
    this.socialSharing.share(
      property.post_title,
      property.post_content,
      property.linked_images[0].image_url,
      null
    );
  }
  private _map: any;
  agm_map: any;
  changeTilt(map) {
    this._map = map;
    this._map.setTilt(0);
    this.agm_map = map;
    map.setOptions({
      zoomControl: "true",
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      streetViewControl: "true",
      streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      },
      mapTypeControl: true,
      mapTypeId: "hybrid",
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_CENTER
      },
      fullscreenControl: false
    });
  }

  status: boolean = false;
  show: boolean = false;
  clickEvent() {
    this.status = !this.status;
    this.show = !this.show;
  }
  openEvent() {
    this.status = true;
    this.show = true;
  }
  closeEvent() {
    this.status = false;
    this.show = false;
  }
  added: any = [];

  addslot(selectedproperty) {
    if (
      selectedproperty.able_to_add_to_cart == "yes" &&
      this.dynamicbutton == "Add Slot"
    ) {
      this.auth.startloader();
      try {
        this.auth
          .addtocart(this.auth.getuserId(), selectedproperty.product_id)
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data) {
                if (data.json().status == 1) {
                  this.auth.toast(data.json().message);
                  this.getcart();
                  this.changeworld();
                  if (this.selectedproperty.date) {
                    this.propertydetail(
                      this.property.ID,
                      this.selectedproperty.date
                    );
                  } else {
                    this.propertydetail(this.property.ID, this.myDate);
                  }
                  this.dynamicbutton = "View Cart";
                  this.auth.clearcookie();
                } else if (data.json().status == 0) {
                  this.auth.toast(data.json().message);
                  this.getcart();
                  if (this.selectedproperty.date) {
                    this.propertydetail(
                      this.property.ID,
                      this.selectedproperty.date
                    );
                  } else {
                    this.propertydetail(this.property.ID, this.myDate);
                  }
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.errtoast(errorHandler);
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
            }
          );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    } else if (this.dynamicbutton == "View Cart") {
      this.goto("MycartPage", null);
    } else {
      this.auth.toast("Currently, you are not able to book this slot");
    }
  }

  getcart() {
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(data => {
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.added = data.json().more_data;
            if (this.added && this.added.length) {
              this.startimers(this.auth.getcarttime());
            } else {
            }
            this.auth.clearcookie();
          } else {
            this.added = 0;
          }
        }
      });
    } catch (e) {
      this.auth.toast(e);
    }
  }
  gotomodal(page, media) {
    if (this.property.embed_video_id) {
      var video: any = {
        embed_video_id: this.property.embed_video_id,
        embed_video_type: this.property.embed_video_type
      };
      // this.navCtrl.push(page, { medias: media, video: video });
      let mediaModal = this.modalCtrl.create(page, {
        medias: media,
        video: video
      });
      mediaModal.present();

      mediaModal.onDidDismiss(data => {});
    } else {
      let mediaModal = this.modalCtrl.create(page, {
        medias: media,
        video: video
      });
      mediaModal.present();

      mediaModal.onDidDismiss(data => {});
    }
  }
  goto(page, media) {
    if (this.property.embed_video_id) {
      var video: any = {
        embed_video_id: this.property.embed_video_id,
        embed_video_type: this.property.embed_video_type
      };
      this.navCtrl.push(page, { medias: media, video: video });
    } else {
      this.navCtrl.push(page, { medias: media, video: video });
    }
  }
  clearcookie() {
    this.auth.clearcookie();
  }
  // AGM

  polygon: any;
  circle: any;
  rectangle: any;
  options: any = {
    lat: 0,
    lng: 0,
    zoom: 18,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    strokeWeight: 3,
    strokePos: 0
  };
  managerOptions = {
    drawingControl: true,
    drawingControlOptions: {
      drawingModes: ["polygon", "rectangle", "circle"]
    },
    polygonOptions: {
      draggable: true,
      editable: true
    },
    drawingMode: "polygon"
  };

  circleCreated($event) {
    if (this.polygon) {
      this.polygon.setMap(null);
    }
    if (this.rectangle) {
      this.rectangle.setMap(null);
    }
    if (this.circle) {
      this.circle.setMap(null);
    }
    this.polygon = null;
    this.rectangle = null;
    this.circle = $event;
  }

  rectangleCreated($event) {
    if (this.polygon) {
      this.polygon.setMap(null);
    }
    if (this.rectangle) {
      this.rectangle.setMap(null);
    }
    if (this.circle) {
      this.circle.setMap(null);
    }
    this.polygon = null;
    this.circle = null;
    this.rectangle = $event;
  }

  polygonCreated($event) {
    if (this.polygon) {
      this.polygon.setMap(null);
    }
    if (this.rectangle) {
      this.rectangle.setMap(null);
    }
    if (this.circle) {
      this.circle.setMap(null);
    }
    this.polygon = $event;
    this.addPolygonChangeEvent(this.polygon);
    google.maps.event.addListener(this.polygon, "coordinates_changed", function(
      index,
      obj
    ) {
      // Polygon object: yourPolygon
    });
  }

  getPaths() {
    if (this.polygon) {
      const vertices = this.polygon.getPaths().getArray()[0];
      let paths = [];
      vertices.getArray().forEach(function(xy, i) {
        let latLng = {
          lat: xy.lat(),
          lng: xy.lng()
        };
        paths.push(JSON.stringify(latLng));
      });
      return paths;
    }
    return [];
  }

  addPolygonChangeEvent(polygon) {
    var me = polygon,
      isBeingDragged = false,
      triggerCoordinatesChanged = function() {
        // Broadcast normalized event
        google.maps.event.trigger(me, "coordinates_changed");
      };

    // If  the overlay is being dragged, set_at gets called repeatedly,
    // so either we can debounce that or igore while dragging,
    // ignoring is more efficient
    google.maps.event.addListener(me, "dragstart", function() {
      isBeingDragged = true;
    });

    // If the overlay is dragged
    google.maps.event.addListener(me, "dragend", function() {
      triggerCoordinatesChanged();
      isBeingDragged = false;
    });

    // Or vertices are added to any of the possible paths, or deleted
    var paths = me.getPaths();
    paths.forEach(function(path, i) {
      google.maps.event.addListener(path, "insert_at", function() {
        triggerCoordinatesChanged();
      });
      google.maps.event.addListener(path, "set_at", function() {
        if (!isBeingDragged) {
          triggerCoordinatesChanged();
        }
      });
      google.maps.event.addListener(path, "remove_at", function() {
        triggerCoordinatesChanged();
      });
    });
  }

  favorite(property_id, user_id) {
    try {
      this.auth.addtofavourite(property_id, user_id).subscribe(data => {
        if (data) {
          var dataa: any = data.json();
          if (dataa.added == false) {
            this.property.is_fav = 0;
            this.auth.vibalert();
          } else if (dataa.added == true) {
            this.property.is_fav = 1;
            this.auth.vibalert();
          }
        }
      });
    } catch (e) {
      this.auth.toast(e);
    }
  }

  datechange(date) {
    setTimeout(() => {
      if (Keyboard.isVisible) {
        Keyboard.hide();
      }
    }, 1000);
    // date.jsdate = this.datepipe.transform(date.jsdate, "yyyy-MM-dd");
    // debugger;
    this.selectedproperty.date = date.formatted;
    // date.jsdate = new Date(date.jsdate);
    if (date.formatted instanceof Date) {
      this.myDate = date.formatted;
      this.propertydetail(this.property.ID, date.formatted);
      this.closeEvent();
    } else {
      this.myDate = date.formatted;
      this.propertydetail(this.property.ID, date.formatted);
      this.closeEvent();
    }
  }

  fullview() {
    let profileModal = this.modalCtrl.create("MapfullviewPage", {
      property_data: this.property,
      property_date: this.myDate,
      cart_data: this.added
    });
    profileModal.present();

    profileModal.onDidDismiss(data => {
      this.property = data.property;
      this.added = data.cart;
      if (this.added && this.added.length > 0) {
        this.startimers(this.auth.getcarttime());
      }
      setTimeout(() => {
        this.resetmap();
      }, 1000);
    });
  }

  startimer(datetime) {
    this.auth.addedtocart(datetime);
    this.triggertimer(datetime);
  }
  triggertimer(datetime) {
    this.startimers(datetime);
  }
  changeworld() {
    this.startimer(new Date());
  }

  ////////////////////////////////
  ///////////Timer////////////////
  ///////////////////////////////
  endatetime: any;
  displaytimer: any = false;
  public startimers(dates) {
    this.displaytimer = true;
    var setdate = new Date(dates);
    var myTimeSpan = 5 * 60 * 1000;
    var currentdate = setdate.setTime(setdate.getTime() + myTimeSpan);
    this.endatetime = this.datepipe.transform(
      currentdate,
      "yyyy-MM-dd HH:mm:ss"
    );
    this.endatetime =
      new Date(this.endatetime).getTime() / 1000 - new Date().getTime() / 1000;
    if (this.endatetime < 0) {
      this.displaytimer = false;
      this.remove_getcart();
    }
  }

  yourOwnFunction() {
    this.displaytimer = false;
    this.remove_getcart();
    this.showalert();
  }
  showalert() {
    const alert = this.alertCtrl.create({
      title: "",
      subTitle:
        "Timer has expired and your cart has been emptied. Please re-add all slots to your cart and checkout before the timer expires.",
      buttons: ["OK"]
    });
    alert.present();
  }
  remove_getcart() {
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            if (data.json() && data.json().status === 1) {
              if (data.json().more_data && data.json().more_data.length) {
                for (var pro = 0; pro < data.json().more_data.length; pro++) {
                  this.removeproductcart(
                    data.json().more_data[pro],
                    pro,
                    data.json().more_data.length - 1
                  );
                }
                this.auth.clearcartstorage();
              }
              this.auth.clearcookie();
            } else {
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (e) {
      this.auth.clearcookie();
      this.auth.toast(e);
    }
  }

  removeproductcart(product, index, matchdata) {
    try {
      this.auth
        .removeproductcart(this.auth.getuserId(), product.product_id)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              if (data.json() && data.json().status === 1) {
                this.auth.clearcookie();
                if (index == matchdata) {
                  setTimeout(() => {
                    this.propertydetail(this.property.ID, this.myDate);
                    this.added = [];
                  }, 1000);
                }
              } else if (data.json().status == 2) {
                this.auth.clearcookie();
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
              }
            }
          },
          errorHandler => {
            this.auth.clearcookie();
            this.auth.errtoast(errorHandler);
          }
        );
    } catch (e) {
      this.auth.clearcookie();
      this.auth.toast(e);
    }
  }
}
