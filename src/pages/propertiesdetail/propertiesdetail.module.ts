import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PropertiesdetailPage } from "./propertiesdetail";
import { ComponentsModule } from "../../components/components.module";
import { AgmCoreModules } from "../../agm/core";
import { MyDatePickerModule } from "mydatepicker";
import { CountdownModule } from "ngx-countdown";

@NgModule({
  declarations: [PropertiesdetailPage],
  imports: [
    MyDatePickerModule,
    ComponentsModule,
    AgmCoreModules,
    CountdownModule,
    IonicPageModule.forChild(PropertiesdetailPage)
  ]
})
export class PropertiesdetailPageModule {}
