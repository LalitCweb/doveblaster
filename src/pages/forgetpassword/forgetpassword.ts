import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController
} from "ionic-angular";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the ForgetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-forgetpassword",
  templateUrl: "forgetpassword.html"
})
export class ForgetpasswordPage implements OnInit {
  // Validation form group definition
  configs: FormGroup;
  user: any = { email: "", password: "" };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    private auth: AuthProvider
  ) {}
  message: any =
    "We have just sent you an email with Password reset instructions.";
  ngOnInit() {
    // Validations Definition
    let EMAILPATTERNS = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.configs = new FormGroup({
      email: new FormControl("", [
        Validators.required,
        Validators.pattern(EMAILPATTERNS)
      ])
    });
  }

  ionViewDidLoad() {}

  submitreset() {
    if (this.configs.valid) {
      // this.navCtrl.push("PasswordassistancePage", { page: "false" });
      this.forgotpassword();
    } else {
      this.auth.toast("Please fill form carefully");
    }
  }
  forgotpassword() {
    try {
      this.auth.forgotpassword(this.configs.value.email).subscribe(data => {
        if (data) {
          if (data.json() && data.json().status == 1) {
            this.auth.toast(this.message);
            this.configs.reset();
          } else if (data.json().status == 0) {
            if (data.json().message) {
              this.auth.toast(this.message);
            } else {
              this.auth.toast("Sorry not able to send email");
            }
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (e) {
      this.auth.toast(e);
    }
  }
  enter() {
    this.navCtrl.setRoot("LoginPage");
  }
}
