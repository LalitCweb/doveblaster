import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MypropertyownerPage } from './mypropertyowner';

@NgModule({
  declarations: [
    MypropertyownerPage,
  ],
  imports: [
    IonicPageModule.forChild(MypropertyownerPage),
  ],
})
export class MypropertyownerPageModule {}
