import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the MypropertyownerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-mypropertyowner",
  templateUrl: "mypropertyowner.html"
})
export class MypropertyownerPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {}

  ionViewDidLoad() {}
  ionViewWillEnter() {
    this.mypropertyuser();
  }

  goto(single_user_id) {
    this.navCtrl.push("MybookingdetailPage", {
      single_user_id: single_user_id
    });
  }

  myproperyusers: any;
  mypropertyuser() {
    try {
      this.auth.startloader();
      this.auth.mypropertyuser(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              this.myproperyusers = data.json().my_prperties_bookings_all_users;
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);

          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }
}
