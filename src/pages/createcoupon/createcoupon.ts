import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the CreatecouponPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-createcoupon",
  templateUrl: "createcoupon.html"
})
export class CreatecouponPage {
  today: any = new Date().toISOString();
  myForm: FormGroup;
  edit: any = false;
  data: any = {
    coupon_id: "",
    coupon_code: "",
    discount_type: "",
    coupon_amount: "",
    usage_limit: "",
    expire_date: ""
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public auth: AuthProvider
  ) {
    this.myForm = this.fb.group({
      coupon_id: [""],
      coupon_code: ["", Validators.required],
      discount_type: ["", Validators.required],
      coupon_amount: ["", Validators.required],
      usage_limit: ["", Validators.required],
      expire_date: ["", Validators.required]
    });
    if (this.navParams.get("single_coupon")) {
      this.edit = true;
      this.myForm.controls["coupon_id"].setValue(
        this.navParams.get("single_coupon").coupon_id
      );
      this.myForm.controls["coupon_code"].setValue(
        this.navParams.get("single_coupon").coupon_name
      );
      this.myForm.controls["discount_type"].setValue(
        this.navParams.get("single_coupon").data.discount_type[0]
      );
      this.myForm.controls["coupon_amount"].setValue(
        this.navParams.get("single_coupon").data.coupon_amount[0]
      );
      this.myForm.controls["usage_limit"].setValue(
        this.navParams.get("single_coupon").data.usage_limit[0]
      );
      this.myForm.controls["expire_date"].setValue(
        this.navParams.get("single_coupon").data.expiry_date[0]
      );
    } else {
      this.data = {};
    }
  }

  ionViewDidLoad() {}

  addcoupon() {
    if (this.myForm.valid) {
      if (this.navParams.get("single_coupon")) {
        this.editcoupon();
      } else {
        this.submitForm();
      }
    }
  }
  editcoupon() {
    this.auth
      .editcoupon(
        this.auth.getuserId(),
        this.myForm.value.coupon_id,
        this.myForm.value
      )
      .subscribe(data => {
        if (data) {
          if (data.json().status == 1) {
            // this.myForm.reset();
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          }
        }
      });
  }
  submitForm() {
    this.auth
      .createcoupon(this.auth.getuserId(), this.myForm.value)
      .subscribe(data => {
        if (data) {
          if (data.json().status == 1) {
            this.myForm.reset();
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          }
        }
      });
  }
}
