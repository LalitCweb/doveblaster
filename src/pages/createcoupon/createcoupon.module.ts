import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreatecouponPage } from './createcoupon';

@NgModule({
  declarations: [
    CreatecouponPage,
  ],
  imports: [
    IonicPageModule.forChild(CreatecouponPage),
  ],
})
export class CreatecouponPageModule {}
