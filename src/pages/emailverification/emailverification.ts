import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the EmailverificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-emailverification",
  templateUrl: "emailverification.html"
})
export class EmailverificationPage {
  number: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {}

  ionViewDidLoad() {}

  goto(page) {
    this.navCtrl.push(page);
  }
  verifyNumber() {
    if (this.number == "" || this.number == null || this.number == undefined) {
      alert("Please Enter Your mobile phone number below.");
    } else {
      this.resendEmail();
      // this.navCtrl.push("EntercodeverifyemailPage");
    }
  }

  resendEmail() {
    this.auth.startloader();
    try {
      this.auth
        .signupphoneverify(this.navParams.get("user_id"), this.number)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data.json()) {
              if (data.json().status == 1) {
                if (data.json().message) {
                  this.auth.toast(data.json().message);
                }
                this.navCtrl.push("EntercodeverifyemailPage", {
                  user_id: this.navParams.get("user_id"),
                  phonumber: JSON.stringify(this.number),
                  userpass: this.navParams.get("userpass")
                });
              } else if (data.json().status == 0) {
                if (data.json().message) {
                  this.auth.toast(data.json().message);
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.toast("Currently not able to signup.");
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
}
