import { Component, ɵConsole } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { DatePipe } from "@angular/common";

/**
 * Generated class for the TodaysbookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-todaysbooking",
  templateUrl: "todaysbooking.html"
})
export class TodaysbookingPage {
  bookings: any = "upcoming";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public datepipe: DatePipe
  ) {}

  ionViewDidLoad() {}
  ionViewWillEnter() {
    this.userreservation();
  }
  history: any;
  upcoming: any;
  currency: any;
  todaybooking: any = [];
  userreservation() {
    try {
      this.auth.startloader();
      this.auth.userreservation(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              // this.history = data.json().past_bookings;
              this.upcoming = data.json().upcoming_bookings;
              this.currency = data.json().currency_symbol;
              this.todaybooking = [];
              for (var tod = 0; tod < this.upcoming.length; tod++) {
                if (
                  this.datepipe.transform(new Date(), "yyyy-MM-dd") ==
                  this.datepipe.transform(
                    this.upcoming[tod].booking_date,
                    "yyyy-MM-dd"
                  )
                ) {
                  this.todaybooking.push(this.upcoming[tod]);
                }
              }
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }

  goto(property) {
    this.navCtrl.push("HunteronpropertyPage", { property_detail: property });
  }
}
