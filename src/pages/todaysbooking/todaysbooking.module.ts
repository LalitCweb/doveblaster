import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TodaysbookingPage } from "./todaysbooking";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [TodaysbookingPage],
  imports: [IonicPageModule.forChild(TodaysbookingPage), PipesModule]
})
export class TodaysbookingPageModule {}
