import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MediauploadPage } from './mediaupload';

@NgModule({
  declarations: [
    MediauploadPage,
  ],
  imports: [
    IonicPageModule.forChild(MediauploadPage),
  ],
})
export class MediauploadPageModule {}
