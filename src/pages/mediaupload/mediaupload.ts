import { Component } from "@angular/core";
import {
  IonicPage,
  Platform,
  NavController,
  NavParams,
  ActionSheetController,
  LoadingController,
  ToastController
} from "ionic-angular";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { Camera } from "@ionic-native/camera";
import { File } from "@ionic-native/file";
import { FilePath } from "@ionic-native/file-path";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the MediauploadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cordova: any;

@IonicPage()
@Component({
  selector: "page-mediaupload",
  templateUrl: "mediaupload.html"
})
export class MediauploadPage {
  images: any = [];
  property: any;
  loading: any;
  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    private filePath: FilePath,
    private file: File,
    private transfer: FileTransfer,
    private camera: Camera,
    private actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public auth: AuthProvider
  ) {
    this.property = this.navParams.get("property_data");
  }

  ionViewDidLoad() {}

  lastImage: any = null;
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select Image Source",
      buttons: [
        {
          text: "Load from Library",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Use Camera",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }
  imgname = "";
  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then(
      imagePath => {
        // Special handling for Android library
        if (
          (this.platform.is("android") &&
            sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) ||
          (this.platform.is("ios") &&
            sourceType === this.camera.PictureSourceType.PHOTOLIBRARY)
        ) {
          this.filePath.resolveNativePath(imagePath).then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf("/") + 1);
            let currentName = imagePath.substring(
              0,
              imagePath.lastIndexOf("?")
            );
            this.imgname = currentName;
            this.lastImage = currentName;
            // this.lastImage = correctPath + currentName;
            this.uploadImage();
            // this.copyFileToLocalDir(
            //   correctPath,
            //   currentName,
            //   this.createFileName()
            // );
          });
        } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf("/") + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf("/") + 1);
          this.imgname = currentName;
          this.lastImage = correctPath + "/" + currentName;
          this.uploadImage();
          // this.copyFileToLocalDir(
          //   correctPath,
          //   currentName,
          //   this.createFileName()
          // );
        }
      },
      err => {
        // this.presentToast("Error while selecting image.");
      }
    );
  }

  // Create a new name for the image
  // private createFileName() {
  //   var d = new Date(),
  //     n = d.getTime(),
  //     newFileName = n + ".jpg";
  //   return newFileName;
  // }

  // Copy the image to a local folder
  // private copyFileToLocalDir(namePath, currentName, newFileName) {
  //   this.file
  //     .copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName)
  //     .then(
  //       success => {
  //         this.lastImage = newFileName;
  //       },
  //       error => {
  //         this.presentToast("Error while storing file.");
  //       }
  //     );
  // }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: "top"
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return "";
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public uploadImage() {
    // Destination URL
    var url = this.auth.uploadslotpicsapi();
    // var url = "https://beta.doveblasters.com/wp-json/wp/v2/media";

    // File for Upload
    // var targetPath = this.pathForImage(this.lastImage);
    var targetPath = this.lastImage;

    // File name only
    var filename = this.imgname;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: { fileName: filename, user_id: this.auth.getuserId() }
    };

    const fileTransfer: FileTransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: "Uploading..."
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(
      data => {
        this.loading.dismissAll();
        this.presentToast("Image succesfully uploaded.");
        debugger;
        var image = {
          src: this.lastImage
        };
        this.images.push(image);
        this.lastImage = "";
      },
      err => {
        debugger;
        this.loading.dismissAll();
        this.presentToast("Error while uploading file.");
      }
    );
  }
}
