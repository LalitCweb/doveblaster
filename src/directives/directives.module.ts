import { NgModule } from '@angular/core';
import { CompleteTestServiceDirective } from './complete-test-service/complete-test-service';
@NgModule({
	declarations: [CompleteTestServiceDirective],
	imports: [],
	exports: [CompleteTestServiceDirective]
})
export class DirectivesModule {}
