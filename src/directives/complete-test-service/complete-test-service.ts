import { Directive } from "@angular/core";

/**
 * Generated class for the CompleteTestServiceDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: "[complete-test-service]" // Attribute selector
})
export class CompleteTestServiceDirective {
  constructor() {}
}
