import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { MyApp } from "./app.component";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
// Providers
import { ApiProvider } from "../providers/api/api";
import { ApicallsProvider } from "../providers/apicalls/apicalls";
import { TracklocationProvider } from "../providers/tracklocation/tracklocation";
// Plugins
import { NativePageTransitions } from "@ionic-native/native-page-transitions";
import { Geolocation } from "@ionic-native/geolocation";
import { AuthProvider } from "../providers/auth/auth";
import { AgmCoreModules } from "../agm/core";
import { AgmCoreModule } from "@agm/core";
import { AgmDirectionModule } from "agm-direction";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import { SocialSharing } from "@ionic-native/social-sharing";
import { AgmJsMarkerClustererModule } from "@agm/js-marker-clusterer";
import { GoogleMapsAPIWrapper } from "@agm/core";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { FilePath } from "@ionic-native/file-path";
import { File } from "@ionic-native/file";
import { Camera } from "@ionic-native/camera";
import { DatePipe } from "@angular/common";
import { Vibration } from "@ionic-native/vibration";
import { NativeGeocoder } from "@ionic-native/native-geocoder";
import { AutoCompleteModule } from "ionic2-auto-complete";
import { CompleteTestServiceProvider } from "../providers/complete-test-service/complete-test-service";
import { GoogleMapsProvider } from "../providers/google-maps/google-maps";
import { OrderModule } from "ngx-order-pipe";
import { PayPal } from "@ionic-native/paypal";
import * as firebase from "firebase/app";
import "firebase/database"; // If using Firebase database
import "firebase/storage"; // If using Firebase storage
import "firebase/auth";
import "firebase/firestore";
import "firebase/messaging";
import { Firebase } from "@ionic-native/firebase";
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { FcmProvider } from "../providers/fcm/fcm";
import { CountdownModule } from "ngx-countdown";
import { LocalNotifications } from "@ionic-native/local-notifications";
// import { BackgroundMode } from "@ionic-native/background-mode";
import { FirestoreSettingsToken } from "@angular/fire/firestore";
import { Toast } from "@ionic-native/toast";

const firebaseConfig = {
  apiKey: "AIzaSyBha9xVn3jY0CX1cPShQVv1GCjcJxgn58Y",
  authDomain: "dayleasing-225305.firebaseapp.com",
  databaseURL: "https://dayleasing-225305.firebaseio.com",
  projectId: "dayleasing-225305",
  storageBucket: "dayleasing-225305.appspot.com",
  messagingSenderId: "770938813402"
};
firebase.initializeApp(firebaseConfig);
@NgModule({
  declarations: [MyApp],
  imports: [
    OrderModule,
    CountdownModule,
    HttpModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    AutoCompleteModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AgmCoreModules.forRoot({
      apiKey: "AIzaSyC9PnuRk42kbCPMOvsfHpn40r5SoyN38zI",
      libraries: ["places", "drawing", "geometry"]
    }),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyC9PnuRk42kbCPMOvsfHpn40r5SoyN38zI",
      libraries: ["places", "drawing", "geometry"]
    }),
    AgmJsMarkerClustererModule,
    AgmDirectionModule,
    IonicModule.forRoot(MyApp, {
      preloadModules: true
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    LocalNotifications,
    Firebase,
    // BackgroundMode,
    StatusBar,
    SplashScreen,
    Geolocation,
    SocialSharing,
    FileTransfer,
    FileTransferObject,
    File,
    FilePath,
    Camera,
    NativeGeocoder,
    NativePageTransitions,
    GoogleMapsAPIWrapper,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: FirestoreSettingsToken, useValue: {} },
    ApiProvider,
    ApicallsProvider,
    AuthProvider,
    DatePipe,
    Vibration,
    CompleteTestServiceProvider,
    GoogleMapsProvider,
    PayPal,
    FcmProvider,
    TracklocationProvider,
    Toast
  ]
})
export class AppModule {}
