import { NgModule } from "@angular/core";
import { BottombarComponent } from "./bottombar/bottombar";
import { PipesModule } from "../pipes/pipes.module";
@NgModule({
  declarations: [BottombarComponent],
  imports: [PipesModule],
  exports: [BottombarComponent]
})
export class ComponentsModule {}
