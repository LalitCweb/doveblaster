import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation";
/*
  Generated class for the GoogleMapsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GoogleMapsProvider {
  mapElement: any;
  pleaseConnect: any;
  map: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  currentMarker: any;
  apiKey: string = "AIzaSyC9PnuRk42kbCPMOvsfHpn40r5SoyN38zI";
  constructor(public http: HttpClient, public geolocation: Geolocation) {}
  init(mapElement: any, pleaseConnect: any): Promise<any> {
    this.mapElement = mapElement;
    this.pleaseConnect = pleaseConnect;

    return this.loadGoogleMaps();
  }

  loadGoogleMaps(): Promise<any> {
    return new Promise(resolve => {
      if (typeof google == "undefined" || typeof google.maps == "undefined") {
        this.disableMap();
      } else {
        resolve(true);
      }
    });
  }

  initMap(): Promise<any> {
    this.mapInitialised = true;

    return new Promise(resolve => {
      this.geolocation.getCurrentPosition().then(position => {
        let latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.mapElement, mapOptions);
        resolve(true);
      });
    });
  }

  disableMap(): void {
    if (this.pleaseConnect) {
      this.pleaseConnect.style.display = "block";
    }
  }

  enableMap(): void {
    if (this.pleaseConnect) {
      this.pleaseConnect.style.display = "none";
    }
  }
}
