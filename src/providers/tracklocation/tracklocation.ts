import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation";
import { AuthProvider } from "../../providers/auth/auth";
import { FcmProvider } from "../../providers/fcm/fcm";

declare var navigator: any;
@Injectable()
export class TracklocationProvider {
  constructor(
    public http: HttpClient,
    private geolocation: Geolocation,
    public auth: AuthProvider,
    public fcmp: FcmProvider
  ) {}
  watch: any;
  getlocation() {
    if (this.watch) {
    } else {
      let options = {
        enableHighAccuracy: true
      };
      this.geolocation
        .getCurrentPosition(options)
        .then(resp => {
          localStorage.setItem(
            "currectlat",
            JSON.stringify(resp.coords.latitude)
          );
          localStorage.setItem(
            "currectlng",
            JSON.stringify(resp.coords.longitude)
          );
        })
        .catch(error => {
          this.auth.errtoast(error);
        });
      this.watch = this.geolocation.watchPosition(options);
      this.watch.subscribe(data => {
        // data can be a set of coordinates, or an error (if an error occurred).
        localStorage.setItem(
          "currectlat",
          JSON.stringify(data.coords.latitude)
        );
        localStorage.setItem(
          "currectlng",
          JSON.stringify(data.coords.longitude)
        );
      });
    }
  }

  getlat() {
    if (localStorage.getItem("currectlat")) {
      return JSON.parse(localStorage.getItem("currectlat"));
    }
  }
  getlng() {
    if (localStorage.getItem("currectlng")) {
      return JSON.parse(localStorage.getItem("currectlng"));
    }
  }

  stoplocation() {
    if (this.watch) {
      navigator.geolocation.clearWatch(this.watch);
    }
  }
}
