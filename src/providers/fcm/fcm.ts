import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Firebase } from "@ionic-native/firebase";
import { Platform } from "ionic-angular";
import { AngularFirestore } from "angularfire2/firestore";
import { AngularFireDatabase } from "angularfire2/database";
// import { AngularFirestore } from '@angular/fire/firestore';
import { AuthProvider } from "../auth/auth";
import { DatePipe } from "@angular/common";
/*
  Generated class for the FcmProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FcmProvider {
  constructor(
    public http: HttpClient,
    public firebaseNative: Firebase,
    public afs: AngularFirestore,
    private platform: Platform,
    public auth: AuthProvider,
    public afd: AngularFireDatabase,
    public datepipe: DatePipe
  ) {}
  checktoken() {
    if (this.platform.is("android")) {
      return this.firebaseNative.getToken();
    }
    if (this.platform.is("ios")) {
      this.firebaseNative.grantPermission();
      return this.firebaseNative.getToken();
    }
  }

  deleteToken(key) {
    return this.afd.list("/dayleasing_data/").remove(key);
  }
  // Get permission from the user
  async getToken() {
    let token;

    if (this.platform.is("android")) {
      token = await this.firebaseNative.getToken();
    }

    if (this.platform.is("ios")) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }

    return this.saveTokenToFirestore(token);
  }
  async updateToken(key) {
    let token;

    if (this.platform.is("android")) {
      token = await this.firebaseNative.getToken();
    }

    if (this.platform.is("ios")) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }

    return this.updateTokenToFirestore(token, key);
  }
  subscribetotopic() {
    this.firebaseNative.subscribe("todaybooking");
  }
  unsubscribeFromTopic() {
    this.firebaseNative.subscribe("todaybooking");
  }
  async updateGeolocation(key, lat, lng, booked_property_id) {
    let token;

    if (this.platform.is("android")) {
      token = await this.firebaseNative.getToken();
    }

    if (this.platform.is("ios")) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }

    return this.saveTokenToFirestore2(key, token, lat, lng, booked_property_id);
  }

  async updateGeolocation2(key, propertyId) {
    let token;

    if (this.platform.is("android")) {
      token = await this.firebaseNative.getToken();
    }

    if (this.platform.is("ios")) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }

    return this.saveTokenToFirestore3(key, token, propertyId);
  }

  // Save the token to firestore
  private saveTokenToFirestore2(key, token, lat, lng, booked_property_id) {
    if (!token) return;

    // const devicesRef = this.afs.collection("devices");

    const docData = {
      token,
      userId: this.auth.getuserId(),
      userName: this.auth.getUsername("username"),
      lat: lat,
      lng: lng,
      propertyId: booked_property_id,
      datetime: this.datepipe.transform(new Date(), "yyyy-MM-dd h:mm a")
    };

    // return devicesRef.doc(token).set(docData);
    return this.afd.list("/dayleasing_data/").update(key, docData);
  }
  // Save the token to firestore
  private saveTokenToFirestore3(key, token, propertyId) {
    if (!token) return;

    // const devicesRef = this.afs.collection("devices");

    const docData = {
      token,
      userId: this.auth.getuserId(),
      userName: this.auth.getUsername("username"),
      lat: "",
      lng: "",
      propertyId: ""
    };

    // return devicesRef.doc(token).set(docData);
    return this.afd.list("/dayleasing_data/").update(key, docData);
  }

  // Save the token to firestore
  private saveTokenToFirestore(token) {
    if (!token) return;
    if (!this.auth.getuserId()) return;
    // const devicesRef = this.afs.collection("devices");

    const docData = {
      token,
      userId: this.auth.getuserId()
    };

    // return devicesRef.doc(token).set(docData);

    return this.afd.list("/dayleasing_data/").push(docData);
  }

  // Save the token to firestore
  private updateTokenToFirestore(token, key) {
    if (!token) return;
    if (!this.auth.getuserId()) return;

    // const devicesRef = this.afs.collection("devices");

    const docData = {
      token,
      userId: this.auth.getuserId()
    };

    // return devicesRef.doc(token).set(docData);
    // here you get the key
    return this.afd.list("/dayleasing_data/").update(key, docData);
  }

  removeitem(id) {
    return this.afd.list("/dayleasing_data/").remove(id);
  }

  // Listen to incoming FCM messages
  listenToNotifications() {
    return this.firebaseNative.onNotificationOpen();
  }

  getdata() {
    return this.afd.list("/dayleasing_data/");
  }

  checkusers() {
    return this.afd.list("/dayleasing_data/", ref =>
      ref.orderByChild("userId").equalTo(this.auth.getuserId())
    );
  }
  checkproperty(property_id) {
    return this.afd.list("/dayleasing_data/", ref =>
      ref.orderByChild("propertyId").equalTo(property_id)
    );
  }
  checktoken2() {
    if (localStorage.getItem("dayleasing_token")) {
      return this.afd.list("/dayleasing_data/", ref =>
        ref
          .orderByChild("token")
          .equalTo(localStorage.getItem("dayleasing_token"))
      );
    }
  }
}
