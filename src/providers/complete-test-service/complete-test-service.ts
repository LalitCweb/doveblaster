import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AutoCompleteService } from "ionic2-auto-complete";

/*
  Generated class for the CompleteTestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CompleteTestServiceProvider implements AutoCompleteService {
  labelAttribute = "name";
  constructor(public http: HttpClient) {}
  getResults(keyword: string) {
    return this.http
      .get("https://restcountries.eu/rest/v1/name/" + keyword)
      .map(result => {
        return result;
      });
  }
}
