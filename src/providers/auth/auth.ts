import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from "@angular/http";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environment";
import {
  ToastController,
  LoadingController,
  Platform,
  AlertController
} from "ionic-angular";
import { Vibration } from "@ionic-native/vibration";
import "rxjs/add/operator/map";
import { DatePipe } from "@angular/common";
import { ValueTransformer } from "@angular/compiler/src/util";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { Toast } from "@ionic-native/toast";
declare var window: any;
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  api_url: any;
  constructor(
    public http: Http,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public https: HttpClient,
    public vibration: Vibration,
    public datepipe: DatePipe,
    public platform: Platform,
    public localnotification: LocalNotifications,
    private alertCtrl: AlertController,
    private toastcntrl: Toast
  ) {}
  options: any;
  locationiconUrl() {
    return environment.locationiconUrl;
  }
  locationiconUrl1() {
    return environment.locationiconUrl1;
  }
  showalert(title, subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ["Ok"]
    });
    alert.present();
  }
  propertynotification(propertyId) {
    var dtt: any = {
      date: new Date(),
      property: propertyId
    };
    localStorage.setItem(
      "dayleasing_property_notification",
      JSON.stringify(dtt)
    );
  }
  propertyslotnotification(slotId) {
    var dtt: any = {
      date: new Date(),
      property: slotId
    };
    localStorage.setItem(
      "dayleasing_property_slot_notification",
      JSON.stringify(dtt)
    );
  }
  removepropertynotification() {
    if (localStorage.getItem("dayleasing_property_notification")) {
      localStorage.removeItem("dayleasing_property_notification");
    }
  }
  removeslotpropertynotification() {
    if (localStorage.getItem("dayleasing_property_slot_notification")) {
      localStorage.removeItem("dayleasing_property_slot_notification");
    }
  }
  getpropertynotification(propertyId) {
    var ee = this;
    if (localStorage.getItem("dayleasing_property_notification")) {
      if (
        ee.datepipe.transform(
          JSON.parse(localStorage.getItem("dayleasing_property_notification"))
            .date,
          "yyyy-MM-dd"
        ) == ee.datepipe.transform(new Date(), "yyyy-MM-dd") &&
        JSON.parse(localStorage.getItem("dayleasing_property_notification"))
          .property == propertyId
      ) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
  getpropertyslotnotification(slotId) {
    if (localStorage.getItem("dayleasing_property_slot_notification")) {
      if (
        this.datepipe.transform(
          JSON.parse(
            localStorage.getItem("dayleasing_property_slot_notification")
          ).date,
          "yyyy-MM-dd"
        ) == this.datepipe.transform(new Date(), "yyyy-MM-dd") &&
        JSON.parse(
          localStorage.getItem("dayleasing_property_slot_notification")
        ).property == slotId
      ) {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
  sendlocalnotification(message) {
    this.localnotification.schedule({
      id: 1,
      text: message
    });
  }
  sendpushnotification(token, body) {
    this.token =
      "key=AAAAs3-Iu9o:APA91bGK1WTcmYR6edkyjRcU4PJBiijdsuLzGX_Hu6K3burmEnHaz65u9fBZR1XKF4aaT_i4N8sq9XXlbYspPv9h_DI-nIcNeOrHkTGN4AU_NQmtNU7HnPzz7JltMc32k2LAn5eAwgvN";
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var postdata = {
      notification: {
        body: body,
        sound: "default",
        click_action: "FCM_PLUGIN_ACTIVITY",
        icon: "fcm_push_icon"
      },
      data: {},
      to: token,
      priority: "high",
      restricted_package_name: ""
    };
    this.api_url = environment.firebase;
    return this.http.post(this.api_url, postdata, this.options);
  }

  cancelreservation(userid, data) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var postdata = {
      linked_slot_tothis_booking: data.linked_slot_tothis_booking,
      booking_product_price_after_discount:
        data.booking_product_price_after_discount,
      user_id: userid,
      linkedproduct: data.linked_product_of_booking,
      linked_order_id_toreservation: data.linked_order_id_toreservation,
      reservationid: data.reservationid,
      policy: data.show_policy
    };
    this.api_url = environment.site_url + environment.cancelproperty;
    return this.http.post(this.api_url, postdata, this.options);
  }
  searchkeyword(apisearchedkeyword) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var postdata = {
      apisearchedkeyword: apisearchedkeyword
    };
    this.api_url = environment.site_url + environment.searchkeyword;
    return this.http.post(this.api_url, postdata, this.options);
  }
  landowners(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var postdata = {
      user_id: user_id
    };
    this.api_url = environment.site_url + environment.getlandowner;
    return this.http.post(this.api_url, postdata, this.options);
  }
  disputereservation(userid, data) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var postdata = {
      user_id: userid,
      product_id: data.linked_product_of_booking,
      order_id: data.linked_order_id_toreservation,
      reason: data.reason,
      reservationid: data.reservationid
    };
    this.api_url = environment.site_url + environment.disputeproperty;
    return this.http.post(this.api_url, postdata, this.options);
  }

  postlogin(username, password) {
    this.api_url = environment.site_url + environment.jwt_url;
    var headers = new Headers();
    // var body = { username: username, password: password };
    // headers.append("Content-Type", "application/json");
    var body = `username=${username}&password=${password}`;
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    this.options = new RequestOptions({ headers: headers });
    return this.http.post(this.api_url, body, this.options);
  }

  getallproperty(data) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var postdata = {
      user_id: data
    };
    this.api_url = environment.site_url + environment.all_property;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getsinglealldetail(property_id, user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      property_id: property_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.single_property_detail;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getfavourites(userid) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: userid
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.favourites_property;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getallmessage(offset, author) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      paged: offset,
      postauthor_id: author
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.inbox_url;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getallproperties(user_id, offset) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var postdata = {
      paged: offset,
      user_id: user_id
    };
    this.api_url = environment.site_url + environment.allproperties_url;
    return this.http.post(this.api_url, postdata, this.options);
  }
  token: any;

  addtofavourite(property_id, user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      property_id: property_id,
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.favourite_url;
    return this.http.post(this.api_url, postdata, this.options);
  }
  getmessages1(property_id, userid) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      prop_id: property_id,
      user_id: userid
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });
    // this.options = { headers: headers };
    this.api_url = environment.site_url + environment.notifymessage1;
    return this.http.post(this.api_url, postdata, this.options);
  }
  reservation(userid, paged, post_per_page) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      paged: paged,
      user_id: userid,
      post_per_page: post_per_page
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });
    // this.options = { headers: headers };
    this.api_url = environment.site_url + environment.user_reservation;
    return this.http.post(this.api_url, postdata, this.options);
  }
  getuserReservation(userid, paged, post_per_page) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      paged: paged,
      user_id: userid,
      post_per_page: post_per_page
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({
      headers: headers,
      withCredentials: true
    });
    // this.options = { headers: headers };
    this.api_url = environment.site_url + environment.user_reservation;
    return this.http.post(this.api_url, postdata, this.options);
  }

  deletemessage(mesg_id, user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      mesg_id: mesg_id,
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.delete_msg_url +
      "?mesg_id=" +
      mesg_id +
      "&user_id=" +
      user_id;
    return this.http.post(this.api_url, postdata, this.options);
  }

  replymessage(user_id, mesg_id, title, content) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      mesg_id: mesg_id,
      user_id: user_id,
      title: title,
      content: content
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.message_reply;
    return this.http.post(this.api_url, postdata, this.options);
  }

  confirmorder(data) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.confirmorder;
    return this.http.post(this.api_url, data, this.options);
  }

  updateprofileapi(data, user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      firstname: data.fname,
      secondname: data.lname,
      useremail: data.email,
      userphone: data.phone,
      usermobile: data.mphone,
      description: data.about,
      userfacebook: data.facebook,
      usertwitter: data.twitter,
      userlinkedin: "",
      userpinterest: "",
      live_in: data.live,
      i_speak: data.speak,
      paypal_payments_to: data.ppayment,
      payment_info: data.mphone
    };
    this.api_url = environment.site_url + environment.updateprofileapi;
    return this.http.post(this.api_url, dataa, this.options);
  }

  createprop1(user_id, data, config, location, checkboxes_eminities) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      wpestate_title: data.title,
      prop_category: data.category,
      prop_action_category: data.wantlease,
      property_country: data.country,
      property_city: config.city,
      property_admin_area: data.property_admin_area,
      prop_area_center_lat: location.prop_area_center_lat,
      prop_area_center_lng: location.prop_area_center_lng,
      property_county: data.county,
      property_state: data.state,
      property_zip: data.zipcode,
      property_description: data.description,
      checkboxes_eminities: checkboxes_eminities
    };
    this.api_url = environment.site_url + environment.createprop;
    return this.http.post(this.api_url, dataa, this.options);
  }

  createprop2(
    user_id,
    property_id,
    area,
    shape,
    shapecords,
    shapefillColor,
    mapaddress,
    lat,
    lng,
    currnt_zoom
  ) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      property_id: property_id,
      prop_total_area: area,
      shapetypetosave: shape,
      shapecords: shapecords,
      shapefillColor: shapefillColor,
      mapaddress: mapaddress,
      center_lat: lat,
      center_lng: lng,
      currnt_zoom: currnt_zoom
    };
    this.api_url = environment.site_url + environment.createprop2;
    return this.http.post(this.api_url, dataa, this.options);
  }

  createprop3(user_id, property_id, post_id, data) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      property_id: property_id,
      area_details: data,
      post_id: post_id
    };
    this.api_url = environment.site_url + environment.createprop3;
    return this.http.post(this.api_url, dataa, this.options);
  }

  createprop4(user_id, data, property_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      area_details: data,
      user_id: user_id,
      property_id: property_id
    };
    this.api_url = environment.site_url + environment.createprop4;
    return this.http.post(this.api_url, dataa, this.options);
  }

  updatepicapi() {
    return environment.site_url + environment.updatepicapi;
  }
  updateidapi() {
    return environment.site_url + environment.updateidapi;
  }
  uploadmedia() {
    return environment.site_url + environment.uploadimages;
  }

  getcategories(user_id) {
    var dataa: any = {
      user_id: user_id
    };
    this.api_url = environment.site_url + environment.categoryapi;
    return this.http.post(this.api_url, dataa);
  }

  getlease(user_id) {
    var dataa: any = {
      user_id: user_id
    };
    this.api_url = environment.site_url + environment.leaseapi;
    return this.http.post(this.api_url, dataa);
  }

  getfeatures(user_id) {
    var dataa: any = {
      user_id: user_id
    };
    this.api_url = environment.site_url + environment.featuresapi;
    return this.http.post(this.api_url, dataa);
  }

  subscriptionList(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id
    };
    this.api_url = environment.site_url + environment.subscriptionList;
    return this.http.post(this.api_url, dataa, this.options);
  }

  buysubscriptionList(user_id, pack_id, pack_name) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      pack_name: pack_name,
      pack_id: pack_id
    };
    this.api_url = environment.site_url + environment.subscriptionapi;
    return this.http.post(this.api_url, dataa, this.options);
  }

  getCart(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id
    };
    this.api_url = environment.site_url + environment.getcart;
    return this.http.post(this.api_url, dataa, this.options);
  }

  removeproductcart(user_id, product_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      product_id: product_id
    };
    this.api_url = environment.site_url + environment.removeproductcart;
    return this.http.post(this.api_url, dataa, this.options);
  }

  // async getuserReservation(offset, postperpage): Promise<any> {
  //   const response = await this.http
  //     .get(
  //       environment.site_url +
  //         environment.user_reservation +
  //         "?offset=" +
  //         offset +
  //         "&postperpage=" +
  //         postperpage
  //     )
  //     .toPromise();
  //   return response.json();
  // }

  getpropertydetail(user_id, property_id, date) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      property_id: property_id,
      current_date: date
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.property_detail_url;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getwallet(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.wallet_url;
    return this.http.post(this.api_url, postdata, this.options);
  }

  uploadslotpicsapi() {
    return environment.site_url + environment.uploadimages;
  }
  async getuserdata1(user_name): Promise<any> {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_name: user_name
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.getusername_url;
    const response = await this.http
      .post(this.api_url, postdata, this.options)
      .toPromise();
    return response.json();
  }

  getuserdata(user_name) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_name: user_name
    };
    var headers = new Headers();
    // headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    headers.append("Content-Type", "application/json");
    // this.options = { headers: headers };
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.getusername_url;
    return this.http.post(this.api_url, postdata, this.options);
  }

  createcoupon(user_id, value) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      coupon_code: value.coupon_code,
      discount_type: value.discount_type,
      coupon_amount: value.coupon_amount,
      usage_limit: value.usage_limit,
      expire_date: value.expire_date,
      coupon_to_remove_handlingcharges: ""
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.createcoupon;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getallcoupon(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.couponslist;
    return this.http.post(this.api_url, postdata, this.options);
  }

  removecoupon(user_id, coupon_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      coupon_id: coupon_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.deletecoupon;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getcoupon(user_id, coupon_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      coupon_id: coupon_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.getcoupon;
    return this.http.post(this.api_url, postdata, this.options);
  }

  editcoupon(user_id, coupon_id, value) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      coupon_id: coupon_id,
      coupon_code: value.coupon_code,
      discount_type: value.discount_type,
      coupon_amount: value.coupon_amount,
      usage_limit: value.usage_limit,
      expire_date: value.expire_date,
      coupon_to_remove_handlingcharges: ""
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.editcoupon;
    return this.http.post(this.api_url, postdata, this.options);
  }

  searchdata(
    start_date,
    end_date,
    category_values,
    action_values,
    all_checkers,
    city,
    property_admin_area,
    country,
    price_low,
    price_max
  ) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    if (action_values && action_values.length > 1) {
      action_values = undefined;
    } else {
      action_values = String(action_values);
    }
    var postdata = {
      start_date: start_date,
      end_date: end_date,
      category_values: category_values,
      action_values: action_values,
      all_checkers: all_checkers,
      city: city,
      property_admin_area: property_admin_area,
      country: country,
      price_low: price_low,
      price_max: price_max,
      newpage: 1,
      postid: 33
      // check_in: end_date
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.searchapi;
    return this.http.post(this.api_url, postdata, this.options);
  }

  serchformap(
    start_date,
    end_date,
    category_values,
    action_values,
    all_checkers,
    city,
    property_admin_area,
    country,
    price_low,
    price_max
  ) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata: any;
    if (action_values && action_values.length > 1) {
      postdata = {
        start_date: start_date,
        end_date: end_date,
        category_values: category_values,
        all_checkers: all_checkers,
        city: city,
        property_admin_area: property_admin_area,
        country: country,
        price_low: price_low,
        price_max: price_max,
        newpage: 1,
        postid: 33
        // check_in: end_date
      };
    } else {
      postdata = {
        start_date: start_date,
        end_date: end_date,
        category_values: category_values,
        action_values: String(action_values),
        all_checkers: all_checkers,
        city: city,
        property_admin_area: property_admin_area,
        country: country,
        price_low: price_low,
        price_max: price_max
      };
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.serchformap;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getmore(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.moreoption;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getmoredetail(user_id, menu_item_id, object_id, object) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      menu_item_id: menu_item_id,
      object_id: object_id,
      object: object
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.moreoptiondetail;
    return this.http.post(this.api_url, postdata, this.options);
  }

  myproperties(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.myproperties;
    return this.http.post(this.api_url, postdata, this.options);
  }

  deleteproperties(user_id, property_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      property_id: property_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.deleteproperty;
    return this.http.post(this.api_url, postdata, this.options);
  }

  disableproperties(user_id, property_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      property_id: property_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.disableproperty;
    return this.http.post(this.api_url, postdata, this.options);
  }

  featureproperties(user_id, property_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      property_id: property_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.featureproperty;
    return this.http.post(this.api_url, postdata, this.options);
  }

  mypropertiesbooking(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.mypropertiesbooking;
    return this.http.post(this.api_url, postdata, this.options);
  }

  userreservation(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.userreservation;
    return this.http.post(this.api_url, postdata, this.options);
  }

  mypropertyuser(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.mypropertyuser;
    return this.http.post(this.api_url, postdata, this.options);
  }

  mypropertyusersingle(user_id, single_user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      my_id: user_id,
      single_user_id: single_user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.mypropertyusersingle;
    return this.http.post(this.api_url, postdata, this.options);
  }

  changepassword(user_id, new_password, old_password, confirm_password) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      newpass: new_password,
      oldpass: old_password,
      renewpass: confirm_password
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.changepassword;
    return this.http.post(this.api_url, postdata, this.options);
  }

  forgotpassword(user_name) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      postid: 0,
      type: 1,
      forgot_email: user_name
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.forgotpassword;
    return this.http.post(this.api_url, postdata, this.options);
  }

  signup(value, user_type) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_login_register: value.cname,
      user_email_register: value.email,
      propid: 0,
      user_type: user_type,
      user_pass: value.password,
      user_pass_retype: value.cpassword,
      registering_from_app: 1
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.signup;
    return this.http.post(this.api_url, postdata, this.options);
  }

  signupemailverify(user_id, otp) {
    var postdata = {
      user_id: user_id,
      digituniquecode: otp
    };
    this.api_url = environment.site_url + environment.verifyemail;
    return this.http.post(this.api_url, postdata);
  }

  signupphoneverify(user_id, mobile_no) {
    var postdata = {
      user_id: user_id,
      mobile_no: mobile_no
    };
    this.api_url =
      environment.site_url +
      environment.savephone +
      "?user_id=" +
      user_id +
      "&mobile_no=" +
      mobile_no;
    return this.http.post(this.api_url, postdata);
  }

  signupphoneverify2(user_id, validate_phoneno) {
    var postdata = {
      user_id: user_id,
      validate_phoneno: validate_phoneno
    };
    this.api_url =
      environment.site_url +
      environment.verifyphone +
      "?user_id=" +
      user_id +
      "&validate_phoneno=" +
      validate_phoneno;
    return this.http.post(this.api_url, postdata);
  }

  applycoupon(user_id, coupon_code) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      coupon_code: coupon_code
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.applycoupon;
    return this.http.post(this.api_url, postdata, this.options);
  }

  addtocart(user_id, product_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      product_id: product_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.addtocart;
    return this.http.post(this.api_url, postdata, this.options);
  }

  savevideo(user_id, property_id, video_type, video_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      property_id: property_id,
      video_type: video_type,
      video_id: video_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.uploadimages;
    return this.http.post(this.api_url, postdata, this.options);
  }

  deleteslot(user_id, id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      id: id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.deleteslot;
    return this.http.post(this.api_url, postdata, this.options);
  }

  editproperty(
    user_id,
    data,
    config,
    location,
    property_id,
    checkboxes_eminities
  ) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      property_id: property_id,
      user_id: user_id,
      wpestate_title: data.title,
      prop_category: data.category,
      prop_action_category: data.wantlease,
      property_country: data.country,
      property_city: config.city,
      property_admin_area: data.property_admin_area,
      prop_area_center_lat: location.prop_area_center_lat,
      prop_area_center_lng: location.prop_area_center_lng,
      property_county: data.county,
      property_state: data.state,
      property_zip: data.zipcode,
      property_description: data.description,
      checkboxes_eminities: checkboxes_eminities
    };
    this.api_url = environment.site_url + environment.editproperty;
    return this.http.post(this.api_url, dataa, this.options);
  }

  deletemedia(user_id, media, property_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      images_id: media,
      property_id: property_id
    };
    this.api_url = environment.site_url + environment.deletemedia;
    return this.http.post(this.api_url, dataa, this.options);
  }

  editstep31(
    user_id,
    area_id,
    prop_area_name,
    prop_area_slot_details,
    prop_area_slot_color
  ) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      area_id: area_id,
      prop_area_name: prop_area_name,
      prop_area_slot_details: prop_area_slot_details,
      prop_area_slot_color: prop_area_slot_color
    };
    this.api_url = environment.site_url + environment.editstep31;
    return this.http.post(this.api_url, dataa, this.options);
  }

  editstep32(
    user_id,
    current_prop_id,
    current_area_id,
    firstdate,
    enddate,
    daterangeprice
  ) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      current_prop_id: current_prop_id,
      current_area_id: current_area_id,
      firstdate: firstdate,
      enddate: enddate,
      daterangeprice: daterangeprice
    };
    this.api_url = environment.site_url + environment.editstep32;
    return this.http.post(this.api_url, dataa, this.options);
  }

  editstep33(user_id, daterange_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      daterange_id: daterange_id
    };
    this.api_url = environment.site_url + environment.editstep33;
    return this.http.post(this.api_url, dataa, this.options);
  }

  makepayment() {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var data: any = {};
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.generateorder;
    return this.http.post(this.api_url, data, this.options);
  }

  orderdetail(order_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var dataa: any = {
      order_id: order_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.orderdetail;
    return this.http.post(this.api_url, dataa, this.options);
  }

  testdata(data) {
    this.api_url = environment.site_url + environment.editstep33;
    return this.http.post(
      "https://beta.doveblasters.com/wp-json/testform/testingpost",
      data
    );
  }

  isLogin() {
    if (
      localStorage.getItem("dayleasing_user") &&
      localStorage.getItem("dayleasing_user_type")
    ) {
      return true;
    } else {
      return false;
    }
  }

  isOwner() {
    if (localStorage.getItem("dayleasing_user_type")) {
      var data: any = localStorage.getItem("dayleasing_user_type");
      data = JSON.parse(data);
      if (data.user_type && data.user_type[0] == "0") {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  getUser() {
    if (localStorage.getItem("dayleasing_user")) {
      return localStorage.getItem("dayleasing_user");
    } else {
      return null;
    }
  }

  getuserpic() {
    if (localStorage.getItem("dayleasing_user_type")) {
      if (
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .custom_picture &&
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .custom_picture[0]
      ) {
        return JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .custom_picture[0];
      } else {
        return JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .user_id_picture;
      }
    }
  }

  getuseridpic() {
    if (localStorage.getItem("dayleasing_user_type")) {
      if (
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .user_id_image &&
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .user_id_image[0]
      ) {
        return JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .user_id_image[0];
      }
    }
  }

  getUsername(data) {
    if (localStorage.getItem("dayleasing_user")) {
      if (data == "username") {
        return JSON.parse(localStorage.getItem("dayleasing_user"))
          .user_display_name;
      } else if (data == "email") {
        return JSON.parse(localStorage.getItem("dayleasing_user")).user_email;
      }
    } else {
      return null;
    }
  }
  getuserId() {
    if (localStorage.getItem("dayleasing_user_type")) {
      return JSON.parse(localStorage.getItem("dayleasing_user_type")).user_id;
    } else {
      return null;
    }
  }

  getpackage_id() {
    if (
      localStorage.getItem("dayleasing_user_type") &&
      JSON.parse(localStorage.getItem("dayleasing_user_type")).package_id
    ) {
      return JSON.parse(localStorage.getItem("dayleasing_user_type"))
        .package_id[0];
    } else {
      return null;
    }
  }

  checkpackage(packag) {
    if (localStorage.getItem("dayleasing_user_type")) {
      if (
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .package_id[0] == parseInt(packag)
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  getProfile() {
    if (localStorage.getItem("dayleasing_user_type")) {
      return localStorage.getItem("dayleasing_user_type");
    } else {
      return null;
    }
  }

  addedtocart(datetime) {
    if (localStorage.getItem("dayleasing_user_type")) {
      localStorage.setItem("dayleasing_cart_product_added", datetime);
      return "Saved";
    } else {
      return null;
    }
  }

  getcarttime() {
    if (localStorage.getItem("dayleasing_cart_product_added")) {
      return localStorage.getItem("dayleasing_cart_product_added");
    } else {
      return false;
    }
  }

  clearcartstorage() {
    localStorage.removeItem("dayleasing_cart_product_added");
  }

  vibalert() {
    this.vibration.vibrate(300);
  }

  toast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "top"
    });

    toast.onDidDismiss(() => {});

    toast.present();
  }
  // toast(msg) {
  //   this.toastcntrl.show(msg, "3000", "top").subscribe(toast => {
  //     console.log(toast);
  //   });
  // }
  loading: any;
  startloader() {
    this.loading = this.loadingCtrl.create({
      spinner: "crescent"
    });
    this.loading.present();
  }
  stoploader() {
    this.loading.dismissAll();
  }
  convertToint(data) {
    return parseFloat(data);
  }
  convertArea(str) {
    if (str) {
      str = str.split("-")[1];
      return parseFloat(str) + 1;
    }
  }
  convertArea1(str) {
    if (str) {
      str = str.replace(/\ Acres/g, "");
      return parseFloat(str).toFixed(2);
    }
  }

  slotpolygon(data) {
    if (typeof data.slot_coordinates == "string") {
      data.slot_coordinates = data.slot_coordinates.replace(/\\/g, "");
      data.slot_coordinates = JSON.parse(data.slot_coordinates);
      var cords: any = {};
      data.slot_coordinate = [];
      for (var y = 0; y < data.slot_coordinates.length; y++) {
        if (typeof data.slot_coordinates[y] == "string") {
          data.slot_coordinates[y] = data.slot_coordinates[y].split(",");
          cords = {
            lat: parseFloat(data.slot_coordinates[y][0]),
            lng: parseFloat(data.slot_coordinates[y][1])
          };
          if (cords) {
            data.slot_coordinate.push(cords);
          }
        }
      }
      return data.slot_coordinate;
    } else {
      var cords: any = {};
      data.slot_coordinate = [];
      for (var y = 0; y < data.slot_coordinates.length; y++) {
        if (typeof data.slot_coordinates[y] == "string") {
          data.slot_coordinates[y] = data.slot_coordinates[y].split(",");
          cords = {
            lat: parseFloat(data.slot_coordinates[y][0]),
            lng: parseFloat(data.slot_coordinates[y][1])
          };
          if (cords) {
            data.slot_coordinate.push(cords);
          }
        }
      }
      return data.slot_coordinate;
    }
  }

  capitalize(value) {
    if (value) {
      var values = value.split(" ");
      if (typeof values == "string") {
        return value.charAt(0).toLowerCase() + value.slice(1);
      } else {
        values.forEach((item, index) => {
          if (index == 0) {
            values[index] = item.charAt(0).toLowerCase() + item.slice(1);
            value = values[index];
          } else {
            values[index] = item.charAt(0).toLowerCase() + item.slice(1);
            value = value + " " + values[index];
          }
        });
        return value;
      }
    }
  }

  capitalizeb(value) {
    if (value) {
      var values = value.split(" ");
      if (typeof values == "string") {
        return value.charAt(0).toUpperCase() + value.slice(1);
      } else {
        values.forEach((item, index) => {
          if (index == 0) {
            values[index] = item.charAt(0).toUpperCase() + item.slice(1);
            value = values[index];
          } else {
            values[index] = item.charAt(0).toUpperCase() + item.slice(1);
            value = value + " " + values[index];
          }
        });
        return value;
      }
    }
  }

  slotnorth(data) {
    if (typeof data == "string") {
      data = data.replace(/\\/g, "");
      return JSON.parse(data)[0].north;
    }
  }

  sloteast(data) {
    if (typeof data == "string") {
      data = data.replace(/\\/g, "");
      return JSON.parse(data)[0].east;
    }
  }

  slotsouth(data) {
    if (typeof data == "string") {
      data = data.replace(/\\/g, "");
      return JSON.parse(data)[0].south;
    }
  }

  slotwest(data) {
    if (typeof data == "string") {
      data = data.replace(/\\/g, "");
      return JSON.parse(data)[0].west;
    }
  }

  checkdate(date, value) {
    if (date) {
      var currdate = new Date(date);
      var dated = new Date(currdate.setDate(currdate.getDate() + value));
      return this.datepipe.transform(dated, "yyyy-MM-dd");
    }
  }
  changeformat(date) {
    if (date) {
      return this.datepipe.transform(
        date.date.year + "-" + date.date.month + "-" + date.date.day,
        "yyyy-MM-dd"
      );
    }
  }
  changeformat1(date) {
    if (date) {
      return this.datepipe.transform(date, "yyyy-MM-dd");
    }
  }
  changeformat2(date) {
    if (date) {
      return this.datepipe.transform(date, "MM-dd-yyyy");
    }
  }
  savelogincredentials(data) {
    localStorage.setItem("dayleasing_login_credentials", JSON.stringify(data));
  }
  havelogincredentials() {
    if (localStorage.getItem("dayleasing_login_credentials")) {
      return true;
    } else {
      return false;
    }
  }
  getlogincredentials() {
    if (localStorage.getItem("dayleasing_login_credentials")) {
      return JSON.parse(localStorage.getItem("dayleasing_login_credentials"));
    }
  }
  removelogincredentials() {
    if (localStorage.getItem("dayleasing_login_credentials")) {
      localStorage.removeItem("dayleasing_login_credentials");
    }
  }
  logout() {
    localStorage.removeItem("dayleasing_user");
    localStorage.removeItem("dayleasing_user_data");
    localStorage.removeItem("dayleasing_user_type");
    localStorage.removeItem("dayleasing_cart_product_added");
    localStorage.removeItem("dayleasing_property_notification");
    // localStorage.removeItem("dayleasing_login_credentials");
  }
  clearcookie() {
    this.platform.ready().then(() => {
      if (this.platform.is("cordova")) {
        window.cookies.clear(function() {});
      }
    });
  }
  ///////////////////////////////////
  /////////Error Reporting///////////
  //////////////////////////////////
  errtoast(msg) {
    var showerror: true;
    if (showerror) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 3000,
        position: "top"
      });
      toast.onDidDismiss(() => {});
      toast.present();
    }
  }
  ///////////////////////////////////
  /////////Save Properties///////////
  //////////////////////////////////
  saveProperties(data) {
    localStorage.setItem("dayleasing_properties_list", JSON.stringify(data));
  }
  getProperties() {
    if (localStorage.getItem("dayleasing_properties_list")) {
      return JSON.parse(localStorage.getItem("dayleasing_properties_list"));
    } else {
      return null;
    }
  }
  ///////////////////////////////////
  /////////Advertisements///////////
  //////////////////////////////////
  getadvertisements() {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var data: any = {
      group_id: 1
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.advertisement;
    return this.http.post(this.api_url, data, this.options);
  }
  saveads(data) {
    localStorage.setItem("dayleasingAds", JSON.stringify(data));
  }
  removeads() {
    localStorage.removeItem("dayleasingAds");
  }
  getads() {
    if (localStorage.getItem("dayleasingAds")) {
      return JSON.parse(localStorage.getItem("dayleasingAds"));
    }
  }
  havingads() {
    if (localStorage.getItem("dayleasingAds")) {
      return true;
    } else {
      return false;
    }
  }
}
