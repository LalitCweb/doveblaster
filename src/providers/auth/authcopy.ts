import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from "@angular/http";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { environment } from "../../environment";
import { ToastController, LoadingController } from "ionic-angular";
import { Vibration } from "@ionic-native/vibration";
import "rxjs/add/operator/map";
import { DatePipe } from "@angular/common";
import { ValueTransformer } from "@angular/compiler/src/util";
import { Observable, of, throwError } from "rxjs";
import { forkJoin } from "rxjs";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  api_url: any;
  constructor(
    public http: Http,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public https: HttpClient,
    public vibration: Vibration,
    public datepipe: DatePipe
  ) {}
  options: any;
  postlogin(username, password) {
    this.api_url = environment.site_url + environment.jwt_url;
    var headers = new Headers();
    var body = `username=${username}&password=${password}`;
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    this.options = new RequestOptions({ headers: headers });
    return this.http.post(this.api_url, body, this.options);
  }

  async ajaxcall(): Promise<any> {
    var params: any = {
      action: "save_map1_area_details",
      center_lat: "444.444",
      center_lng: "5555.5555",
      currnt_zoom: "8",
      property_id: "15500",
      mapaddress: "dgtdr et",
      prop_total_area: "454545",
      shapetypetosave: "rectangle",
      shapefillColor: "#545454",
      shapecords: "4543543"
    };
    // return this.http.post("https://beta.doveblasters.com/wp-admin/admin-ajax.php", params)
    const response = await this.http
      .post("https://beta.doveblasters.com/wp-admin/admin-ajax.php", params)
      .toPromise();
    return response;
  }

  postsignup(data) {
    this.api_url = environment.site_url + environment.sign_up_url;
    var token =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYmV0YS5kb3ZlYmxhc3RlcnMuY29tIiwiaWF0IjoxNTQ1NzM4Mzc4LCJuYmYiOjE1NDU3MzgzNzgsImV4cCI6MTU0NjM0MzE3OCwiZGF0YSI6eyJ1c2VyIjp7ImlkIjoiMjI5In19fQ.UepIjhu3UiSgJ-VMpDUsSkxtOJzjlYbz8Isuqt83UOQ";
    var headers = new Headers();
    // let headers = new HttpHeaders();
    // let headers = new HttpHeaders({
    //   "Access-Control-Allow-Origin": "https://localhost:8100",
    //   "Access-Control-Allow-Methods": "POST",
    //   "Content-Type": "application/x-www-form-urlencoded",
    //   Authorization: "Bearer " + token,
    //   Accept: "*/*"
    // });
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", "Bearer " + token);
    // var body = `username=${data.cname}&password=${data.password}&email=${
    //   data.email
    // }&first_name=${data.fname}&last_name=${data.lname}`;
    // var body = {
    //   username: data.cname,
    //   password: data.password,
    //   email: data.email,
    //   first_name: data.fname,
    //   last_name: data.lname
    // };
    this.options = new RequestOptions({ headers: headers });

    return this.http.get(
      "https://beta.doveblasters.com/wp-json/wp/v2/users",
      this.options
    );
  }

  getallproperty(data) {
    this.api_url =
      environment.site_url + environment.all_property + "?user_id=" + data;
    return this.http.get(this.api_url);
  }

  getsinglealldetail(property_id, user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   property_id: property_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.single_property_detail +
      "?user_id=" +
      user_id +
      "&property_id=" +
      property_id;
    return this.http.get(this.api_url);
  }

  getfavourites(userid) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: userid
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.favourites_property +
      "?user_id=" +
      userid;
    return this.http.get(this.api_url);
  }

  getallmessage(offset, author) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   paged: offset,
    //   postauthor_id: author
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.inbox_url +
      "?paged=" +
      offset +
      "&postauthor_id" +
      author;
    return this.http.get(this.api_url);
  }

  getallproperties(user_id, offset) {
    this.api_url =
      environment.site_url +
      environment.allproperties_url +
      "?user_id=" +
      user_id +
      "&paged=" +
      offset;
    return this.http.get(this.api_url);
  }
  token: any;

  addtofavourite(property_id, user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   property_id: property_id,
    //   user_id: user_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.favourite_url +
      "?property_id=" +
      property_id +
      "&user_id" +
      user_id;
    return this.http.get(this.api_url);
  }

  getuserReservation(userid, paged) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   paged: paged,
    //   user_id: userid
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.user_reservation +
      "?paged=" +
      paged +
      "&user_id" +
      userid;
    return this.http.get(this.api_url);
  }

  deletemessage(mesg_id, user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   mesg_id: mesg_id,
    //   user_id: user_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.delete_msg_url +
      "?mesg_id=" +
      mesg_id +
      "&user_id=" +
      user_id;
    return this.http.get(this.api_url);
  }

  replymessage(user_id, mesg_id, title, content) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   mesg_id: mesg_id,
    //   user_id: user_id,
    //   title: title,
    //   content: content
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.message_reply +
      "?mesg_id=" +
      mesg_id +
      "&user_id=" +
      user_id +
      "&title=" +
      title +
      "&content=" +
      content;
    return this.http.get(this.api_url);
  }

  updateprofileapi(data, user_id) {
    var dataa: any = {
      user_id: user_id,
      firstname: data.fname,
      secondname: data.lname,
      useremail: data.email,
      userphone: data.phone,
      usermobile: data.mphone,
      description: data.about,
      userfacebook: data.facebook,
      usertwitter: data.twitter,
      userlinkedin: "",
      userpinterest: "",
      live_in: data.live,
      i_speak: data.speak,
      paypal_payments_to: data.ppayment,
      payment_info: data.mphone
    };
    this.api_url =
      environment.site_url +
      environment.updateprofileapi +
      "&user_id=" +
      user_id +
      "&firstname=" +
      data.fname +
      "&secondname=" +
      data.lname +
      "&useremail=" +
      data.email +
      "&userphone=" +
      data.phone +
      "&usermobile=" +
      data.mphone +
      "&description=" +
      data.about +
      "&userfacebook=" +
      data.facebook +
      "&usertwitter=" +
      data.twitter +
      "&userlinkedin=" +
      "" +
      "&userpinterest=" +
      "" +
      "&live_in=" +
      data.live +
      "&i_speak=" +
      data.speak +
      "&paypal_payments_to=" +
      data.ppayment +
      "&payment_info=" +
      data.mphone;
    return this.http.get(this.api_url, dataa);
  }

  createprop1(user_id, data, config, location) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id,
    //   wpestate_title: data.title,
    //   prop_category: data.category,
    //   prop_action_category: data.wantlease,
    //   property_country: data.country,
    //   property_city: config.city,
    //   property_admin_area: data.property_admin_area,
    //   prop_area_center_lat: location.prop_area_center_lat,
    //   prop_area_center_lng: location.prop_area_center_lng,
    //   property_county: data.county,
    //   property_state: data.state,
    //   property_zip: data.zipcode,
    //   property_description: data.description,
    //   checkboxes_eminities: data.feature
    // };
    this.api_url =
      environment.site_url +
      environment.createprop +
      "&user_id=" +
      user_id +
      "&wpestate_title=" +
      data.title +
      "&prop_category=" +
      data.category +
      "&prop_action_category=" +
      data.wantlease +
      "&property_country=" +
      data.country +
      "&property_city=" +
      config.city +
      "&property_admin_area=" +
      data.property_admin_area +
      "&prop_area_center_lat=" +
      location.prop_area_center_lat +
      "&prop_area_center_lng=" +
      location.prop_area_center_lng +
      "&property_county=" +
      data.county +
      "&property_state=" +
      data.state +
      "&property_zip=" +
      data.zipcode +
      "&property_description=" +
      data.description +
      "&checkboxes_eminities=" +
      data.feature;
    return this.http.get(this.api_url);
  }

  createprop2(
    user_id,
    property_id,
    area,
    shape,
    shapecords,
    shapefillColor,
    mapaddress,
    lat,
    lng,
    currnt_zoom
  ) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id,
    //   property_id: property_id,
    //   prop_total_area: area,
    //   shapetypetosave: shape,
    //   shapecords: shapecords,
    //   shapefillColor: shapefillColor,
    //   mapaddress: mapaddress,
    //   center_lat: lat,
    //   center_lng: lng,
    //   currnt_zoom: currnt_zoom
    // };
    this.api_url =
      environment.site_url +
      environment.createprop2 +
      "&user_id=" +
      user_id +
      "&property_id=" +
      property_id +
      "&prop_total_area=" +
      area +
      "&shapetypetosave=" +
      shape +
      "&shapecords=" +
      shapecords +
      "&shapefillColor=" +
      shapefillColor +
      "&mapaddress=" +
      mapaddress +
      "&center_lat=" +
      lat +
      "&center_lng=" +
      lng +
      "&currnt_zoom=" +
      currnt_zoom;
    return this.http.get(this.api_url);
  }

  createprop3(user_id, property_id, post_id, data) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id,
    //   property_id: property_id,
    //   area_details: data,
    //   post_id: post_id
    // };
    this.api_url =
      environment.site_url +
      environment.createprop3 +
      "&user_id=" +
      user_id +
      "&property_id=" +
      property_id +
      "&area_details=" +
      data +
      "&post_id=" +
      post_id;
    return this.http.get(this.api_url);
  }

  createprop4(user_id, data, property_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   area_details: data,
    //   user_id: user_id,
    //   property_id: property_id
    // };
    this.api_url =
      environment.site_url +
      environment.createprop4 +
      "&user_id=" +
      user_id +
      "&property_id=" +
      property_id +
      "&area_details=" +
      data;
    return this.http.get(this.api_url);
  }

  updatepicapi() {
    return environment.site_url + environment.updatepicapi;
  }
  updateidapi() {
    return environment.site_url + environment.updateidapi;
  }
  uploadmedia() {
    return environment.site_url + environment.uploadimages;
  }

  getcategories(user_id) {
    this.api_url =
      environment.site_url + environment.categoryapi + "?user_id=" + user_id;
    return this.http.get(this.api_url);
  }

  getlease(user_id) {
    this.api_url =
      environment.site_url + environment.leaseapi + "?user_id=" + user_id;
    return this.http.get(this.api_url);
  }

  getfeatures(user_id) {
    this.api_url =
      environment.site_url + environment.featuresapi + "?user_id=" + user_id;
    return this.http.get(this.api_url);
  }

  subscriptionList(user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id
    // };
    this.api_url =
      environment.site_url +
      environment.subscriptionList +
      "?user_id=" +
      user_id;
    return this.http.get(this.api_url);
  }

  buysubscriptionList(user_id, pack_id, pack_name) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      pack_name: pack_name,
      pack_id: pack_id
    };
    this.api_url =
      environment.site_url +
      environment.subscriptionapi +
      "?user_id=" +
      user_id +
      "&pack_name=" +
      pack_name +
      "&pack_id=" +
      pack_id;
    return this.http.get(this.api_url);
  }

  getCart(user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id
    // };
    this.api_url =
      environment.site_url + environment.getcart + "?user_id=" + user_id;
    return this.http.get(this.api_url);
  }

  removeproductcart(user_id, product_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id,
    //   product_id: product_id
    // };
    this.api_url =
      environment.site_url +
      environment.removeproductcart +
      "?user_id=" +
      user_id +
      "&product_id=" +
      product_id;
    return this.http.get(this.api_url);
  }

  // async getuserReservation(offset, postperpage): Promise<any> {
  //   const response = await this.http
  //     .get(
  //       environment.site_url +
  //         environment.user_reservation +
  //         "?offset=" +
  //         offset +
  //         "&postperpage=" +
  //         postperpage
  //     )
  //     .toPromise();
  //   return response.json();
  // }

  getpropertydetail(user_id, property_id, date) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id,
      property_id: property_id,
      current_date: date
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.property_detail_url;
    return this.http.post(this.api_url, postdata, this.options);
  }

  getwallet(user_id) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url + environment.wallet_url + "?user_id=" + user_id;
    return this.http.get(this.api_url);
  }

  uploadslotpicsapi() {
    return environment.site_url + environment.uploadimages;
  }
  async getuserdata1(user_name): Promise<any> {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_name: "user_name",
      password: ""
    };
    var body = `username=${"username"}&password=${"password"}`;
    var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Access-Control-Allow-Origin", "*");
    // headers.append("Access-Control-Allow-Methods", "*");
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    // headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.getusername_url;
    const response = await this.http
      .post(
        "https://beta.doveblasters.com/wp-json/ttestpostnew/testpostneww",
        body,
        this.options
      )
      .toPromise();
    return response.json();
  }

  getuserdata(user_name) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_name: user_name
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.getusername_url +
      "?user_name=" +
      user_name;
    return this.http.get(this.api_url);
  }

  createcoupon(user_id, value) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   coupon_code: value.coupon_code,
    //   discount_type: value.discount_type,
    //   coupon_amount: value.coupon_amount,
    //   usage_limit: value.usage_limit,
    //   expire_date: value.expire_date,
    //   coupon_to_remove_handlingcharges: ""
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.createcoupon +
      "?user_id=" +
      user_id +
      "&coupon_code=" +
      value.coupon_code +
      "&discount_type=" +
      value.discount_type +
      "&coupon_amount=" +
      value.coupon_amount +
      "&usage_limit=" +
      value.usage_limit +
      "&expire_date=" +
      value.expire_date +
      "&coupon_to_remove_handlingcharges=" +
      "";
    return this.http.get(this.api_url);
  }

  getallcoupon(user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url + environment.couponslist + "?user_id=" + user_id;
    return this.http.get(this.api_url);
  }

  removecoupon(user_id, coupon_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   coupon_id: coupon_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.deletecoupon +
      "?user_id=" +
      user_id +
      "&coupon_id=" +
      coupon_id;
    return this.http.get(this.api_url);
  }

  getcoupon(user_id, coupon_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   coupon_id: coupon_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.getcoupon +
      "?user_id=" +
      user_id +
      "&coupon_id=" +
      coupon_id;
    return this.http.get(this.api_url);
  }

  editcoupon(user_id, coupon_id, value) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   coupon_id: coupon_id,
    //   coupon_code: value.coupon_code,
    //   discount_type: value.discount_type,
    //   coupon_amount: value.coupon_amount,
    //   usage_limit: value.usage_limit,
    //   expire_date: value.expire_date,
    //   coupon_to_remove_handlingcharges: ""
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.editcoupon +
      "?user_id=" +
      user_id +
      "&coupon_id=" +
      coupon_id +
      "&coupon_code=" +
      value.coupon_code +
      "&discount_type=" +
      value.discount_type +
      "&coupon_amount=" +
      value.coupon_amount +
      "&usage_limit=" +
      value.usage_limit +
      "&expire_date=" +
      value.expire_date +
      "&coupon_to_remove_handlingcharges=" +
      "";
    return this.http.get(this.api_url);
  }

  searchdata(
    start_date,
    end_date,
    category_values,
    action_values,
    all_checkers,
    city,
    property_admin_area,
    country,
    price_low,
    price_max
  ) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // if (action_values && action_values.length > 1) {
    //   action_values = undefined;
    // } else {
    //   action_values = String(action_values);
    // }
    // var postdata = {
    //   start_date: start_date,
    //   end_date: end_date,
    //   category_values: category_values,
    //   action_values: action_values,
    //   all_checkers: all_checkers,
    //   city: city,
    //   property_admin_area: property_admin_area,
    //   country: country,
    //   price_low: price_low,
    //   price_max: price_max
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    var params: any;

    if (price_max == undefined || price_max == null) {
      price_max = "";
    }

    if (price_low == undefined || price_low == null) {
      price_low = "";
    }

    if (country == undefined || country == null) {
      country = "";
    }

    if (property_admin_area == undefined || property_admin_area == null) {
      property_admin_area = "";
    }

    if (city == undefined || city == null) {
      city = "";
    }
    if (all_checkers == undefined || all_checkers == null) {
      all_checkers = "";
    }

    if (action_values == undefined || action_values == null) {
      action_values = "all";
    }
    if (category_values == undefined || category_values == null) {
      category_values = "all";
    }

    params =
      "?start_date=" +
      start_date +
      "&end_date=" +
      end_date +
      "&category_values=" +
      category_values +
      "&action_values=" +
      action_values +
      "&all_checkers=" +
      all_checkers +
      "&city=" +
      city +
      "&property_admin_area=" +
      property_admin_area +
      "&country=" +
      country +
      "&price_low=" +
      price_low +
      "&price_max=" +
      price_max;

    this.api_url = environment.site_url + environment.searchapi + params;
    return this.http.get(this.api_url);
  }

  serchformap(
    start_date,
    end_date,
    category_values,
    action_values,
    all_checkers,
    city,
    property_admin_area,
    country,
    price_low,
    price_max
  ) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    var params: any;

    if (price_max == undefined || price_max == null) {
      price_max = "";
    }

    if (price_low == undefined || price_low == null) {
      price_low = "";
    }

    if (country == undefined || country == null) {
      country = "";
    }

    if (property_admin_area == undefined || property_admin_area == null) {
      property_admin_area = "";
    }

    if (city == undefined || city == null) {
      city = "";
    }
    if (all_checkers == undefined || all_checkers == null) {
      all_checkers = "";
    }

    if (action_values == undefined || action_values == null) {
      action_values = "all";
    }
    if (category_values == undefined || category_values == null) {
      category_values = "all";
    }

    params =
      "?start_date=" +
      start_date +
      "&end_date=" +
      end_date +
      "&category_values=" +
      category_values +
      "&action_values=" +
      action_values +
      "&all_checkers=" +
      all_checkers +
      "&city=" +
      city +
      "&property_admin_area=" +
      property_admin_area +
      "&country=" +
      country +
      "&price_low=" +
      price_low +
      "&price_max=" +
      price_max;
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url = environment.site_url + environment.serchformap + params;
    return this.http.get(this.api_url);
  }
  myproperties1(user_id): Observable<any> {
    if (localStorage.getItem("dayleasing_user")) {
      this.token = JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    var headers = new Headers({ Accept: "application/json" });
    // headers.append("Content-Type", "application/json");
    // headers.append('Accept', 'application/json');
    headers.append("Authorization", `Bearer ${this.token}`);
    this.options = new RequestOptions({ headers: headers });
    let response1 = this.http.get(
      environment.site_url + environment.myproperties + "?user_id=" + user_id
    );
    return forkJoin([response1]);
  }
  myproperties(user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token = JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id
    // };
    // var headers = new Headers({ Accept: "application/json" });
    // headers.append("Content-Type", "application/json");
    // headers.append('Accept', 'application/json');
    // headers.append("Authorization", `Bearer ${this.token}`);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url + environment.myproperties + "?user_id=" + user_id;
    return this.http.get(this.api_url);
  }

  deleteproperties(user_id, property_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   property_id: property_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.deleteproperty +
      "?user_id=" +
      user_id +
      "property_id=" +
      property_id;
    return this.http.get(this.api_url);
  }

  disableproperties(user_id, property_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   property_id: property_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.disableproperty +
      "?user_id=" +
      user_id +
      "property_id=" +
      property_id;
    return this.http.get(this.api_url);
  }

  featureproperties(user_id, property_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   property_id: property_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.featureproperty +
      "?user_id=" +
      user_id +
      "property_id=" +
      property_id;
    return this.http.get(this.api_url);
  }

  mypropertiesbooking(user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.mypropertiesbooking +
      "?user_id=" +
      user_id;
    return this.http.get(this.api_url);
  }

  userreservation(user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.userreservation +
      "?user_id=" +
      user_id;
    return this.http.get(this.api_url);
  }

  mypropertyuser(user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url + environment.mypropertyuser + "?user_id=" + user_id;
    return this.http.get(this.api_url);
  }

  mypropertyusersingle(user_id, single_user_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   my_id: user_id,
    //   single_user_id: single_user_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.mypropertyusersingle +
      "?my_id=" +
      user_id +
      "&single_user_id=" +
      single_user_id;
    return this.http.get(this.api_url);
  }

  changepassword(user_id, new_password, old_password, confirm_password) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   newpass: new_password,
    //   oldpass: old_password,
    //   renewpass: confirm_password
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.changepassword +
      "?user_id=" +
      user_id +
      "&newpass=" +
      new_password +
      "&oldpass=" +
      old_password +
      "&renewpass=" +
      confirm_password;
    return this.http.get(this.api_url);
  }

  forgotpassword(user_name) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   postid: 0,
    //   type: 1,
    //   forgot_email: user_name
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.forgotpassword +
      "?postid=" +
      0 +
      "&type=" +
      1 +
      "&forgot_email=" +
      user_name;
    return this.http.get(this.api_url);
  }

  signup(value, user_type) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_login_register: value.name,
    //   user_email_register: value.email,
    //   propid: 0,
    //   user_type: user_type,
    //   user_pass: value.password,
    //   user_pass_retype: value.cpassword
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.signup +
      "?user_login_register=" +
      value.name +
      "&user_email_register=" +
      value.email +
      "&propid=" +
      0 +
      "&user_type=" +
      user_type +
      "&user_pass=" +
      value.password +
      "&user_pass_retype=" +
      value.cpassword +
      "&registering_from_app=" +
      1;
    return this.http.get(this.api_url);
  }

  signupemailverify(user_id, otp) {
    this.api_url =
      environment.site_url +
      environment.verifyemail +
      "?user_id=" +
      user_id +
      "&4digituniquecode=" +
      otp;
    return this.http.get(this.api_url);
  }

  signupphoneverify(user_id, mobile_no) {
    this.api_url =
      environment.site_url +
      environment.savephone +
      "?user_id=" +
      user_id +
      "&mobile_no=" +
      mobile_no;
    return this.http.get(this.api_url);
  }

  signupphoneverify2(user_id, validate_phoneno) {
    this.api_url =
      environment.site_url +
      environment.verifyphone +
      "?user_id=" +
      user_id +
      "&validate_phoneno=" +
      validate_phoneno;
    return this.http.get(this.api_url);
  }

  applycoupon(user_id, coupon_code) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   coupon_code: coupon_code
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.applycoupon +
      "?user_id=" +
      user_id +
      "&coupon_code=" +
      coupon_code;
    return this.http.get(this.api_url);
  }

  addtocart1(user_id, product_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   product_id: product_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.addtocart +
      "?user_id=" +
      user_id +
      "&product_id=" +
      product_id;
    return this.http.get(this.api_url);
  }

  addtocart(user_id, product_id): Observable<any> {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var postdata = {
      user_id: user_id
    };
    const getHeaders: HttpHeaders = new HttpHeaders({
      Accept: "application/json",
      Authorization: this.token
    });
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = { headers: headers };
    // this.options = new RequestOptions({ headers: headers });
    let response1 = this.https.get(
      environment.site_url +
        environment.addtocart +
        "?user_id=" +
        user_id +
        "&product_id=" +
        product_id,
      {
        headers: getHeaders
      }
    );
    return forkJoin([response1]);
  }
  savevideo(user_id, property_id, video_type, video_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   property_id: property_id,
    //   video_type: video_type,
    //   video_id: video_id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.uploadimages +
      "?user_id=" +
      user_id +
      "&property_id=" +
      property_id +
      "&video_type=" +
      video_type +
      "&video_id=" +
      video_id;
    return this.http.get(this.api_url);
  }

  deleteslot(user_id, id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var postdata = {
    //   user_id: user_id,
    //   id: id
    // };
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    this.api_url =
      environment.site_url +
      environment.deleteslot +
      "?user_id=" +
      user_id +
      "&id=" +
      id;
    return this.http.get(this.api_url);
  }

  editproperty(user_id, data, config, location, property_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      property_id: property_id,
      user_id: user_id,
      wpestate_title: data.title,
      prop_category: data.category,
      prop_action_category: data.wantlease,
      property_country: data.country,
      property_city: config.city,
      property_admin_area: data.property_admin_area,
      prop_area_center_lat: location.prop_area_center_lat,
      prop_area_center_lng: location.prop_area_center_lng,
      property_county: data.county,
      property_state: data.state,
      property_zip: data.zipcode,
      property_description: data.description,
      checkboxes_eminities: data.feature
    };
    this.api_url =
      environment.site_url +
      environment.editproperty +
      "?user_id=" +
      user_id +
      "&property_id=" +
      property_id +
      "&wpestate_title=" +
      data.title +
      "&prop_category=" +
      data.category +
      "&prop_action_category=" +
      data.wantlease +
      "&property_country=" +
      data.country +
      "&property_city=" +
      config.city +
      "&property_admin_area=" +
      data.property_admin_area +
      "&prop_area_center_lat=" +
      location.prop_area_center_lat +
      "&prop_area_center_lng=" +
      location.prop_area_center_lng +
      "&property_county=" +
      data.county +
      "&property_state=" +
      data.state +
      "&property_zip=" +
      data.zipcode +
      "&property_description=" +
      data.description +
      "&checkboxes_eminities=" +
      data.feature;
    return this.http.get(this.api_url);
  }

  deletemedia(user_id, media, property_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id,
    //   images_id: media,
    //   property_id: property_id
    // };
    this.api_url =
      environment.site_url +
      environment.deletemedia +
      "?user_id=" +
      user_id +
      "&property_id=" +
      property_id +
      "&images_id=" +
      media;
    return this.http.get(this.api_url);
  }

  editstep31(
    user_id,
    area_id,
    prop_area_name,
    prop_area_slot_details,
    prop_area_slot_color
  ) {
    if (localStorage.getItem("dayleasing_user")) {
      this.token =
        "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    headers.append("Authorization", this.token);
    this.options = new RequestOptions({ headers: headers });
    var dataa: any = {
      user_id: user_id,
      area_id: area_id,
      prop_area_name: prop_area_name,
      prop_area_slot_details: prop_area_slot_details,
      prop_area_slot_color: prop_area_slot_color
    };
    this.api_url =
      environment.site_url +
      environment.editstep31 +
      "?user_id=" +
      user_id +
      "&area_id=" +
      area_id +
      "&prop_area_name=" +
      prop_area_name +
      "&prop_area_slot_details=" +
      prop_area_slot_details +
      "&prop_area_slot_color=" +
      prop_area_slot_color;
    return this.http.get(this.api_url);
  }

  editstep32(
    user_id,
    current_prop_id,
    current_area_id,
    firstdate,
    enddate,
    daterangeprice
  ) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id,
    //   current_prop_id: current_prop_id,
    //   current_area_id: current_area_id,
    //   firstdate: firstdate,
    //   enddate: enddate,
    //   daterangeprice: daterangeprice
    // };
    this.api_url =
      environment.site_url +
      environment.editstep32 +
      "?user_id=" +
      user_id +
      "&current_prop_id=" +
      current_prop_id +
      "&current_area_id=" +
      current_area_id +
      "&firstdate=" +
      firstdate +
      "&enddate=" +
      enddate +
      "&daterangeprice=" +
      daterangeprice;
    return this.http.get(this.api_url);
  }

  editstep33(user_id, daterange_id) {
    // if (localStorage.getItem("dayleasing_user")) {
    //   this.token =
    //     "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token;
    // }
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append("Accept", "application/json");
    // headers.append("Authorization", this.token);
    // this.options = new RequestOptions({ headers: headers });
    // var dataa: any = {
    //   user_id: user_id,
    //   daterange_id: daterange_id
    // };
    this.api_url =
      environment.site_url +
      environment.editstep33 +
      "?user_id=" +
      user_id +
      "&daterange_id=" +
      daterange_id;
    return this.http.get(this.api_url);
  }

  testdata(data) {
    this.api_url = environment.site_url + environment.editstep33;
    return this.http.post(
      "https://beta.doveblasters.com/wp-json/testform/testingpost",
      data
    );
  }

  isLogin() {
    if (localStorage.getItem("dayleasing_user")) {
      return true;
    } else {
      return false;
    }
  }

  isOwner() {
    if (localStorage.getItem("dayleasing_user_type")) {
      var data: any = localStorage.getItem("dayleasing_user_type");
      data = JSON.parse(data);
      if (data.user_type[0] == "0") {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  getUser() {
    if (localStorage.getItem("dayleasing_user")) {
      return localStorage.getItem("dayleasing_user");
    } else {
      return null;
    }
  }

  getuserpic() {
    if (localStorage.getItem("dayleasing_user_type")) {
      if (
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .custom_picture &&
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .custom_picture[0]
      ) {
        return JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .custom_picture[0];
      } else {
        return JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .user_id_picture;
      }
    }
  }

  getuseridpic() {
    if (localStorage.getItem("dayleasing_user_type")) {
      if (
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .user_id_image &&
        JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .user_id_image[0]
      ) {
        return JSON.parse(localStorage.getItem("dayleasing_user_type"))
          .user_id_image[0];
      }
    }
  }

  getUsername(data) {
    if (localStorage.getItem("dayleasing_user")) {
      if (data == "username") {
        return JSON.parse(localStorage.getItem("dayleasing_user"))
          .user_display_name;
      } else if (data == "email") {
        return JSON.parse(localStorage.getItem("dayleasing_user")).user_email;
      }
    } else {
      return null;
    }
  }
  getuserId() {
    if (localStorage.getItem("dayleasing_user_type")) {
      return JSON.parse(localStorage.getItem("dayleasing_user_type")).user_id;
    } else {
      return null;
    }
  }

  getProfile() {
    if (localStorage.getItem("dayleasing_user_type")) {
      return localStorage.getItem("dayleasing_user_type");
    } else {
      return null;
    }
  }

  vibalert() {
    this.vibration.vibrate(300);
  }

  toast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "top"
    });

    toast.onDidDismiss(() => {});

    toast.present();
  }
  loading: any;
  startloader() {
    this.loading = this.loadingCtrl.create({
      spinner: "crescent"
    });
    this.loading.present();
  }
  stoploader() {
    this.loading.dismissAll();
  }
  convertToint(data) {
    return parseFloat(data);
  }

  slotnorth(data) {
    data = data.replace(/\\/g, "");
    return JSON.parse(data)[0].north;
  }

  sloteast(data) {
    data = data.replace(/\\/g, "");
    return JSON.parse(data)[0].east;
  }

  slotsouth(data) {
    data = data.replace(/\\/g, "");
    return JSON.parse(data)[0].south;
  }

  slotwest(data) {
    data = data.replace(/\\/g, "");
    return JSON.parse(data)[0].west;
  }

  checkdate(date, value) {
    if (date) {
      var currdate = new Date(date);
      var dated = new Date(currdate.setDate(currdate.getDate() + value));
      return this.datepipe.transform(dated, "yyyy-MM-dd");
    }
  }
  changeformat(date) {
    if (date) {
      return this.datepipe.transform(date, "yyyy-MM-dd");
    }
  }

  changeformat2(date) {
    if (date) {
      return this.datepipe.transform(date, "MM-dd-yyyy");
    }
  }
  logout() {
    localStorage.removeItem("dayleasing_user");
    localStorage.removeItem("dayleasing_user_data");
    localStorage.removeItem("dayleasing_user_type");
  }
}
