import { NgModule } from "@angular/core";
import { PropertybookingPipe } from "./propertybooking/propertybooking";
import { SearchmypropertyPipe } from "./searchmyproperty/searchmyproperty";
import { SortpropertiesPipe } from "./sortproperties/sortproperties";
import { UniquepropertyPipe } from './uniqueproperty/uniqueproperty';
import { SafeHtmlPipe } from './safe-html/safe-html';
@NgModule({
  declarations: [PropertybookingPipe, SearchmypropertyPipe, SortpropertiesPipe,
    UniquepropertyPipe,
    SafeHtmlPipe],
  imports: [],
  exports: [PropertybookingPipe, SearchmypropertyPipe, SortpropertiesPipe,
    UniquepropertyPipe,
    SafeHtmlPipe]
})
export class PipesModule {}
